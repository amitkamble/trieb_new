import colors from '../globals/colors';
import fonts from '../globals/fonts';
import Language from '../globals/languages';
import Images from '../globals/images';
import API from '../utils/API';
import sizes from '../utils/sizes';
import NavgationStyles from '../utils/styles';
import Navigation from '../../src/components/Navigation';

export {
    colors,Images,fonts,sizes,NavgationStyles,Navigation,API,Language
};
