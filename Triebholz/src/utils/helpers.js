
import {
  listAppointmentIdGenarates
} from "../../src/graphql/queries";
import {
  updateAppointmentIdGenarate,
  createAppointmentIdGenarate
} from "../../src/graphql/mutations";
import constants from "../utils/constants"
import Moment from 'moment';
import { API, graphqlOperation } from "aws-amplify";
import CryptoJS from "react-native-crypto-js";
export const postWith = (body) => {
  return {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: body
  }
}

export const encCryptoJS=(text) =>{

  let ciphertext = CryptoJS.AES.encrypt(text, constants.aesKey).toString();
  return ciphertext

}
export const decCryptoJS=(ciphertext)=>{
  let bytes = CryptoJS.AES.decrypt(ciphertext, constants.aesKey);
  let originalText = bytes.toString(CryptoJS.enc.Utf8);
  return originalText
}


export const genreateAppointmentID = async (props) => {

  let response = await API.graphql(
    graphqlOperation(listAppointmentIdGenarates)
  );
  if (response.data != null && response.data.listAppointmentIdGenarates.items.length > 0) {

    var idDynamo = response.data.listAppointmentIdGenarates.items[0].id
    var prefixNameDynamo = response.data.listAppointmentIdGenarates.items[0].prefixName
    var uniqueIdDynamo = Number(response.data.listAppointmentIdGenarates.items[0].uniqueId)

    var tempId = uniqueIdDynamo + 1
    var appointMentIdInfo = {
      input: {
        id: idDynamo,
        prefixName: prefixNameDynamo,
        uniqueId: tempId,
      }
    }
    const response1 = await API.graphql(
      graphqlOperation(
        updateAppointmentIdGenarate,
        appointMentIdInfo
      )
    );

    if (response1.data.updateAppointmentIdGenarate != null) {
      var name = response1.data.updateAppointmentIdGenarate.prefixName
      var idD = response1.data.updateAppointmentIdGenarate.uniqueId
      var padId = pad(Number(idD), 10)
      props.getUniqueId(`${name}${padId}`)
    }

  }
  else {

    var prefixNameTemo = "W"
    var uniqueIdTemp = "1"
    var appointMentIdInfo = {
      input: {
        prefixName: prefixNameTemo,
        uniqueId: uniqueIdTemp,
      }
    }
    const response1 = await API.graphql(
      graphqlOperation(
        createAppointmentIdGenarate,
        appointMentIdInfo
      )
    );
    if (response1.data.createAppointmentIdGenarate != null) {
      var name = response1.data.createAppointmentIdGenarate.prefixName
      var idD = response1.data.createAppointmentIdGenarate.uniqueId
      var padId = pad(Number(idD), 10)
      props.getUniqueId(`${name}${padId}`)
    }

  }
}
function pad(num, size) {
  var s = num + "";
  while (s.length < size) s = "0" + s;
  return s;
}
