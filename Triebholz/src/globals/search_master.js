import React, { memo, useState, useEffect } from 'react';
import { View, StatusBar, TextInput, Dimensions, Keyboard, TouchableWithoutFeedback, Image, TouchableOpacity, Modal, ActivityIndicator, Text, ScrollView, FlatList, Alert, KeyboardAvoidingView } from 'react-native';
import styles from './styles';
import AppIntroSlider from 'react-native-app-intro-slider';
import Swiper from 'react-native-swiper'
import { colors, fonts, Images, API } from '../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import Animated1 from 'react-native-reanimated';
import { useNetInfo } from "@react-native-community/netinfo";
import PTRView from 'react-native-pull-to-refresh';
import Spinner from 'react-native-loading-spinner-overlay';
import RoundedCheckbox from "react-native-rounded-checkbox";
import RoundedCheckbox_new from "react-native-rounded-checkbox";
import Video from 'react-native-video';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import Moment from 'moment';
import Modal_new from 'react-native-modal';
import DatePicker from 'react-native-datepicker'
import SearchableDropdown from 'react-native-searchable-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import KeyboardListener from 'react-native-keyboard-listener';

const search_master = ({ tag, platform, creator, setSelected_subsys, setSelected_platform,
  isModalVisible_search, setModalVisibleSearch, Search_now, selected_sub_sys, selected_sub_platform, selected_item,
  setSelected_item, setFromDate, setToDate, from_date, to_date, setClear }) => {

  const netInfo = useNetInfo();
  const [isModalVisible, setModalVisible] = useState(false);
  // var status_know= 0 ;
  // var commentdata= [] ;
  var cureentDate = Moment(new Date())
  var radio_props = [
    { label: 'All Knowledge', value: 0 },
    { label: 'My area of interest', value: 1 }
  ];
  var width = Dimensions.get('window').width; //full width
  var height = Dimensions.get('window').height; //full height
  const [refreshing, setRefreshing] = React.useState(false);

  const [keyboardStatus, setKeyboardStatus] = useState(false);

 
  const [state, setState] = React.useState({
    index: 0,
    routes: [
      { key: 'first', title: 'Creator' },
      { key: 'second', title: 'Created Date' },
      { key: 'third', title: 'Platform Id' },
      { key: 'four', title: 'System Id' },
    ]
  });

  const toggleModal_search = () => {
    setModalVisibleSearch(!isModalVisible_search);
  };
  const search_now = () => {

    Search_now();

  };


  const renderItemDr_tag = ({ item, index }) => (

    <View style={{ flex: 1,marginTop:5, flexDirection: 'row', paddingLeft: 20, padding: 10, alignItems: 'center' }}>
      <RoundedCheckbox text={''} innerSize={23}
        outerSize={23}
        isChecked={item.isSelected}
        checkedColor={colors.green_text_coot}
        component={
          <Image source={Images.correct} style={styles.pop_up_correct} />

        }
        onPress={(checked) => setSelected_subsys(item, index, checked)} />
      <Text style={{ flex: 1, fontFamily: fonts.SemiBold, marginLeft: 10, fontSize: 14 }}>{item.system_desc}</Text>

    </View>
  );

  const renderItemDr_platform = ({ item, index }) => (

    <View style={{ flex: 1,marginTop:5, flexDirection: 'row', paddingLeft: 20, padding: 10, alignItems: 'center' }}>
      <RoundedCheckbox_new text={''} innerSize={23}
        outerSize={23}
        isChecked={item.isSelected_pla}
        checkedColor={colors.green_text_coot}
        component={
          <Image source={Images.correct} style={styles.pop_up_correct} />

        }
        onPress={(checked) => setSelected_platform(item, index, checked)} />
      <Text style={{ flex: 1, fontFamily: fonts.SemiBold, marginLeft: 10, fontSize: 14 }}>{item.platform_desc}</Text>

    </View>
  );


  const _handleIndexChange = index => setState({ ...state, ['index']: index });

  const _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <TouchableOpacity activeOpacity={1.0} style={{
        flexDirection: 'column',
        backgroundColor: colors.main_color_dull_main 
      }}>
        <ScrollView >
          {props.navigationState.routes.map((route, i) => {
            return (
              <TouchableOpacity
                onPress={() => setState({ ...state, ['index']: i })}
                style={state.index == i ? { alignItems: "center", padding: 10,paddingTop:20, paddingBottom:20, backgroundColor: colors.white } : { alignItems: "center", padding: 10,paddingTop:20, paddingBottom:20, }}>

                <Text style={state.index == i ? { color: '#000', fontFamily: fonts.SemiBold, fontSize: 14, } : { color: '#000', fontFamily: fonts.Regular, fontSize: 14, }}>
                  {route.title}
                </Text>
                {/* <View style={{ height: 3, position: 'absolute', marginTop: Platform.OS == 'ios' ? 79 : 35, backgroundColor: state.index == i ? colors.black : colors.white, width: width - width / 1.4 }} /> */}
                {/* <View style={{ height: 1, position: 'absolute', marginTop: Platform.OS == 'ios' ? 79 : 37, backgroundColor: colors.gray, width: width }} /> */}

              </TouchableOpacity>
            );

          })}
        </ScrollView>
        {/* <View style={{ height: '100%', marginLeft: 113, position: 'absolute', backgroundColor: colors.gray, width: 1 }} /> */}

      </TouchableOpacity>
    );
  };

  const FirstRoute = (data_user) => (
    <FlatList
      data={data_user}
      renderItem={renderItemDr_tag}
      keyExtractor={item => item.id}
    />
  );

  const second_route = (data_user) => (
    <FlatList
      data={data_user}
      renderItem={renderItemDr_platform}
      keyExtractor={item => item.id}
    />
  );

  const render_keyboard = () => {
    // if(keyboardStatus){
      Keyboard.emit()

      setKeyboardStatus(true)
    // }else{
    //   setKeyboardStatus(true)

    // }
  }
  const render_creator = () => (
    <View keyboardShouldPersistTaps="handled"    style={{ flex: 1,marginTop:5, marginRight: 5 }}>
      <SearchableDropdown 
        onTextChange={(text) => console.log(text)}
        // onFocus={() => setKeyboardStatus(false)}
        showSoftInputOnFocus
        autoFocus={true}
        onBlur={() => setKeyboardStatus(false)}
        blurOnSubmit={false}
        selectedItems={selected_item}
        onItemSelect={(item) => {
          var items = selected_item;
          if (selected_item.length == 0) {

            items.push(item)
            setSelected_item(items);
            console.log(JSON.stringify(items))
          } else {
            alert('You can select only one creator')
          }

        }}
        containerStyle={{ padding: 5, flex: 1 }}
        onRemoveItem={(item, index) => {
          var items = selected_item.filter((sitem) => sitem.id !== item.id);
          setSelected_item(items);
        }} 
        itemStyle={{
          padding: 10,
          marginTop: 2,
          backgroundColor: '#ededed',
          borderColor: '#ededed',
          borderWidth: 1,
          borderRadius: 5,
          fontSize: 14,
        }}
        itemTextStyle={{ color: '#222', fontSize: 14, }}
        itemsContainerStyle={{ maxHeight: 140 }}
        items={creator}
        chip={true}
        resetValue={false}
        textInputProps={
          {
            placeholder: "Enter Name",
            underlineColorAndroid: "transparent",
            fontSize: 14,
            style: {
              padding: 12,
              borderWidth: 1,
              borderColor: '#ccc',
              borderRadius: 5,
              fontSize: 14,
              height:50
            },
          }
        }
        listProps={
          {
            nestedScrollEnabled: true,
          }
        }
        multi={true}

      />
    </View>
  );
  const renderDates = () => (

    <View style={styles.searchSection_date2}>
      <Text style={{
        color: colors.black,
        fontSize: 14,
        paddingLeft: 25,
        fontFamily: fonts.SemiBold,
      }}>Select From Date</Text>

      <View style={styles.searchSection_date1}>
        <DatePicker
          style={styles.dobText}
          date={from_date}
          mode="date"
          placeholder={"From date"}
          format="DD-MM-YYYY"
          maxDate={cureentDate}
          minDate={"01-01-2021"}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          showIcon={true}
          customStyles={{
            placeholderText: {
              ...styles.placeholderText
            },
            dateText: {
              ...styles.dateText
            },
            dateInput: { ...styles.dateInput },
            dateIcon: { ...styles.logo_date }

          }}
          onDateChange={(date) => {
            setFromDate(date)
          }}
        />
      </View>
      <Text style={{
        color: colors.black,
        fontSize: 14,
        paddingLeft: 25,
        fontFamily: fonts.SemiBold,
        marginTop: 30,
        marginBottom: 20
      }}>Select To Date</Text>

      <View style={styles.searchSection_date3}>
        <DatePicker
          style={styles.dobText}
          date={to_date}
          mode="date"
          placeholder={'To Date'}
          format="DD-MM-YYYY"
          maxDate={cureentDate}
          minDate={'01-01-2021'}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          showIcon={true}
          customStyles={{
            placeholderText: {
              ...styles.placeholderText
            },
            dateText: {
              ...styles.dateText
            },
            dateInput: { ...styles.dateInput },
            dateIcon: { ...styles.logo_date }
          }}
          onDateChange={(date) => {
            setToDate(date)
          }}
        />
      </View>

    </View>
  );


  const _renderScene = (data_user) => SceneMap({
    first: () => render_creator(),
    second: () => renderDates(),
    third: () => second_route(data_user),
    four: () => FirstRoute(data_user),
  });

const clearfilter = ()=>{
  setState({ ...state, ['index']: 0 });
  setClear();
}
  return (

    <ScrollView keyboardShouldPersistTaps="handled" contentContainerStyle={{flex:1, width: width}}>
      
      <View style={{ flex: 1, width: width, height: keyboardStatus ? height : height, alignItems: 'center', backgroundColor: colors.white, position: 'absolute', alignSelf: 'center', justifyContent: 'center' }}>
      <View style={{ width: width,padding:10, flexDirection: 'row',backgroundColor:colors.main_dark_color, alignSelf: 'center',alignItems:'center' }}>

        <TouchableOpacity
          onPress={() => clearfilter()}
          style={{
            marginTop: Platform.OS == 'ios' ? 0 : 0,
            margin: 10, position: 'absolute', right: 0
          }}>
          <Text style={styles.cleartext}>{'Clear Filters'}</Text>

        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => toggleModal_search()}
          style={{
            marginTop: Platform.OS == 'ios' ? 0 : 5, marginLeft: 10
          }}>
          <Image source={Images.back_arrow_white} style={styles.pop_up_cancel} />

        </TouchableOpacity>
        <Text style={{ fontFamily: fonts.SemiBold,color:colors.white, marginLeft: 15, marginTop: 3, fontSize: 16 }}>{'Filters '}</Text>

      </View>
      {/* <View style={{ width: '100%', height: 1, backgroundColor: colors.gray, marginTop: 10, }}></View> */}

      <TabView
        navigationState={state}
        renderScene={_renderScene(
          state.index == 0 ?
            tag :
            state.index == 1 ?
              tag :
              state.index == 2 ?
                platform :
                tag)}
        renderTabBar={_renderTabBar}
        swipeEnabled={false}
        onIndexChange={_handleIndexChange}
        style={{ flexDirection: 'row' }}
      />
      <TouchableOpacity onPress={() => search_now()} style={styles.login_pop}>
        <Text style={styles.submmittext}>APPLY</Text>
      </TouchableOpacity>
    </View>
    </ScrollView>
  );
};

export default search_master;
