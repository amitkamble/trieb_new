


import React, { useRef, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashContainer from '../screens/Splash/SplashContainer';
import DemoContainer from '../screens/Demo/DemoContainer';
import LoginContainer from '../screens/Login/LoginContainer';
import ForgetPasswordContainer from '../screens/ForgetPassword/ForgetPasswordContainer';
import Sign_upContainer from '../screens/Sign_up/Sign_upContainer';
import EnterOTPContainer from '../screens/EnterOTP/EnterOTPContainer';
import ResetPassContainer from '../screens/ResetPass/ResetPassContainer';
import Reset_successContainer from '../screens/Reset_success/Reset_successContainer';
import HomePageContainer from '../screens/HomePage/HomePageContainer';
import MyTripsContainer from '../screens/MyTrips/MyTripsContainer';
import Add_FlightContainer from '../screens/Add_Flight/Add_FlightContainer';
import ScanScreenContainer from '../screens/ScanScreen/ScanScreenContainer';
import MyTripsDetailsContainer from '../screens/MyTripsDetails/MyTripsDetailsContainer';
import WidgetContainer from '../screens/Widget/WidgetContainer';
import TipsContainer from '../screens/Tips/TipsContainer';
import ReviewsContainer from '../screens/Reviews/ReviewsContainer';
import CheckListContainer from '../screens/CheckList/CheckListContainer';
import MyProfileContainer from '../screens/MyProfile/MyProfileContainer';
import InboxContainer from '../screens/Inbox/InboxContainer';
import Import_flightContainer from '../screens/Import_flight/Import_flightContainer';
import LeaderboardContainer from '../screens/Leaderboard/LeaderboardContainer';
import SettingContainer from '../screens/Setting/SettingContainer';
import Subscribe_eChannelContainer from '../screens/Subscribe_eChannel/Subscribe_eChannelContainer';
import EditProfileContainer from '../screens/EditProfile/EditProfileContainer';
import StatisticDataContainer from '../screens/StatisticData/StatisticDataContainer';
import Select_FlightContainer from '../screens/Select_Flight/Select_FlightContainer';
import Add_Flight_manuallyContainer from '../screens/Add_Flight_manually/Add_Flight_manuallyContainer';
import Ar_EarthContainer from '../screens/Ar_Earth/Ar_EarthContainer';
import { View, StyleSheet, Alert, Button, Image, Dimensions, Text, TouchableOpacity, Platform } from 'react-native';
import { colors, fonts, sizes } from '../utils/theme';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const SplashStack = createStackNavigator();
const SplashScreen = () => (
  <SplashStack.Navigator screenOptions={{ headerShown: false }} initialRouteName="SplashContainer">
    <SplashStack.Screen name="SplashContainer" component={SplashContainer} />
    <DemoStack.Screen name="LoginContainer" component={LoginContainer} />
    <DemoStack.Screen name="SignUp" component={Sign_upContainer} />
    <DemoStack.Screen name="ForgetPass" component={ForgetPasswordContainer} />
    <DemoStack.Screen name="EnterOTP" component={EnterOTPContainer} />
    <DemoStack.Screen name="ResetPass" component={ResetPassContainer} />
    <DemoStack.Screen name="Reset_success" component={Reset_successContainer} />
    <DemoStack.Screen name="HomePage" component={HomePageContainer} />
    <DemoStack.Screen name="MyTrips" component={MyTripsContainer} />
    <DemoStack.Screen name="Add_Flight" component={Add_FlightContainer} />
    <DemoStack.Screen name="ScanScreen" component={ScanScreenContainer} />
    <DemoStack.Screen name="MyTripsDetails" component={MyTripsDetailsContainer} />
    <DemoStack.Screen name="Widget" component={WidgetContainer} />
    <DemoStack.Screen name="Checklist" component={CheckListContainer} />
    <DemoStack.Screen name="Add_Flight_manually" component={Add_Flight_manuallyContainer} />
    <DemoStack.Screen name="Tips" component={TipsContainer} />
    <DemoStack.Screen name="Reviews" component={ReviewsContainer} />
    <DemoStack.Screen name="MyProfile" component={MyProfileContainer} />
    <DemoStack.Screen name="Inbox" component={InboxContainer} />
    <DemoStack.Screen name="Leaderboard" component={LeaderboardContainer} />
    <DemoStack.Screen name="Import_flight" component={Import_flightContainer} />
    <DemoStack.Screen name="Setting" component={SettingContainer} />
    <DemoStack.Screen name="EditProfile" component={EditProfileContainer} />
    <DemoStack.Screen name="StatisticDataContainer" component={StatisticDataContainer} />
    <DemoStack.Screen name="Select_FlightContainer" component={Select_FlightContainer} />
    <DemoStack.Screen name="Subscribe_eChannelContainer" component={Subscribe_eChannelContainer} />
    <DemoStack.Screen name="Ar_EarthContainer" component={Ar_EarthContainer} />
  </SplashStack.Navigator>
);

const DemoStack = createStackNavigator();
const DemoScreen = () => (
  <DemoStack.Navigator screenOptions={{ headerShown: false }} initialRouteName="LoginContainer">
    {/* <DemoStack.Screen name="DemoContainer" component={DemoContainer} /> */}
    <DemoStack.Screen name="LoginContainer" component={LoginContainer} />
    <DemoStack.Screen name="SignUp" component={Sign_upContainer} />
    <DemoStack.Screen name="ForgetPass" component={ForgetPasswordContainer} />
    <DemoStack.Screen name="EnterOTP" component={EnterOTPContainer} />
    <DemoStack.Screen name="ResetPass" component={ResetPassContainer} />
    <DemoStack.Screen name="Reset_success" component={Reset_successContainer} />
    <DemoStack.Screen name="HomePage" component={HomePageContainer} />
    <DemoStack.Screen name="MyTrips" component={MyTripsContainer} />
    <DemoStack.Screen name="Add_Flight" component={Add_FlightContainer} />
    <DemoStack.Screen name="ScanScreen" component={ScanScreenContainer} />
    <DemoStack.Screen name="MyTripsDetails" component={MyTripsDetailsContainer} />
    <DemoStack.Screen name="Widget" component={WidgetContainer} />
    <DemoStack.Screen name="Checklist" component={CheckListContainer} />
    <DemoStack.Screen name="Add_Flight_manually" component={Add_Flight_manuallyContainer} />
    <DemoStack.Screen name="Tips" component={TipsContainer} />
    <DemoStack.Screen name="Reviews" component={ReviewsContainer} />
    <DemoStack.Screen name="MyProfile" component={MyProfileContainer} />
    <DemoStack.Screen name="Inbox" component={InboxContainer} />
    <DemoStack.Screen name="Leaderboard" component={LeaderboardContainer} />
    <DemoStack.Screen name="Import_flight" component={Import_flightContainer} />
    <DemoStack.Screen name="Setting" component={SettingContainer} />
    <DemoStack.Screen name="EditProfile" component={EditProfileContainer} />
    <DemoStack.Screen name="StatisticDataContainer" component={StatisticDataContainer} />
    <DemoStack.Screen name="Select_FlightContainer" component={Select_FlightContainer} />
    <DemoStack.Screen name="Subscribe_eChannelContainer" component={Subscribe_eChannelContainer} />
    <DemoStack.Screen name="Ar_EarthContainer" component={Ar_EarthContainer} />

  </DemoStack.Navigator>
);


const getInitialRouteName = () => {
  return store.getState().userSession.isUserLoggedin ? "Demo" : "Splash";
}

const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator headerMode="none" initialRouteName={getInitialRouteName}>
    <RootStack.Screen
      name="Splash"
      component={SplashScreen}
      options={{
        animationEnabled: false
      }}
    />
    <RootStack.Screen
      name="Demo"
      component={DemoScreen}
      options={{
        animationEnabled: false
      }}
    />

  </RootStack.Navigator>
);

function AppNavigator() {

  return (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );
}

const textStyles = StyleSheet.create({
  inputSelected: {
    fontSize: 12,
    color: colors.hint_color,
  },
  inputUnselected: {
    fontSize: 12,
    color: colors.login_button_color,
  },

});

const styles = StyleSheet.create({
  stack: {
    flex: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.6,
    shadowRadius: 10,
    elevation: 5,
    marginBottom: Platform.OS == 'ios' ? 0 : 0
  },
  drawerStyles: { flex: 1, width: '60%', backgroundColor: 'transparent' },
  drawerItem: { alignItems: 'flex-start', marginVertical: 0 },
  drawerLabel: { color: 'white', marginLeft: -16 },
  avatar: {
    borderRadius: 60,
    marginBottom: 16,
    borderColor: 'white',
    borderWidth: 0,
  },

  back_image: {
    height: 18,
    width: 25,
    marginTop: Platform.OS == 'ios' ? 10 : 25,
    alignSelf: 'center',
    marginLeft: 20
  },
  back_image1: {
    height: 18,
    width: 25,
    marginTop: Platform.OS == 'ios' ? 10 : 65,
    alignSelf: 'center',
    marginLeft: 20
  },
});

export default AppNavigator;



