import React, { memo, useState } from 'react';
import { View, Image, Text, StatusBar, TouchableOpacity, ScrollView,TextInput} from 'react-native';
import styles from './styles';
import { Images } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';

import { WebView } from 'react-native-webview';

const DemoComponent = memo((props) => {

    const continueButtonPress = () => {
        props.continueButtonPress();
    };

    const [data, setData] = useState({
        division_id: 1,
        platform_id: '',
        system_id: '',
        subsystem_id: '',
        title: '',
        attachment: '',
        description: '',
      });

    const onLikePress = () => {
        // props.SignUpButtonPress();
    };
    const [iscroll, setScroll] = React.useState(false);

    return (
        // <View safe style={styles.container}>
        // <View safe style={styles.container}>
        <ScrollView style={{ backgroundColor: colors.white, }}>
            <StatusBar translucent backgroundColor={colors.main_dark_color} />

            {/* <View style={styles.container_slide}>
                <Swiper showsButtons={false} contentContainerStyle={{ height: 600 }} activeDotColor={colors.main_color} dotColor={colors.dot_color}>
                    <View style={styles.slide1}>
                        <WebView source={{ uri: 'https://in.docworkspace.com/d/sIFW_x9Qp5pH4ggY' }} style= {{height:200, width:500}} />
                    </View>
                </Swiper>
            </View> */}
            <View style={{ flexDirection: 'row', position: 'relative', marginTop: 40 }}>
                <View style={{ flexDirection: 'row' }}>

                    <Image
                        source={Images.profile}
                        // resizeMode="center"
                        style={styles.profileImg1}
                    />
                    <View style={{ flexDirection: 'column', }}>

                        <Text style={styles.banner_name}>Aniket Shinde</Text>
                        <Text style={styles.banner_designation}>DY Manager</Text>

                    </View>
                </View>
                <View style={{ flexDirection: 'row', right: 0, position: 'absolute' }}>

                    <View style={{ flexDirection: 'column' }}>

                        <Text style={styles.vehicle_designation}>Aniket Shinde</Text>
                        <Text style={styles.date_designation}>14 March 2021 | 2:31 pm</Text>

                    </View>

                    <Image
                        source={Images.q_and_a}
                        // resizeMode="center"
                        style={styles.bookmarks}
                    />
                </View>
            </View>

            {/* <View style={styles.profileImg1}>
            <Text style={{
              fontFamily: fonts.SemiBold,
              fontSize: sizes.regularLarge,
              fontSize: 18,
              color: colors.black,
              alignItems: 'center'
            }}>{(data?.name == "" || data.name == undefined) ? "" : data.name[0]+data.lastname[0]}</Text>
          </View> */}
            <Text style={styles.banner_heading}>When to replace your cluch plates : warning signs</Text>

            <View style={{ margin: 2, borderColor: colors.black, borderWidth: 1, height: 260 }}>
                <WebView source={{ uri: 'https://in.docworkspace.com/d/sIFW_x9Qp5pH4ggY' }} style={{ flex: 1, borderRadius: 15, borderWidth: 1, borderColor: colors.black }} />
            </View>
            {/* <View style={{ margin: 20, height: 2, backgroundColor: colors.view_color, alignSelf: 'stretch' }} /> */}
            <View style={styles.white_row_container}>
                <ScrollView scrollEnabled={iscroll}>
                    <Text style={styles.banner_desc}>The clutch is one of the most important parts in a manual transmission car as it connects the engine with the gearbox, and is what provides motive force for the car. It is also one of the parts of the car that sees high wear and tear depending on a person’s driving style and the environment it is driven in.

                    Cars that primarily drive in bumper-to-bumper city traffic are likely to see more clutch wear than those that are driven on the highway or in moderate traffic conditions. Similarly, cars driven in hilly areas are likely to see more clutches wear than cars driven in the plains.
                    Another sign that there is a problem with your clutch is when there is excessive shuddering when you release the clutch pedal, especially at low speeds. This could be because of some contamination as well on the surface of the clutch plate like oil or grease (which shouldn’t normally happen), or even water and sludge if you have driven through water-logged roads. If the shuddering goes away after running for a while it’s not a serious issue, but if the shudder remains then it could mean that the clutch pressure plate is worn and not pushing the clutch friction plate against the engine flywheel properly. Sometimes a misaligned engine and gearbox due to a bad engine mount or gearbox mount also has a similar symptom.

                    Sometimes when you press the clutch pedal and then release it you will hear a chirping noise coming from the engine/gearbox area. This sounds like anklets being jingled very faintly. When you press down the clutch pedal this sound usually goes away, but reappears as soon as you release the clutch. Sometimes you will get a deeper grinding or humming noise. This is a symptom indicative of a worn clutch release bearing. While just the clutch release bearing (CRB) can be replaced, if not checked in time this can lead to a bent clutch fork (the mechanism that pushes or releases the clutch inside the clutch housing), which can then lead to pressure plate problems (bent fingers on the plate) and ultimately a change of the entire clutch assembly.
                    </Text>

                    <Text style={styles.banner_desc}>The clutch is one of the most important parts in a manual transmission car as it connects the engine with the gearbox, and is what provides motive force for the car. It is also one of the parts of the car that sees high wear and tear depending on a person’s driving style and the environment it is driven in.

                    Cars that primarily drive in bumper-to-bumper city traffic are likely to see more clutch wear than those that are driven on the highway or in moderate traffic conditions. Similarly, cars driven in hilly areas are likely to see more clutches wear than cars driven in the plains.
                    Another sign that there is a problem with your clutch is when there is excessive shuddering when you release the clutch pedal, especially at low speeds. This could be because of some contamination as well on the surface of the clutch plate like oil or grease (which shouldn’t normally happen), or even water and sludge if you have driven through water-logged roads. If the shuddering goes away after running for a while it’s not a serious issue, but if the shudder remains then it could mean that the clutch pressure plate is worn and not pushing the clutch friction plate against the engine flywheel properly. Sometimes a misaligned engine and gearbox due to a bad engine mount or gearbox mount also has a similar symptom.

                    Sometimes when you press the clutch pedal and then release it you will hear a chirping noise coming from the engine/gearbox area. This sounds like anklets being jingled very faintly. When you press down the clutch pedal this sound usually goes away, but reappears as soon as you release the clutch. Sometimes you will get a deeper grinding or humming noise. This is a symptom indicative of a worn clutch release bearing. While just the clutch release bearing (CRB) can be replaced, if not checked in time this can lead to a bent clutch fork (the mechanism that pushes or releases the clutch inside the clutch housing), which can then lead to pressure plate problems (bent fingers on the plate) and ultimately a change of the entire clutch assembly.
                    </Text>

                </ScrollView>
                {
                    !iscroll ?
                        <TouchableOpacity onPress={() => setScroll(true)} style={{ flexDirection: 'row', height: 50, backgroundColor: '000', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ width: 90, height: 30, fontSize: 12, fontFamily: fonts.SemiBold, backgroundColor: colors.black, borderRadius: 20, textAlign: 'center', textAlignVertical: 'center', color: colors.white }}>More</Text>
                            <Image
                                source={Images.down_smal}
                                // resizeMode="center"
                                style={styles.down}
                            />
                        </TouchableOpacity>
                        : null
                }


                <View style={{ flexDirection: 'row', height: 30, backgroundColor: '000', justifyContent: 'center', alignItems: 'center', }}>
                    <Text style={{ fontSize: 11, marginTop: 7, marginRight: 7, fontFamily: fonts.SemiBold, borderRadius: 20, textAlign: 'center', textAlignVertical: 'center', color: colors.main_dark_color }}>8 people found this reply as helpful - was this reply helpful?</Text>
                    <TouchableOpacity onPress={() => onLikePress()}>
                        <Image
                            source={Images.like}
                            // resizeMode="center"
                            style={styles.like}
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <Text style={{ fontSize: 13, marginTop: 7, marginRight: 7, fontFamily: fonts.SemiBold, borderRadius: 20, textAlign: 'center', textAlignVertical: 'center', color: colors.black }}>Leave a comment</Text>

            <View style={styles.container_view_que}>

                <TextInput
                    placeholder={
                        "Leave your comment here"
                    }
                    onChangeText={(value) => setData({ ...data, ['title']: value })}
                    onFocus={() => setFocus_search(true)}
                    multiline={true}
                    value={data.title}
                    onBlur={() => setFocus_search(false)}
                    placeholderTextColor={colors.text_color_semi_black}
                    autoCorrect={false}
                    style={styles.inputText} />
            </View>
        </ScrollView>
        // </View>

        // </View>

    );
});


export default DemoComponent;
