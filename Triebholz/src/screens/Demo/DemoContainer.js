import React, {useCallback} from 'react';
import {connect} from 'react-redux';
import DemoComponent from './DemoComponent';


const DemoContainer = (props) => {
 
  const continueButtonPress = () => {

   
    const { navigation } = props;
    navigation.navigate('LoginContainer')
  };

  const SignUpButtonPress = () => {

   
    const { navigation } = props;
    // navigation.navigate('SignUpContainer')
    // navigation.navigate('Tab')

  };

  return (
    <DemoComponent props={props} continueButtonPress={continueButtonPress} SignUpButtonPress = {SignUpButtonPress} />
  );
};
DemoContainer.navigationOptions = {
  header: null,
};
const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => ({
  
});

export default connect(mapStateToProps, mapDispatchToProps)(DemoContainer);
