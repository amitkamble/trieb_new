import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors, fonts, sizes } from '../../utils/theme';

import Icon from 'react-native-vector-icons/Ionicons';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch'
  }, 
  sub_container: {
    flex:1,
    backgroundColor : colors.view_color_new
  },
  sub_row_container: {
    flexDirection:'row',
  },
  white_row_container: {
    marginLeft:5,
    marginRight:5,
    marginBottom:10,
    height:height/2.5
  },
  back_image: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection:'row',
    height: Platform.OS == 'ios'? 130:110,
    width: width,
    position:'absolute',
  },
  back_image1: {
    flexDirection:'row',
    height: Platform.OS == 'ios'? 125:100,
    width: width,
    alignItems:'center',

  },

  HedingText: {
    marginTop: 30,
    fontFamily: fonts.SemiBold,
    fontSize: sizes.regularLarge,
    color: colors.main_dark_color,
    marginLeft: 25
  },

  inputText: {
    fontSize: sizes.regularLarge,
    fontFamily: fonts.Regular,
    paddingLeft: 5,
    alignSelf: 'center',
    marginLeft: 10,
    flex:1,
    // textAlign:'center',
    borderBottomColor: colors.white,
    color: colors.black,
    borderBottomWidth: 1,
  },

  logintext: {
    color: colors.black,
    margin:15,
    fontSize: sizes.medium,
    fontFamily: fonts.SemiBold,
  },

  logintext_red: {
    color: colors.main_dark_color,
    fontSize: sizes.small,
    fontFamily: fonts.SemiBold,
  },

  touchable_opacity: {
    margin:15,
    position:'absolute',
    right:0,
    },

  banner_heading: {
    color: colors.text_color_semi_black,
    marginTop:20,
    marginLeft:20,
    marginBottom:10,
    fontSize: 12,
    fontFamily: fonts.SemiBold,
  },

  banner_desc: {
    color: colors.text_color_semi_black,
    marginTop:10,
    marginLeft:10,
    marginBottom:10,
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    flex:1
  },

  banner_name: {
    color: colors.black,
    marginTop:10,
    marginLeft:10,

    fontSize: sizes.small,
    fontFamily: fonts.SemiBold,
  },

  banner_designation: {
    color: colors.dark_gray,
    fontSize: 11,
    marginLeft:10,
    fontFamily: fonts.SemiBold,
  },

  date_designation: {
    color: colors.dark_gray,
    fontSize: 11,
    marginLeft:10,
    fontFamily: fonts.SemiBold,
    textAlign:'right'
  },

  vehicle_designation: {
    color: colors.black,
    marginTop:10,
    marginLeft:10,

    fontSize: sizes.small,
    fontFamily: fonts.SemiBold,
    textAlign:'right'
  },

  banner_text: {
    color: colors.black,
    marginLeft:5,
    marginRight:5,
    fontSize: sizes.small,
    fontFamily: fonts.Regular,
    flexWrap: 'nowrap',
    flexShrink: 1
  },

  banner_last_text: {
    color: colors.dark_gray,
    margin:5,
    fontSize: sizes.small,
    fontFamily: fonts.SemiBold,
    flexWrap: 'nowrap',
    flexShrink: 1
  },

  banner_2ndlast_text: {
    color: colors.gray,
    margin:5,
    fontSize: sizes.small,
    fontFamily: fonts.SemiBold,
    flexWrap: 'nowrap',
    flexShrink: 1
  },

  login: {
    flexDirection: 'row',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 45,
    width:width-155,
    alignSelf: 'center',
    borderRadius: 10,
    marginRight: 10,
    marginTop: Platform.OS == 'ios' ? '5%' :35 ,
    marginLeft:70,
    backgroundColor: colors.white,
    paddingRight:10,
  },

  text_regular_14: {
    color: colors.white,
    marginTop: 20,
    fontSize: sizes.regularLarge,
    fontFamily: fonts.Regular
    // fontFamily:fonts.Bold
  },

  logo: {
    right:0,
    height: 10,
    width: 15,
    position:'absolute',
    marginRight:40,
    alignSelf:'center'
  },

  logo_pass: {
    right:0,
    height: 15,
    width: 13,
    position:'absolute',
    marginRight:40,
    alignSelf:'center'
  },
  
  back_image2: {
    height: 20,
    width: 20,
    marginLeft:20,
  },

  back_image3: {
    height: 20,
    width: 20,
    right:0,
    marginLeft:5,
    marginRight:5,
  },

  nav_image2: {
    height: 22,
    width: 20,
  },
  banner_image: {
    height: 75,
    width: 85,
    margin:5,
    borderRadius:10
  },
  nav_image3: {
    height: 20,
    width: 20,
  },
  spinnerTextStyle: {
    color: '#fff'
  },
  modalBackground: {
    flex: 1,
    width:width,
    height:height,
    marginRight:-20,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  
  profileImg1: {
    height: 50,
    width: 50,
    borderRadius: 25,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft:20
  },

  bookmarks: {
    height: 20,
    width: 15,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight:20, marginTop:15,
    marginLeft:10
  },

  down: {
    height: 5,
    width: 8,
    marginLeft:-20
  },
  like: {
    height: 18,
    width: 15,
    marginRight:5
  },
  
  container_view_que: {
    paddingTop:10,
    fontSize: sizes.regularLarge,
    height: 85,
    borderRadius: 10,
    paddingRight: 10,
    marginTop:10,
    margin:5,
    alignContent:'flex-start',
    borderColor:colors.main_dark_color,
    borderWidth:1,
  },
  
  
  inputText: {
    fontSize: sizes.medium,
    fontFamily: fonts.Regular,
    paddingLeft: 5,
    textAlignVertical:'top',
    marginLeft: 10,
    flex:1,
    // textAlign:'center',
    borderBottomColor: colors.transparent,
    color: colors.black,
    borderBottomWidth: 0,
  },
});
