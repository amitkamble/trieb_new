import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, Image, StyleSheet, FlatList, Alert, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import { colors, fonts, sizes } from '../../utils/theme';
import * as geolib from 'geolib';
import RoundedCheckbox from "react-native-rounded-checkbox";
import DocumentPicker from 'react-native-document-picker';
import languages from '../../globals/languages';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height


const CheckListComponent = memo((props) => {

  const ScanScreen = () => {
    props.ScanScreen();
  }

  const [singleFile, setSingleFile] = useState(null);
  const [selected_name, set_selected_name] = useState('');


  const selectFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      console.log(JSON.stringify(res))
      if (res.type.includes('video')) {
        set_selected_name('')

        Alert.alert('You cannot upload video')
        setSingleFile(null)

      } else if (res.type.includes('audio')) {
        set_selected_name('')
        Alert.alert('You cannot upload audio')
        setSingleFile(null)
      } else if (res.size > 9999999) {
        set_selected_name('')
        Alert.alert('You cannot upload more than 10MB')
        setSingleFile(null)
      } else {
        set_selected_name(res.name);
        setSingleFile(res);
        // alert('selected' + res.name)
      }

    } catch (err) {
      setSingleFile(null);
      if (DocumentPicker.isCancel(err)) {
      } else {
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  }

  const onBack = () => {
    props.onBack();
  }

  const renderItemDr = ({ item, index }) => (

    <View style={{ flexDirection: 'row', width: width - 50, paddingLeft: 20, padding: 20, alignItems: 'center' }}>
      <RoundedCheckbox text={''} innerSize={25}
        outerSize={25}
        isChecked={item.isSelected}
        checkedColor={colors.green_text_coot}
        component={
          <Image source={Images.correct} style={styles.pop_up_correct} />

        }
        onPress={(checked) => props.setSelected(item, index, checked)} />
      <Text style={{ fontFamily: fonts.SemiBold, marginLeft: 25, marginTop: 3, fontSize: 16 }}>{item.name}</Text>

    </View>

  );

  return (
    <View style={{ flex: 1 }}>

      <View style={styles.logo_map} >

        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
          <TouchableOpacity onPress={() => onBack()} style={{
            marginLeft: 15,
          }}>
            <Image source={Images.back} style={styles.logo_back} />
          </TouchableOpacity>
          <Text style={styles.logintitle}>{Language.lang.checklist}</Text>
          {
            props.isCovidButton ?
              <TouchableOpacity onPress={() => onBack()} style={{
                position: 'absolute',
                right: 0,
                marginRight: 10
              }}>
                <TouchableOpacity onPress={() => props.setDocEnable()} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={styles.card_add_new_white}>{Language.lang.upload_docs}</Text>
                </TouchableOpacity>
              </TouchableOpacity>
              : null}
        </View>
      </View>

      <ScrollView contentContainerStyle={styles.container}>
        <View style={{ flexDirection: 'row', marginLeft: 20, width: width - 40, marginBottom: 80, backgroundColor: 'white', padding: 20, marginTop: 20 }}>
          <FlatList
            data={props.data}
            renderItem={renderItemDr}
            keyExtractor={item => item.scheme_code}
          />
        </View>
      </ScrollView>

      {
        props.isCovid ? <View style={{ height: height, opacity: 0.8, width: width, backgroundColor: colors.background_opacity, position: 'absolute' }} />
          : null
      }
      {
        !props.isCovid ?
          null
          :

          <View style={styles.view_import_program}>

            <TouchableOpacity style={{ alignSelf: 'center', margin: 20 }}>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Image source={Images.loyalty_prog} style={styles.logo_icon_country} />
                <Text style={styles.From_country_name}>{languages.lang.add_covid_doc}</Text>
              </View>
            </TouchableOpacity>

            <Text style={styles.inputTextHint}>{languages.lang.doc_uploaded}</Text>

            <View style={{ height: 1, backgroundColor: colors.hint_color, marginLeft: 15, marginRight: 15, marginTop: 10 }} />

            <TouchableOpacity onPress={() => ScanScreen()} style={{ paddingLeft: 30 }}>
              <View style={{ flexDirection: 'row', flex: 1, marginTop: 20 }}>
                <Image source={Images.scan} style={styles.logo_icon_account} />
                <Text style={styles.add_program}>{languages.lang.scan_pass}</Text>
              </View>
            </TouchableOpacity>

            <View style={{ height: 1, backgroundColor: colors.hint_color, margin: 15 }} />
            <TouchableOpacity onPress={() => selectFile()} style={{ paddingLeft: 30 }}>
              <View style={{ flexDirection: 'row', flex: 1, marginTop: 10 }}>
                <Image source={Images.travel_doc} style={styles.logo_icon_account} />
                <Text style={styles.add_program}>{languages.lang.upload_docs}</Text>
              </View>
            </TouchableOpacity>

            <View style={{ height: 1, backgroundColor: colors.hint_color, marginLeft: 15, marginRight: 15, marginTop: 20 }} />

            {
              selected_name != '' ?
                <Text style={styles.inputTextRed}>{languages.lang.you_have_selected + selected_name + ' )'}</Text>
                : null
            }
            <Text style={styles.inputTextGray}>{languages.lang.nothing_uploaded}</Text>

            <TouchableOpacity onPress={() => props.setCovidVisa(false)} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 20, justifyContent: 'center' }}>
              <Text style={styles.card_add_new}>{Language.lang.cancel}</Text>

            </TouchableOpacity>
          </View>

      }
    </View>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default CheckListComponent;
