import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import CheckListComponent from './CheckListComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const CheckListContainer = (props) => {

  const { navigation, clearUserObj, route } = props;
  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }

    // Setting 
    const [isLoading, setLoading] = React.useState(false);
    const [isCovid, setCovidVisa] = React.useState(false);
    const [isCovidButton, setCovidVisaButton] = React.useState(false);
    const [data, setChecklist] = React.useState([
      {
        name: 'Personal information',
        isSelected: false,
        isVisible: true
      },
      {
        name: 'Covid Visa',
        isSelected: false,
        isVisible: true
      },
      {
        name: 'Check visa',
        isSelected: false,
        isVisible: true
      },
      {
        name: 'Get your visa well in advance',
        isSelected: false,
        isVisible: true
      },
      {
        name: 'Book a hotel',
        isSelected: false,
        isVisible: true
      },
      {
        name: 'Book airport transfer service',
        isSelected: false,
        isVisible: true
      },
      {
        name: 'Order foreign currency',
        isSelected: false,
        isVisible: true
      },
      {
        name: 'Purchase travel insurance',
        isSelected: false,
        isVisible: true
      },
      {
        name: 'Insurance of documents',
        isSelected: false,
        isVisible: true
      },
      {
        name: 'sunglasses',
        isSelected: false,
        isVisible: true
      },
      {
        name: 'Other',
        isSelected: false,
        isVisible: true
      },
    ]);
  
  
  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
    navigation.navigate('LoginContainer')
  };

  const ScanScreen = () => {
    navigation.navigate('ScanScreen')
  };

  const onBack = () => {
    navigation.goBack();
  };

  const setSelected = (item, index, status) => {
    data.map((item1) => {
        if (item1.name == item.name) {
          data[index] = { ...item1, isSelected: status }
        }         
    })

    if (item.name == 'Covid Visa') {
      if(status){
        setCovidVisa(true)
        setCovidVisaButton(true)
        console.log('true')
      }else{
        setCovidVisa(false)
        console.log('false')
        setCovidVisaButton(false)
      }
    } 
    var newData = data;
    setChecklist([])
    setChecklist(newData)
  }

  const setDocEnable = () => {
    setCovidVisa(true)
  }

  return (
    <CheckListComponent props={props} login_button_pressed={login_button_pressed} onBack={onBack} isCovidButton={isCovidButton}
    ScanScreen={ScanScreen} data={data} setSelected={setSelected} isCovid={isCovid} setCovidVisa={setCovidVisa}
    setDocEnable={setDocEnable}/>

  );
};
CheckListContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
});


export default connect(mapStateToProps, mapDispatchToProps)(CheckListContainer);
