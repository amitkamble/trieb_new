import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import PolylineDirection from '@react-native-maps/polyline-direction';
import MapView, { Polyline, PROVIDER_GOOGLE } from 'react-native-maps';
import * as geolib from 'geolib';
import { ProgressBar, Colors } from 'react-native-paper';
import { TabView, SceneMap } from 'react-native-tab-view';
import languages from '../../globals/languages';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const ReviewsComponent = memo((props) => {

  const [isVisible, setVisiable_Pop] = useState(false);


  //to check and set focus error field or password  
  const onBack_click = () => {
    props.onBack_click();
  };




  return (
    <ScrollView contentContainerStyle={styles.container}>

      <View style={{
        height: 100,
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
      }}>
        <View style={styles.back_image} />


        <TouchableOpacity onPress={() => onBack_click()} style={{
          marginLeft: 15,
          marginTop: 20
        }}>
          <Image source={Images.back} style={styles.logo_back} />
        </TouchableOpacity>
        <Text style={styles.tab_bar_text}>{Language.lang.flight_reviews}</Text>
        <Text style={styles.card_add_new}>{'10 ' +languages.lang.reviews}</Text>

      </View>

      <View style={{ marginTop: 20 }}>

        <View style={styles.view_flight_card}>


        <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20, marginRight: 20 }}>
        <Text style={{ color: colors.main_color_dark, fontFamily: fonts.SemiBold, }}>AA 3 JFK-LAX</Text>


            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', flex: 1 }}>
              <Text style={styles.From_country_name}>{'2 Days ago'}</Text>
            </View>

          </View>
          <Text style={styles.From_like_text}>{'Best Flight i ever seen'}</Text>

          <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20,marginBottom: 20, marginRight: 20 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
              <Image source={Images.profile_image} style={styles.logo_icon_country} />
              <Text style={styles.From_country_name}>{'Jhon doe'}</Text>
            </View>

          </View>

        </View>

      </View>

    </ScrollView>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 0,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'white',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default ReviewsComponent;
