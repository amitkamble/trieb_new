import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, FlatList, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import PolylineDirection from '@react-native-maps/polyline-direction';
import MapView, { Polyline, PROVIDER_GOOGLE } from 'react-native-maps';
import * as geolib from 'geolib';
import { ProgressBar, Colors } from 'react-native-paper';
import { TabView, SceneMap } from 'react-native-tab-view';
import Moment from 'moment';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const MyTripsComponent = memo((props) => {

  const [state, setState] = React.useState({
    index: 0,
    routes: [
      { key: 'first', title: 'Upcoming' },
      { key: 'second', title: 'Past' }
    ]
  });

  // Storing Name & Pass
  const [user, set_user] = useState({
    pass: '',
    confirm_pass: '',
  });

  const view_all = (item) => {
    props.detailsPage(item);
  }

  const add_flight = () => {
    props.add_flight();
  }



  const renderItemDr = ({ item, index }) => (

    <TouchableOpacity onPress={() => view_all(item)}>

      <View style={{ flexDirection: 'row', marginRight: 20 }}>
        <View style={{ flex: 1 }}>
          <Text style={styles.card_date}>{Moment(item.take_of_date_time).format('DD MMM')}</Text>

        </View>

        <Text style={styles.Flight_time}>{item.journey_left}</Text>

      </View>
      <View style={styles.view_flight_card}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.from_time}>{Language.lang.from}</Text>
          <Text style={styles.to_time}>{Language.lang.to}</Text>
        </View>

        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.From_flight}>{item.code_name_from}</Text>
          <Image source={Images.flight_upcoming} style={styles.logo_icon_flight} />
          <Text style={styles.To_flight}>{item.code_name_to}</Text>
        </View>

        <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20, marginRight: 20 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
            <Image source={Images.coutry1} style={styles.logo_icon_country} />
            <Text style={styles.From_country_name}>{item.from_name}</Text>
          </View>

          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', flex: 1 }}>
            <Image source={Images.coutry1} style={styles.logo_icon_country} />
            <Text style={styles.From_country_name}>{item.to_name}</Text>
          </View>

        </View>

        <View style={{ flexDirection: 'row', marginTop: 15, marginBottom: 10, borderWidth: 1, padding: 10, borderStyle: 'dashed', borderRadius: 5, borderColor: colors.main_color_dark, marginLeft: 20, marginRight: 20 }}>
          <View style={{ flexDirection: 'column', flex: 1 }}>
            <Text style={styles.From_country_name}>{Language.lang.flight}</Text>
            <Text style={{ color: colors.main_dark_color, marginLeft: 5, marginTop: 5, fontFamily: fonts.Monsterat_SemiBold }}>{item.flight_name}</Text>
          </View>
          <View style={{ width: 1, backgroundColor: colors.main_color_dark, height: 15, position: 'absolute', marginTop: Platform.OS == 'ios' ? 30 : 35, marginLeft: 80 }} />
          <View style={{ flexDirection: 'column', flex: 1 }}>
            <Text style={styles.From_country_name}>{Language.lang.terminal}</Text>
            <Text style={{ color: colors.main_dark_color, marginLeft: 5, marginTop: 5, fontFamily: fonts.Monsterat_SemiBold }}>{item.from_terminal_no}</Text>
          </View>
          <View style={{ width: 1, backgroundColor: colors.main_color_dark, height: 15, position: 'absolute', marginTop: Platform.OS == 'ios' ? 30 : 35, marginLeft: 180 }} />

          <View style={{ flexDirection: 'column', flex: 1 }}>
            <Text style={styles.From_country_name}>{Language.lang.boarding}</Text>
            <Text style={{ color: colors.main_dark_color, marginLeft: 5, marginTop: 5, fontFamily: fonts.Monsterat_SemiBold }}>{item.boarding_time}</Text>
          </View>
        </View>

        <View style={{ height: 1, backgroundColor: colors.view_color, marginTop: 10 }} />
        <TouchableOpacity onPress={() => view_all(item)} style={styles.View_all_details}>
          <Text style={styles.logintext_button}>View all details</Text>
        </TouchableOpacity>
      </View>

    </TouchableOpacity>
  );


  const renderItemDrPast = ({ item, index }) => (
    <TouchableOpacity onPress={() => view_all(item)} style={{ marginTop: 10 }}>
      <View style={{ flexDirection: 'row', marginRight: 20 }}>
        <View style={{ flex: 1 }}>
          {/* <Text style={styles.card_date_past}>{'01  ' + Language.lang.May}</Text> */}
          <Text style={styles.card_date_past}>{Moment(item.take_of_date_time).format('DD MMM')}</Text>

        </View>


        {/* <Text style={styles.Flight_time}>{'5h 30m Left'}</Text> */}

      </View>
      <View style={styles.view_flight_card}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.from_time}>{Language.lang.from}</Text>
          <Text style={styles.to_time}>{Language.lang.to}</Text>
        </View>

        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.From_flight}>{item.code_name_from}</Text>
          <Image source={Images.flight_upcoming} style={styles.logo_icon_flight} />
          <Text style={styles.To_flight}>{item.code_name_to}</Text>
        </View>

        <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20, marginRight: 20 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
            <Image source={Images.coutry1} style={styles.logo_icon_country} />
            <Text style={styles.From_country_name}>{item.from_name}</Text>
          </View>

          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', flex: 1 }}>
            <Image source={Images.coutry1} style={styles.logo_icon_country} />
            <Text style={styles.From_country_name}>{item.to_name}</Text>
          </View>

        </View>
        <View style={{ flexDirection: 'row', marginTop: 10, marginLeft: 20, marginRight: 20, marginBottom: 10 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
            <Text style={{ color: colors.main_dark_color, marginLeft: 5, marginTop: 5, fontFamily: fonts.Monsterat_SemiBold }}>{item.take_of_time}</Text>
          </View>

          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', flex: 1 }}>
            <Text style={{ color: colors.main_dark_color, marginLeft: 5, marginTop: 5, fontFamily: fonts.Monsterat_SemiBold }}>{item.landing_time}</Text>
          </View>

        </View>

      </View>

    </TouchableOpacity>

  );


  const FirstRoute = (items) => (

    props.is_data_upcoming == true ?

      <View style={{ paddingBottom: 0 }}>
        {/* <Text style={styles.headingtitle}>{Language.lang.upcoming_flight}</Text> */}

        <FlatList
          data={props.data_upcomming}
          renderItem={renderItemDr}
          keyExtractor={item => item.trip_id}

        />
      </View>

      :
      <View>

        <Text style={styles.headingtitle}>{Language.lang.plan_trip}</Text>

        <View style={styles.view_flight_card}>
          <View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
              <Text style={styles.check_in}>{Language.lang.add_manul}</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
              <Text style={styles.on_time}>{Language.lang.add_desc}</Text>
            </View>
            <TouchableOpacity onPress={() => add_flight()} style={styles.View_add}>
              <Text style={styles.logintext_button}>{Language.lang.add_flight}</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.view_flight_card}>
          <View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
              <Text style={styles.check_in}>{Language.lang.share_invite}</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
              <Text style={styles.on_time}>{Language.lang.share_desc}</Text>
            </View>
            <TouchableOpacity onPress={() => console.log()} style={styles.View_add}>
              <Text style={styles.logintext_button}>{Language.lang.share}</Text>
            </TouchableOpacity>
          </View>
        </View>

      </View>

  );

  const SecondRoute = ({ items }) => (
    <FlatList
      data={props.data_past}
      renderItem={renderItemDrPast}
      keyExtractor={item => item.trip_id}

    />
  );

  const _handleIndexChange = index => setState({ ...state, ['index']: index });

  const _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => {

          return (

            <TouchableOpacity
              onPress={() => setState({ ...state, ['index']: i })}
              style={{ alignItems: "center", padding: 5 }}
            >

              <Text style={{ color: '#000', fontFamily: fonts.SemiBold }}>
                {route.title}
              </Text>
              <View style={{ height: 3, width: state.index == 0 ? 70 : 50, marginTop: Platform.OS == 'ios' ? 10 : 10, backgroundColor: state.index == i ? colors.main_color_dark : null }} />


            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  //to check and set focus error field or password  
  const onBack_click = () => {
    props.onBack_click();
  };



  const _renderScene = (New_data) => SceneMap({
    first: () => FirstRoute(New_data),
    second: () => SecondRoute(New_data),
  });


  return (
    <ScrollView contentContainerStyle={styles.container}>

      <View style={{
        height: 100,
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
      }}>
        <View style={styles.back_image} />


        <TouchableOpacity onPress={() => onBack_click()} style={{
          marginLeft: 15,
          marginTop: 20
        }}>
          <Image source={Images.back} style={styles.logo_back} />
        </TouchableOpacity>
        <Text style={styles.tab_bar_text}>{Language.lang.trips_card_case_sen}</Text>
        <TouchableOpacity onPress={() => add_flight()} style={{ flex: 1, flexDirection: 'row', position: 'absolute', right: 0 }}>
          <Text style={styles.card_add_new}>{Language.lang.add_flight}</Text>
          <Text style={{ fontSize: 25, position: 'absolute', fontFamily: fonts.Monsterat_Regular, right: 0, color: 'white', marginTop: 30, marginRight: 27 }}>{'+'}</Text>

        </TouchableOpacity>
      </View>
      {
        props.is_past_data == false ?

          props.is_data == true ?
            <View>
              {/* <Text style={styles.headingtitle}>{Language.lang.upcoming_flight}</Text> */}

              <FlatList
                data={props.data_upcomming}
                renderItem={renderItemDr}
                keyExtractor={item => item.trip_id}

              />
            </View>

            :
            <View>

              <Text style={styles.headingtitle}>{Language.lang.plan_trip}</Text>

              <View style={styles.view_flight_card}>
                <View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                    <Text style={styles.check_in}>{Language.lang.add_manul}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                    <Text style={styles.on_time}>{Language.lang.add_desc}</Text>
                  </View>
                  <TouchableOpacity onPress={() => add_flight()} style={styles.View_add}>
                    <Text style={styles.logintext_button}>{Language.lang.add_flight}</Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={styles.view_flight_card}>
                <View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                    <Text style={styles.check_in}>{Language.lang.share_invite}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                    <Text style={styles.on_time}>{Language.lang.share_desc}</Text>
                  </View>
                  <TouchableOpacity onPress={() => console.log()} style={styles.View_add}>
                    <Text style={styles.logintext_button}>{Language.lang.share}</Text>
                  </TouchableOpacity>
                </View>
              </View>

            </View>

          :
          <TabView
            navigationState={state}
            renderScene={state.index == 0 ? _renderScene(props.data_upcomming) : _renderScene(props.data_upcomming)}
            renderTabBar={_renderTabBar}
            onIndexChange={_handleIndexChange}
          />
      }


      <Spinner
        visible={props.isLoading}
        textContent={'Loading...'}
        color={'white'}
        overlayColor={'rgba(0, 0, 0, 0.45)'}
        textStyle={styles.spinnerTextStyle}
      />

    </ScrollView>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default MyTripsComponent;
