import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import MyTripsComponent from './MyTripsComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";

const MyTripsContainer = (props) => {

  const { navigation, clearUserObj, myTrips, my_trips } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };

  const [is_data, SetData_air] = useState(false);
  const [data_upcomming, setDataUpcoming] = useState([]);
  const [data_past, setDataPast] = useState([]);
  const [is_data_upcoming, SetUpcoming] = useState(false);
  const [is_past_data, SetPast] = useState(false);

  useEffect(() => {
    setLoading(false);
    const unsubscribe = navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        console.log("Connection type", state.type);
        console.log("Is connected?", state.isConnected);

        if (state.isConnected == true) {
          Get_MY_Trips();
          console.log('here get triips')
        } else {
          var data_response = myTrips.myTrips;
          console.log("offline_data" + JSON.stringify(data_response));
          if (data_response.upcoming.trips.length == 0) {
            SetUpcoming(false)
          } else {
            SetUpcoming(true)
            setDataUpcoming(data_response.upcoming.trips)
          }

          if (data_response.past.trips.length == 0) {
            SetPast(false)
          } else {
            SetPast(true)
            setDataPast(data_response.past.trips)

          }

          if (data_response.past.trips.length == 0 && data_response.upcoming.trips.length == 0) {
            SetData_air(false)
          } else {
            SetData_air(true)
          }
        }
      });
      BackHandler.addEventListener('hardwareBackPress', (handleBackButtonClick));
    });
    return unsubscribe;
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
    navigation.navigate('LoginContainer')
  };
  const onBack_click = () => {
    navigation.goBack();
  };
  const add_flight = () => {
    navigation.navigate('Add_Flight')
  };

  const detailsPage = (item) => {
    navigation.navigate('MyTripsDetails', { data_selected: item })
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);

  const Get_MY_Trips = async () => {
    setLoading(true);

    let data = new FormData();
    data.append("user_id", 1);

    await my_trips(data)
      .then((response) => {
        setLoading(false);
        var data_response = response.payload.data;
        console.log("api_login_Response" + JSON.stringify(data_response));
        if (data_response.upcoming.trips.length == 0) {
          SetUpcoming(false)
        } else {
          SetUpcoming(true)
          setDataUpcoming(data_response.upcoming.trips)
        }

        if (data_response.past.trips.length == 0) {
          SetPast(false)
        } else {
          SetPast(true)
          setDataPast(data_response.past.trips)

        }

        if (data_response.past.trips.length == 0 && data_response.upcoming.trips.length == 0) {
          SetData_air(false)
        } else {
          SetData_air(true)
        }

      }).catch(error => {
        console.log('error' + JSON.stringify(error));
        Alert.alert(error.error.data)
        setLoading(false);
      });
  }

  return (
    <MyTripsComponent props={props} login_button_pressed={login_button_pressed} onBack_click={onBack_click}
      add_flight={add_flight} detailsPage={detailsPage} isLoading={isLoading} is_data={is_data} is_data_upcoming={is_data_upcoming}
      is_past_data={is_past_data} data_upcomming={data_upcomming} data_past={data_past} />

  );
};
MyTripsContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession, myTrips }) => ({
  dashboard,
  userObj,
  myTrips,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
  my_trips: (params) => dispatch(Actions.my_trips(params)),

});


export default connect(mapStateToProps, mapDispatchToProps)(MyTripsContainer);
