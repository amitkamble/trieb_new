import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors, fonts, sizes } from '../../utils/theme';

import Icon from 'react-native-vector-icons/Ionicons';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default StyleSheet.create({
  container: {
    flexGrow: 1,
  },

  logo_map: {
    height:600,
    width: width+40,
    position: 'absolute',
  },

  logo_lock: {
    height: 30,
    width: 30,
    position: 'absolute',
    right: 0,
    marginTop: 70,
    marginRight: 20
  },

  logo_icon: {
    height: 35,
    width: 35,
    marginBottom:20
  },

  logo_icon_message: {
    height: 32,
    width: 38.5,
    marginBottom:20
  },

  logo_icon_flight: {
    height: 42,
    width:150,
    resizeMode:'contain',
    position : 'absolute',
    marginLeft:90
  },

  logo_icon_country: {
    width:15,
    height: 8,
    alignSelf:'center',
    marginTop:3
  },

  logo_icon_next: {
    width:15,
    height: 15,
    marginTop:123,
    marginRight:120
  },

  logo_icon_pre: {
    width:15,
    height: 15,
    marginTop:123,
    marginLeft:120
  },

  logo_back: {
    height: 13,
    width: 18,
  },

  back_image: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    height: 100,
    width: width,
    marginBottom: 40,
    position:'absolute',
    backgroundColor: colors.main_dark_color
  },

  logintitle: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.SemiBold,
    alignSelf: 'center',
    marginTop: 70,
    marginLeft: 20
  },

  From_flight: {
    color: colors.main_dark_color,
    fontSize: 24,
    fontFamily: fonts.SemiBold,
    marginTop: 5,
    marginLeft: 20,
    flex:1,
    textTransform: 'uppercase'
  },

  From_country_name: {
    color: colors.main_dark_color,
    fontSize: 12,
    fontFamily: fonts.Regular,
    marginLeft: 5,
  },

  Flight_time: {
    color: colors.dark_gray,
    fontSize: 12,
    fontFamily: fonts.Monsterat_SemiBold,
    marginLeft: 5,
    flex:1,
    textAlign:'right',
    alignSelf:'center',
    marginTop:15
  },

  journey_hours: {
    color: colors.main_dark_color,
    fontSize: 18,
    fontFamily: fonts.Monsterat_Regular,
    marginLeft: 5,
    textAlignVertical:'top'
  },

  To_flight: {
    color: colors.main_dark_color,
    fontSize: 24,
    fontFamily: fonts.SemiBold,
    marginTop: 5,
    paddingRight: 40,
    flex:1,
    textAlign:'right',
    textTransform: 'uppercase'
  },

  headingtitle: {
    color: colors.black,
    fontSize: 16,
    fontFamily: fonts.SemiBold,
    marginTop: 30,
    marginLeft: 20,
    marginBottom:20
  },

  tabBar: {
    flexDirection: 'row',
    marginLeft:10,
    marginTop:20
  },
  card_date: {
    color: colors.white,
    fontSize: 14,
    backgroundColor:colors.main_color_dark,
    fontFamily: fonts.Monsterat_SemiBold,
    paddingLeft:10,
    paddingRight:10,
    width:80,
    marginLeft:20,
    paddingTop:Platform.OS=='ios'? 8 : 0,
    textAlignVertical:'center',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    marginTop:20,
    height:34,
    textAlign:'center'
  },
  card_add_new: {
    color: colors.white,
    borderColor:'white', borderWidth:1,
      fontSize: 14,
    fontFamily: fonts.Regular,
    paddingLeft:10,
    paddingRight:10,
    width:110,
    marginLeft:20,
    marginRight:20,
    paddingTop:Platform.OS=='ios'? 8 : 0,
    textAlignVertical:'center',
    marginTop:30,
    height:34,
  },
  card_date_past: {
    color: colors.white,
    fontSize: 14,
    backgroundColor:colors.gray,
    fontFamily: fonts.Monsterat_SemiBold,
    paddingLeft:10,
    paddingRight:10,
    width:80,
    marginLeft:20,
    paddingTop:Platform.OS=='ios'? 8 : 0,
    textAlignVertical:'center',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    marginTop:20,
    height:34,
  },

  card_fight_name: {
    color: colors.main_dark_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    marginTop:20,
    alignSelf:'center',
    paddingLeft:20,
    paddingRight:20
  },

  from_time : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    marginTop:20,
    paddingLeft:20,
    flex:1
  },

  to_time : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    marginTop:20,
    paddingRight:70,
    flex:1,
    textAlign:'right'
  },

  check_in : {
    color: colors.black,
    fontSize: 16,
    fontFamily: fonts.Monsterat_Medium,
    textAlignVertical:'center',
    paddingLeft:20,
    flex:1
  },

  check_in_time : {
    color: colors.hint_color,
    fontSize: 14,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    paddingRight:25,
    flex:1,
    textAlign:'right'
  },
  on_time : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft:20,
    textAlignVertical:'center',
    flex:1,
  },
  terminal : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft:10,
    textAlignVertical:'center',
  },
  terminal_no : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft:5,
    textAlignVertical:'center',
  },
  card_title: {
    color: colors.main_dark_color,
    fontSize: sizes.regular,
    fontFamily: fonts.SemiBold,
  },

  view_home_card: {
    flex: 1,
    margin: 10,
    height: 140,
    backgroundColor: 'white',
    borderRadius: 5,
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 8,
    alignItems:'center',
    justifyContent:'center'
  },

  view_flight_card: {
    margin: 20,
    backgroundColor: 'white',
    shadowColor: colors.gray,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 8,
    marginTop:0,
  },

  View_all: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 50,
    paddingLeft:50,
    paddingRight:50,
    alignSelf: 'center',
    borderRadius: 5,
    marginTop: 30,
    marginBottom:20,
    backgroundColor: colors.main_dark_color,
  },

  View_all_details: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 40,
    paddingLeft:30,
    paddingRight:30,
    alignSelf: 'center',
    borderRadius: 5,
    marginTop: 10,
    marginBottom:10,
    backgroundColor: colors.main_dark_color,
  },

  View_add: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 50,
    width:115,
    alignSelf: 'flex-start',
    borderRadius: 5,
    marginTop: 15,
    marginLeft:20,
    marginBottom:20,
    backgroundColor: colors.main_dark_color,
  },

  logintext_button: {
    color: colors.white,
    fontSize: sizes.regular,
    fontFamily: fonts.SemiBold,
    alignSelf:'center',
  },

  tab_bar_text: {
    color: colors.white,
    fontSize: sizes.regular16,
    fontFamily: fonts.SemiBold,
    marginTop:20,
    marginLeft:20
  },
  spinnerTextStyle: {
    color: '#fff'
  },
});
