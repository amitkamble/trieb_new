import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import LeaderboardComponent from './LeaderboardComponent';
import { Alert, BackHandler, NativeModules, } from 'react-native';

const LeaderboardContainer = (props) => {

  const { navigation,  clearUserObj, route } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };
  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }

  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
      navigation.navigate('LoginContainer')
  };
  const onBack_click = () => {
    navigation.goBack();
  };
  const add_flight = () => {
    navigation.navigate('Add_Flight')
  };

  const detailsPage = () => {
    navigation.navigate('MyTripsDetails')
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);



  return (
    <LeaderboardComponent props={props} login_button_pressed={login_button_pressed} onBack_click={onBack_click} 
    add_flight={add_flight} detailsPage={detailsPage}/>

  );
};
LeaderboardContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
});


export default connect(mapStateToProps, mapDispatchToProps)(LeaderboardContainer);
