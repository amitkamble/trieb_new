import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import { colors, fonts, sizes } from '../../utils/theme';
import RNPickerSelect from 'react-native-picker-select';
import { TabView, SceneMap } from 'react-native-tab-view';
import languages from '../../globals/languages';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const LeaderboardComponent = memo((props) => {

  const [state, setState] = React.useState({
    index: 0,
    routes: [
      { key: 'first', title: languages.lang.friends },
      { key: 'second', title: languages.lang.worldwide },
    ]
  });

  const sports = [
    {
      label: languages.lang.food,
      value: languages.lang.food,
    },
    {
      label: languages.lang.wifi,
      value: languages.lang.wifi,
    },
    {
      label: languages.lang.outlets,
      value: languages.lang.outlets,
    },
    {
      label: languages.lang.transportaion,
      value: languages.lang.transportaion,
    },
    {
      label: languages.lang.shops,
      value: languages.lang.shops,
    },
    {
      label: languages.lang.services,
      value: languages.lang.services,
    },
  ];

  const [airline, set_airline] = useState({
    name: '',
  });

  const onCancel_press = () => {
    setVisiable_Pop(false)
  }
  const [data] = useState(true);
  const [data_upcoming] = useState(true);
  const [past_data] = useState(true);


  // Storing Name & Pass
  const [user, set_user] = useState({
    username: '',
    email: '',
    pass: '',
    confirm_pass: '',
    role: '',
  });

  const view_all = () => {
    props.detailsPage();
  }

  const add_flight = () => {
    props.add_flight();
  }
  //to check and set focus error field or password  
  const here_select_role = (value) => {
    set_user({ ...user, ['role']: value })

  };
  const [isVisible, setVisiable_Pop] = useState(false);

  const FirstRoute = () => (

    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} >


      <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 10, marginBottom: 10 }}>
        <Text style={styles.on_time}>{Language.lang.add_frinds}</Text>
      </View>
      <TouchableOpacity onPress={() => view_all()} style={styles.View_add}>
        <Text style={styles.logintext_button}>{Language.lang.sign_in}</Text>
      </TouchableOpacity>
      <View style={{ flexDirection: 'row', alignSelf: 'center', }}>
        <Text style={styles.on_time}>{Language.lang.or}</Text>
      </View>

      <TouchableOpacity onPress={() => view_all()} style={styles.View_add_invite}>
        <Text style={styles.logintext_button_dark}>{Language.lang.invite_friends}</Text>
      </TouchableOpacity>

    </View>
  );

  const SecondRoute = () => (
    <View style={{ marginTop: 20 }}>

      <View style={styles.view_flight_card}>

        <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20, marginRight: 20 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
            <Image source={Images.profile_image} style={styles.logo_icon_country} />
            <View style={{ flexDirection: 'column', justifyContent: 'flex-end', flex: 1 }}>
              <Text style={styles.From_country_name}>{'#60 061'}</Text>
              <Text style={{ color: colors.main_color_dark, fontFamily: fonts.SemiBold, marginLeft: 20 }}>{'Pushkraj Jadhav'}</Text>
              <Text style={styles.From_country_name1}>{'3,982 Km, 6 hrs'}</Text>

            </View>
          </View>

        </View>

      </View>

    </View>
  );

  const _handleIndexChange = index => setState({ ...state, ['index']: index });

  const _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View horizontal style={styles.tabBar}>
        <ScrollView horizontal>

          {props.navigationState.routes.map((route, i) => {

            return (

              <TouchableOpacity
                onPress={() => setState({ ...state, ['index']: i })}
                style={{ alignItems: "center", padding: 5 }}
              >

                <Text style={{ color: '#000', fontFamily: fonts.SemiBold }}>
                  {route.title}
                </Text>
                <View style={{ height: 3, width: 80, marginTop: Platform.OS == 'ios' ? 10 : 10, backgroundColor: state.index == i ? colors.main_color_dark : null }} />


              </TouchableOpacity>
            );
          })}
        </ScrollView>

      </View>
    );
  };

  //to check and set focus error field or password  
  const onBack_click = () => {
    props.onBack_click();
  };



  const _renderScene = () => SceneMap({
    first: () => FirstRoute(),
    second: () => SecondRoute(),
  });


  return (
    <ScrollView contentContainerStyle={styles.container}>

      <View style={{
        height: 100,
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
      }}>
        <View style={styles.back_image} />


        <TouchableOpacity onPress={() => onBack_click()} style={{
          marginLeft: 15,
          marginTop: 20
        }}>
          <Image source={Images.back} style={styles.logo_back} />
        </TouchableOpacity>
        <Text style={styles.tab_bar_text}>{Language.lang.leaderboard}</Text>
        <View style={{ flex: 1 }}>
          
          <TouchableOpacity onPress={() => console.log()} style={{ flexDirection: 'row', alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
            <Image source={Images.add_user} style={styles.logo_add_user} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => console.log('here')} style={{ flexDirection: 'row', alignSelf: 'center', position: 'absolute', right: 0, marginRight: 60 }}>
            <Image source={Images.share} style={styles.logo_share_} />
          </TouchableOpacity>
        </View>

      </View>
      {
        isVisible ? <View style={{ height: height, opacity: 0.8, width: width, backgroundColor: colors.background_opacity, position: 'absolute' }} />
          : null
      }
      {
        !isVisible ?
          past_data == false ?

            null
            :
            <TabView
              navigationState={state}
              renderScene={_renderScene()}
              renderTabBar={_renderTabBar}
              onIndexChange={_handleIndexChange}
            />
          :

          <View style={styles.view_import_program}>

            <TouchableOpacity style={{ margin: 20 }}>
              <View style={{ flexDirection: 'row', flex: 1, marginTop: 10 }}>
                <Image source={Images.loyalty_prog} style={styles.logo_icon_pop_up} />
                <Text style={styles.add_program}>{languages.lang.add_program}</Text>
              </View>
            </TouchableOpacity>

            <TextInput
              placeholder={languages.lang.new_tip}
              onChangeText={(value) => set_airline({ ['airline']: value })}
              value={airline.name}
              placeholderTextColor={colors.gray}
              autoCorrect={false}
              style={styles.inputText} />

            <View style={styles.container_viewtop}>

              <RNPickerSelect
                placeholder={{ label: languages.lang.select_category, value: '', color: colors.dark_gray }}
                onValueChange={(value) => here_select_role(value)}
                items={sports}
                style={pickerSelectStyles}
                Icon={() => {
                  return <Image source={Images.dropdown_arrow} style={styles.drop_down} />
                }}
              />
              <View style={{ width: width - 40, height: 1, backgroundColor: 'gray' }} />

            </View>


            <View style={{ flexDirection: 'row', marginTop: 35, marginBottom: 40, borderWidth: 1, padding: 10, borderStyle: 'dashed', borderRadius: 5, borderColor: colors.main_color_dark, marginLeft: 20, marginRight: 20 }}>
              <View style={{ flexDirection: 'column', flex: 1 }}>
                <Image source={Images.attach_image} style={styles.logo_attach_pop_up} />
                <Text style={styles.From_country_name}>{Language.lang.select_image}</Text>
              </View>

            </View>
            <View style={{ flexDirection: 'row', flex: 1 }}>
              <TouchableOpacity onPress={() => onCancel_press()} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.card_add_new_button}>{Language.lang.cancel}</Text>

              </TouchableOpacity>
              <TouchableOpacity onPress={() => onCancel_press()} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.View_add_pop}>{Language.lang.send}</Text>

              </TouchableOpacity>
            </View>



          </View>

      }
    </ScrollView>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 0,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'white',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: width,
    padding: 20,
    marginLeft: 30,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default LeaderboardComponent;
