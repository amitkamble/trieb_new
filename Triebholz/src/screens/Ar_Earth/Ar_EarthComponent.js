import React, { memo, useState, useRef } from 'react';
import { View, Fragment, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import { colors, fonts, sizes } from '../../utils/theme';
import * as geolib from 'geolib';
import languages from '../../globals/languages';
import SearchableDropdown from 'react-native-searchable-dropdown';
import DatePicker from 'react-native-datepicker'
import Moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
// import {
//   ViroARScene,
//   ViroText,
// } from 'react-viro';


var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const Ar_EarthComponent = memo((props) => {

  return (
    // <ViroARScene  >
    //   <ViroText text={'this.state.text'} scale={[.5, .5, .5]} position={[0, 0, -1]} style={styles.helloWorldTextStyle} />
    // </ViroARScene>

    <View/>
  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default Ar_EarthComponent;
