import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Ar_EarthComponent from './Ar_EarthComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const Ar_EarthContainer = (props) => {

  const { navigation,  clearUserObj, route,getAirlines } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };
  const { isComeFrom, } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }

  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
      navigation.navigate('LoginContainer')
  };

  const ScanScreen = () => {
      navigation.navigate('ScanScreen')
  };

  const add_flight_select = () => {
      navigation.navigate('Add_Flight_manually')
  };

  const manage_link_ac = () => {
      navigation.navigate('Import_flight')
  };

  const onData_select = (request) => {
      navigation.navigate('Select_FlightContainer',{request_data : request})
  };

  const onBack = () => {
      navigation.goBack();
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);
  const [airline_data, setAirlines] = React.useState([]);


  const get_Airlines = async () => {
    setLoading(true);

    await getAirlines()
      .then(response => {
        var temp = [];
        setLoading(false);
        response.payload.data.data.map(item => {
          temp.push({ id: item.airline_id, name: item.codeIataAirline })
        });
        setAirlines(temp)
      })
      .catch(error => {
        console.log('error' + JSON.stringify(error));
        // Alert.alert(error)
        setLoading(false);
      })
  };

  return (
    <Ar_EarthComponent props={props} login_button_pressed={login_button_pressed} onBack={onBack} 
    ScanScreen={ScanScreen} add_flight_select={add_flight_select}manage_link_ac={manage_link_ac}
    onData_select={onData_select} airline_data={airline_data} isLoading={isLoading}/>

  );
};
Ar_EarthContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
  getAirlines: () => dispatch(Actions.getAirlines()),
});


export default connect(mapStateToProps, mapDispatchToProps)(Ar_EarthContainer);
