import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors, fonts, sizes } from '../../utils/theme';

import Icon from 'react-native-vector-icons/Ionicons';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default StyleSheet.create({
  container: {
    flexGrow: 1,
    marginTop:150,
  },
  container1: {
    flexGrow: 1,
  },
  helloWorldTextStyle: {
    fontFamily: 'Arial',
    fontSize: 30,
    color: '#ffffff',
    textAlignVertical: 'center',
    textAlign: 'center',
  },
  
});
