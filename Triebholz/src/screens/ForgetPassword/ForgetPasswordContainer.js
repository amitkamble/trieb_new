import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import ForgetPasswordComponent from './ForgetPasswordComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const ForgetPasswordContainer = (props) => {

  const { navigation,  clearUserObj, route } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };
  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }

  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const send_otp = (mob_no, code) => {
    var mobile_no = code +'-'+ mob_no
    navigation.navigate('EnterOTP',{mobile_no})
  };

  const sign_up = () => {
    navigation.navigate('SignUp')
  };

  const onBack_click = () => {
    navigation.goBack();
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);



  return (
    <ForgetPasswordComponent props={props} onBack_click={onBack_click} send_otp={send_otp} isLoading={isLoading} sign_up={sign_up} />

  );
};
ForgetPasswordContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
});


export default connect(mapStateToProps, mapDispatchToProps)(ForgetPasswordContainer);
