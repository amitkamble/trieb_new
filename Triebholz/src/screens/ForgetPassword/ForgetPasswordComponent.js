import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView } from 'react-native';
import styles from './styles';
import { Images } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const ForgetPasswordComponent = memo((props) => {

  // Sign IN Button Pressed
  const onSignupclick = () => {
    props.sign_up();

  }
  const sendOtp = () => {

    if (user.mobile_no.trim().length) {
      if (user.mobile_no.trim().length == 10) {
        if (/^\d+$/.test(user.mobile_no)) {

          setFocus_user_error({ ...on_user_error, ['mobile_no']: false })
          props.send_otp(user.mobile_no, user.code);
        } else {
          setFocus_user_error({ ...on_user_error, ['mobile_no']: true })

        }
      } else {
        setFocus_user_error({ ...on_user_error, ['mobile_no']: true })

      }

    } else {
      setFocus_user_error({ ...on_user_error, ['mobile_no']: true })
    }
  };

  // Storing Name & Pass
  const [user, set_user] = useState({
    mobile_no: '',
    code: '+91'
  });

  //to set Focus for Username Field
  const [on_user, setFocus_user] = useState(false);

  //to set Focus for Password Field
  const [on_pass, setFocus_pass] = useState(false);

  //For Show & Hide Password
  const [on_secure, setSecurity] = useState(true);

  //to hide & show error for input_text
  const [on_user_error, setFocus_user_error] = useState({
    mobile_no: false,
  });

  //to check and set focus error field or username
  const here_user = (value) => {
    set_user({ ...user, ['mobile_no']: value })
    if (value == '') {
      setFocus_user_error({ ...on_user_error, ['mobile_no']: true })

    } else {
      setFocus_user_error({ ...on_user_error, ['mobile_no']: false })

    }

  };

  //to check and set focus error field or password  
  const here_select_code = (value) => {
    set_user({ ...user, ['code']: value })
  };

  //to check and set focus error field or password  
  const onBack_click = () => {
    props.onBack_click();
  };


  const sports = [

    {
      label: '+86',
      value: '+86',
    },
    {
      label: '+25',
      value: '+25',
    },
  ];

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
      <StatusBar translucent backgroundColor={colors.main_dark_color} />
      <View style={{
        height: 100,
        width: width,
      }}>
        <View style={styles.back_image} />

        <Image source={Images.circle_login} style={styles.logo_new} />

        <TouchableOpacity onPress={() => onBack_click()} style={{
          position: 'absolute',
          marginTop: 50,
          marginLeft: 15,
        }}>
          <Image source={Images.back} style={styles.logo_back} />
        </TouchableOpacity>
      </View>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

        <View safe style={styles.container}>

          <StatusBar barStyle="light-content" translucent={true} />

          <Text style={styles.logintitle}>Forgot Password</Text>

          <Text style={styles.logintext}>Please enter your mobile number</Text>

          <View style={styles.sub_container_field}>

            <View style={styles.sub_container}>

              <RNPickerSelect
                placeholder={{ label: '+91', value: '+91', color: colors.dark_gray }}
                onValueChange={(value) => here_select_code(value)}
                items={sports}
                style={pickerSelectStyles}

              />

              <Image source={Images.dropdown_arrow} style={styles.drop_down} />
              <View style={{ width: 1, height: 20, marginLeft: 78, position: 'absolute', alignSelf:'center', marginTop: 10, backgroundColor: colors.hint_color }} />

              <View style={{ width: 61, height: 1.2, marginLeft: 20, position: 'absolute', bottom:0, backgroundColor: colors.black }} />
              <TextInput
                placeholder={
                  !on_user ? user.mobile_no == '' ? "Mobile No" : ''
                    : ''
                }
                onChangeText={(value) => here_user(value)}
                onFocus={() => setFocus_user(true)}
                value={user.mobile_no}
                onBlur={() => setFocus_user(false)}
                placeholderTextColor={colors.gray}
                keyboardType={'numeric'}
                autoCorrect={false}
                style={styles.inputText} />


            </View>
            {on_user_error.mobile_no ?
              <Text style={styles.errorText}>{'Please Enter Valid Mobile No'}</Text>
              : null
            }

            <TouchableOpacity onPress={() => sendOtp()} style={styles.login}>
              <Text style={styles.logintext_button}>SUBMIT</Text>
            </TouchableOpacity>

          </View>

        </View>


      </TouchableWithoutFeedback>


      <Spinner
        visible={props.isLoading}
        textContent={'Loading...'}
        color={'white'}
        overlayColor={'rgba(0, 0, 0, 0.45)'}
        textStyle={styles.spinnerTextStyle}
      />

    </ScrollView>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'white',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

export default ForgetPasswordComponent;
