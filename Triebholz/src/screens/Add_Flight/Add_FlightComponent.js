import React, { memo, useState, useRef } from 'react';
import { View, Fragment, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import { colors, fonts, sizes } from '../../utils/theme';
import * as geolib from 'geolib';
import languages from '../../globals/languages';
import SearchableDropdown from 'react-native-searchable-dropdown';
import DatePicker from 'react-native-datepicker'
import Moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const Add_FlightComponent = memo((props) => {

  const [isVisible, setVisiable_Pop] = useState(false);
  // Storing Name & Pass
  const [airline, set_airline] = useState({
    name: '',
    flight_no: ''
  });
  var cureentDate = Moment(new Date())

  const [selected_item, setSelected_item] = useState([]);
  const [keyboardStatus, setKeyboardStatus] = useState(false);
  const [isDatevisible, setDateVisible] = useState(false);
  const [from_date, setFromDate] = useState('');
  const [date, setDate] = useState(new Date())

  var items = [
    {
      id: 1,
      name: 'AI671',
    },
    {
      id: 2,
      name: 'AI672',
    },
    {
      id: 3,
      name: 'AI672',
    },
    {
      id: 4,
      name: 'AI673',
    },
    {
      id: 5,
      name: 'AI674',
    },
  ];
  const onCancel_press = () => {
    setVisiable_Pop(false)
  }
  const add_flight_select = () => {
    props.add_flight_select()
  }

  const onBack = () => {
    props.onBack();
  }

  const onData_select = () => {
    if (selected_item.length == 0) {
      alert('Please select airline')
    } else if (from_date == '') {
      alert('please select date')
    } else if (airline.flight_no == '') {
      alert('please enter flight no')
    } else {
      const New_date = Moment(from_date).format('YYYY-MM-DD')

      var request = {
        carrierCode: selected_item[0].name,
        departure_date: New_date,
        flight_no: airline.flight_no,
      }
      props.onData_select(request);
    }
  }

  const ScanScreen = () => {
    props.ScanScreen();
  }

  const manage_link_ac = () => {
    props.manage_link_ac();
  }

  const ImportProgram = () => {
    setVisiable_Pop(true)
  }

  const center = geolib.getCenter([
    { latitude: 28.2014833, longitude: -177.3813083 },
    { latitude: 39.849312, longitude: -104.673828 },
  ]);

  return (
    <TouchableOpacity onPress={() => Keyboard.dismiss()} activeOpacity={1} style={styles.container1}>

      <View style={styles.logo_map} >

        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 50, }}>

          <Text style={styles.logintitle}>{Language.lang.add_flight}</Text>
          <TouchableOpacity onPress={() => onBack()} style={{
            position: 'absolute',
            right: 0,
            marginTop: 10,
            marginRight: 20
          }}>
            <Image source={Images.close} style={styles.logo_lock} />

          </TouchableOpacity>

        </View>
      </View>

      {/* <View style={styles.sub_container}> */}

      {/* </View> */}

      <ScrollView contentContainerStyle={styles.container}>


        {
          isVisible ? <View style={{ height: height, opacity: 0.8, width: width, backgroundColor: colors.background_opacity, position: 'absolute' }} />
            : null
        }
        {
          !isVisible ?
            !isDatevisible ?
              <View style={styles.view_flight_card}>

                <TouchableOpacity onPress={() => ScanScreen()} style={{ paddingLeft: 30 }}>
                  <View style={{ flexDirection: 'row', flex: 1, marginTop: 20 }}>
                    <Image source={Images.scan} style={styles.logo_icon_account} />
                    <Text style={styles.From_country_name}>{languages.lang.scan_pass}</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => manage_link_ac()} style={{ paddingLeft: 30, marginTop: 20 }}>
                  <View style={{ flexDirection: 'row', flex: 1, marginTop: 20 }}>
                    <Image source={Images.link_acc} style={styles.logo_icon_account} />
                    <Text style={styles.From_country_name}>{languages.lang.manage_account}</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => ImportProgram()} style={{ paddingLeft: 30, marginTop: 20 }}>
                  <View style={{ flexDirection: 'row', flex: 1, marginTop: 20 }}>
                    <Image source={Images.loyalty_prog} style={styles.logo_icon_country} />
                    <Text style={styles.From_import_name}>{languages.lang.import_loyalty}</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity style={{ paddingLeft: 30, marginTop: 20 }}>
                  <View style={{ flexDirection: 'row', flex: 1, marginTop: 20 }}>
                    <Image source={Images.forword} style={styles.logo_icon_share} />
                    <Text style={styles.From_country_name}>{languages.lang.forword_email}</Text>
                  </View>
                </TouchableOpacity>

              </View>
              :
              <View style={styles.view_flight_card_date}>

                <Text style={{
                  color: colors.black,
                  fontSize: 18,
                  fontFamily: fonts.SemiBold,
                }}>Enter Flight No</Text>

                <TextInput
                  placeholder={languages.lang.enter_flight}
                  onChangeText={(value) => set_airline({ ...airline, ['flight_no']: value })}
                  value={airline.flight_no}
                  placeholderTextColor={colors.gray}
                  autoCorrect={false}
                  style={styles.inputText_border} />

                <View style={{ height: 1, backgroundColor: colors.view_color, width: width, marginTop: 30 }} />

                <Text style={{
                  color: colors.black,
                  fontSize: 18,
                  fontFamily: fonts.SemiBold,
                  marginTop:50
                }}>Select Date</Text>

                <View style={styles.searchSection_date1}>
                  <DatePicker
                    style={styles.dobText}
                    date={from_date}
                    mode="date"
                    // androidMode={'spinner'}
                    placeholder={"From date"}
                    format="YYYY-MM-DD"
                    // maxDate={cureentDate}
                    minDate={cureentDate}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={true}
                    customStyles={{
                      placeholderText: {
                        ...styles.placeholderText
                      },
                      dateText: {
                        ...styles.dateText
                      },
                      dateInput: { ...styles.dateInput },
                      dateIcon: { ...styles.logo_date }

                    }}
                    onDateChange={(date) => {
                      setFromDate(date)
                    }}
                  />
                </View>



                <TouchableOpacity onPress={() => onData_select()} style={{ marginTop: 20, position: 'absolute', bottom: 0, marginBottom: 20, width: 200, padding: 10, borderRadius: 10, backgroundColor: colors.main_color_dark }}>
                  <Text style={styles.From_country_name1}>{languages.lang.submit}</Text>
                </TouchableOpacity>
              </View>

            :

            <View style={styles.view_import_program}>

              <TouchableOpacity style={{ margin: 20 }}>
                <View style={{ flexDirection: 'row', flex: 1, marginTop: 10 }}>
                  <Image source={Images.loyalty_prog} style={styles.logo_icon_country} />
                  <Text style={styles.add_program}>{languages.lang.add_program}</Text>
                </View>
              </TouchableOpacity>

              <TextInput
                placeholder={languages.lang.name_of_program}
                onChangeText={(value) => set_airline({ ...airline, ['name']: value })}
                value={airline.name}
                placeholderTextColor={colors.gray}
                autoCorrect={false}
                style={styles.inputText} />

              <Text style={styles.inputTextGray}>{languages.lang.nothing_found}</Text>

              <View style={{ flexDirection: 'row', flex: 1 }}>
                <TouchableOpacity onPress={() => onCancel_press()} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={styles.card_add_new}>{Language.lang.cancel}</Text>

                </TouchableOpacity>
                <TouchableOpacity onPress={() => add_flight_select()} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={styles.View_add_pop}>{Language.lang.select}</Text>

                </TouchableOpacity>
              </View>

            </View>

        }
      </ScrollView>

      <View style={{ flex: 1, width: width, padding: 20, marginTop: 90, position: 'absolute', backgroundColor: 'white', marginRight: 5 }}>
        <SearchableDropdown
          onTextChange={(text) => console.log(text)}
          // onFocus={() => setKeyboardStatus(false)}
          showSoftInputOnFocus
          autoFocus={true}
          onBlur={() => setKeyboardStatus(false)}
          blurOnSubmit={false}
          selectedItems={selected_item}
          onItemSelect={(item) => {
            var items = selected_item;
            if (selected_item.length == 0) {

              items.push(item)
              setSelected_item(items);
              setDateVisible(true)
              console.log(JSON.stringify(items))
            } else {
              alert('You can select only one airline')
            }

          }}
          containerStyle={{ padding: 5, flex: 1 }}
          onRemoveItem={(item, index) => {
            var items = selected_item.filter((sitem) => sitem.id !== item.id);
            setSelected_item(items);
            setDateVisible(false)

          }}
          itemStyle={{
            padding: 10,
            marginTop: 2,
            backgroundColor: '#ededed',
            borderColor: '#ededed',
            borderWidth: 1,
            borderRadius: 5,
            fontSize: 14,
          }}
          itemTextStyle={{ color: '#222', fontSize: 14, }}
          itemsContainerStyle={{ maxHeight: 140 }}
          items={props.airline_data}
          chip={true}
          resetValue={false}
          textInputProps={
            {
              placeholder: "Airline",
              underlineColorAndroid: "transparent",
              fontSize: 14,
              style: {
                padding: 12,
                borderWidth: 1,
                borderColor: '#ccc',
                borderRadius: 5,
                fontSize: 14,
                height: 50
              },
            }
          }
          listProps={
            {
              nestedScrollEnabled: true,
            }
          }
          multi={true}
        />
      </View>


      <Spinner
        visible={props.isLoading}
        textContent={'Loading...'}
        color={'white'}
        overlayColor={'rgba(0, 0, 0, 0.45)'}
        textStyle={styles.spinnerTextStyle}
      />

    </TouchableOpacity>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default Add_FlightComponent;
