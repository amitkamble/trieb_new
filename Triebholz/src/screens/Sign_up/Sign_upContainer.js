import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Sign_upComponent from './Sign_upComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Alert, BackHandler, NativeModules, } from 'react-native';

import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { LoginManager, GraphRequest, GraphRequestManager } from "react-native-fbsdk";

const iosClientId = '29730712707-9ogt435pljbjisd919la6jhtte4mdaq2.apps.googleusercontent.com';
const webClientId = '29730712707-9ogt435pljbjisd919la6jhtte4mdaq2.apps.googleusercontent.com';


const Sign_upContainer = (props) => {

  const { navigation,  clearUserObj, route,userrole,userSignup,socialLogin} = props;
  const { isComeFrom } = route;
  const [isLoading, setLoading] = React.useState(false);
  const [user_role, setUserRole] = useState([]);

  if (isComeFrom === 'logout') {
    clearUserObj();
  }

  useEffect(() => {

    setGoogleClinet();
    setLoading(true);

    getUser_role();

    const backAction = () => {
      navigation.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

  }, []);

  const setGoogleClinet = () => {
    GoogleSignin.configure({
      iosClientId: iosClientId,
      webClientId: webClientId,
      offlineAccess: false
    });
  }


  const login_button_pressed = (user) => {
    setLoading(true);

    let data = new FormData();
    data.append("fullname", user.username);
    data.append("email", user.email);
    data.append("user_role", user.role);
    data.append("password", user.pass);

    console.log('here', JSON.stringify(data))

    Register_now(data)
  };

  const onSignIn = () => {
    navigation.goBack();
  };

  const onFacebookLogin = async () => {
    console.log('onFacebookLogin');

    var result = await LoginManager.logInWithPermissions(["public_profile", "email"]);
    if (result.isCancelled) {
      console.log("Login cancelled");
    } else {
      // console.log("permissions granted : " + result.grantedPermissions.toString());
      this.graphRequest();
    }

  }

  const onGmailLogin = async () => {
    console.log('onGmailLogin');

    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(JSON.stringify(userInfo));

      callSocialLogin(userInfo.email, name, "gmail");


      // this.setState({ userInfo });
    } catch (error) {

      console.log('onGmailLogin error', JSON.stringify(error));

      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }

  }

  const onOutlookLogin = () => {
    console.log('onOutlookLogin');
  }

  // const _responseInfoCallback = (error: ?Object, result: ?Object) => {
  //   if (error) {
  //     console.log('Error fetching data: ' + error.toString());
  //   } else {
  //     console.log(JSON.stringify(result));
  //     callSocialLogin(result.email, name, "facebook");
  //   }
  // }

  // const graphRequest = async () => {
  //   // Create a graph request asking for user information with a callback to handle the response.
  //   const infoRequest = new GraphRequest(
  //     '/me?fields=name,picture,email',
  //     null,
  //     (err, res) => { this._responseInfoCallback(err, res); }
  //   );
  //   // Start the graph request
  //   const response = await new GraphRequestManager().addRequest(infoRequest).start();
  // }

  const callSocialLogin = async(email, name, type) => {

    let fData = new FormData();
    fData.append("email", email);
    fData.append("fullname", name);
    fData.append("social_type", type);

    await socialLogin(fData)
      .then(response => {
        setLoading(false);
        var data_response = response.payload.data;
        console.log("api_login_Response" + JSON.stringify(data_response.isProgress));

        if (data_response.status == 200) {
          // navigation.navigate('LoginContainer');
          alert("Social Successfully.")
        } else {
          alert(data_response.message)
        }
      })
      .catch(error => {
        alert(JSON.stringify(error.error.message))
        setLoading(false);

      })

  }


  const Register_now = async () => {
    await userSignup()
      .then(response => {
        setLoading(false);
        var data_response = response.payload.data;
        console.log("api_login_Response" + JSON.stringify(data_response.isProgress));

        if (data_response.status == 200) {
          navigation.navigate('LoginContainer')
          alert("Register Successfully , Try login now.")

        } else {
          alert(data_response.message)
        }
      })
      .catch(error => {
        alert(JSON.stringify(error.error.message))  
        setLoading(false);
      })
  }

  const getUser_role = async () => {

    await userrole()
      .then(response => {
        var data_response = response.payload.data;
        var temp = [];
        setLoading(false);
        response.payload.data.data.map(item => {
          temp.push({ label: item.role_name, value: item.role_id })
        });
        setUserRole(temp)
        console.log("api_login_Response" + JSON.stringify(temp));

      })
      .catch(error => {
        console.log('error' + JSON.stringify(error));
        // Alert.alert(error)
        setLoading(false);
      })
  };

  // Setting 
  return (
    <Sign_upComponent props={props} login_button_pressed={login_button_pressed} isLoading={isLoading}
      onSignIn={onSignIn} user_role={user_role}
      onFacebookLogin={onFacebookLogin}
      onGmailLogin={onGmailLogin}
      onOutlookLogin={onOutlookLogin} />
  );
};

Sign_upContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
  userrole: () => dispatch(Actions.userrole()),
  socialLogin: (params) => dispatch(Actions.socialLogin(params)),
  userSignup: (params) => dispatch(Actions.userSignup(params)),
});


export default connect(mapStateToProps, mapDispatchToProps)(Sign_upContainer);
