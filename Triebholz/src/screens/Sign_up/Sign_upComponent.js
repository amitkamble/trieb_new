import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, Modal, StyleSheet, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView } from 'react-native';
import styles from './styles';
import { Images } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, Language } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import { Picker } from '@react-native-picker/picker';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const Sign_upComponent = memo((props) => {
  const [selectedLanguage, setSelectedLanguage] = useState();


  // Storing Name & Pass
  const [user, set_user] = useState({
    username: '',
    email: '',
    pass: '',
    confirm_pass: '',
    role: '',
  });

  // Storing Name & Pass
  const user_arr = [
    { name: 'username', value: user.username },
    { name: 'email', value: user.email },
    { name: 'pass', value: user.pass },
    { name: 'confirm_pass', value: user.confirm_pass },
    { name: 'role', value: user.role },
  ]

  // Sign IN Button Pressed
  const onSignInclick = () => {
    props.onSignIn();

  }

  const onFacebookLogin =()=>{
    props.onFacebookLogin();
  }

  const onGmailLogin =()=>{
    props.onGmailLogin();
  }

  const onOutlookLogin =()=>{
    props.onOutlookLogin();
  }

  // Sign IN Button Pressed
  const signup_button_pressed = () => {
    if (user.username.trim().length && user.pass.trim().length && user.email.trim().length && user.role != '' && user.confirm_pass.trim().length) {
      if (user.pass == user.confirm_pass) {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (reg.test(user.email) === false) {
          alert("Please enter valid email");
        }
        else {
          setFocus_user_error({ ...on_user_error, ['username']: false, ['pass']: false, ['confirm_pass']: false, ['email']: false, })
          props.login_button_pressed(user);
        }

      } else {
        alert('Password mismatch !')
      }

    }

    for (var i = 0; i < 5; i++) {
      var name = user_arr[i].name
      if (user_arr[i].value == '' || !user_arr[i].value.trim().length) {
        if (name == 'username') {
          console.log('user1')
          setFocus_user_error(true);
        } else if (name == 'pass') {
          console.log('user2')
          setFocus_pass_error(true);
        } else if (name == 'email') {
          console.log('user3')
          setFocus_email_error(true);
        } else if (name == 'role') {
          console.log('use4')
          setFocus_role_error(true);
        } else if (name == 'confirm_pass') {
          console.log('user5')
          setFocus_confirm_pass_error(true);
        }

      } else {
        if (name == 'username') {
          console.log('user1')
          setFocus_user_error(false);
        } else if (name == 'pass') {
          console.log('user2')
          setFocus_pass_error(false);
        } else if (name == 'email') {
          console.log('user3')
          setFocus_email_error(false);
        } else if (name == 'role') {
          console.log('use4')
          setFocus_role_error(false);
        } else if (name == 'confirm_pass') {
          console.log('user5')
          setFocus_confirm_pass_error(false);
        }
      }
    }

  };

  //to set Focus for Username Field
  const [on_user, setFocus_user] = useState(false);

  //to set Focus for Password Field
  const [on_pass, setFocus_pass] = useState(false);

  const [on_confirm_pass, setFocus_confirm_pass] = useState(false);

  //to set Focus for Password Field
  const [on_email, setFocus_email] = useState(false);

  //For Show & Hide Password
  const [on_secure, setSecurity] = useState(true);
  const [on_secure_confirm, setSecurity_confirm] = useState(true);

  //to hide & show error for input_text
  const [on_user_error, setFocus_user_error] = useState(false);

  //to hide & show error for input_text
  const [on_email_error, setFocus_email_error] = useState(false);
  const [on_role_error, setFocus_role_error] = useState(false);
  const [on_pass_error, setFocus_pass_error] = useState(false);
  const [on_confirm_pass_error, setFocus_confirm_pass_error] = useState(false);

  //to check and set focus error field or username
  const here_user = (value) => {
    set_user({ ...user, ['username']: value })
    if (value == '') {
      setFocus_user_error(true);

    } else {
      setFocus_user_error(false);

    }

  };
  const sports = [
    {
      label: 'Airline',
      value: 'Alirline',
    },
    {
      label: 'Shop Owner',
      value: 'Shop Owner',
    },
    {
      label: 'Crew Member',
      value: 'Crew Member',
    },
  ];
  //to check and set focus error field or password  
  const here_pass = (value) => {
    set_user({ ...user, ['pass']: value })
    if (value == '') {
      setFocus_pass_error(true)

    } else {
      setFocus_pass_error(false)

    }
  };
  //to check and set focus error field or password  
  const here__confirm_pass = (value) => {
    set_user({ ...user, ['confirm_pass']: value })
    if (value == '') {
      setFocus_confirm_pass_error(true)

    } else {
      setFocus_confirm_pass_error(false)

    }

  };

  //to check and set focus error field or password  
  const here_email = (value) => {
    set_user({ ...user, ['email']: value })
    if (value == '') {
      setFocus_email_error(true)

    } else {
      setFocus_email_error(false)

    }

  };
  //to check and set focus error field or password  
  const here_select_role = (value) => {
    set_user({ ...user, ['role']: value })
    if (value == '') {
      setFocus_role_error(true)

    } else {
      setFocus_role_error(false)

    }

  };

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor={colors.main_dark_color} />
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

        <ScrollView safe contentContainerStyle={styles.container}>

          <StatusBar barStyle="light-content" translucent={true} />

          <View style={{
            height: 210,
            width: width,
          }}>
            <View style={styles.back_image} />

            <Image source={Images.circle_login} style={styles.logo_new} />

            <Image source={Images.logo_app} style={styles.logo_trieb} />


          </View>

          <View style={styles.sub_container_field}>
            <Text style={styles.logintitle}>Welcome</Text>

            <Text style={styles.logintext}>Please register your account.</Text>

            {
              <Animated.Text style={styles.HedingText}>{!on_user ? user.username != '' ? "Username" : ''
                : 'Username'}</Animated.Text>

            }

            <View style={styles.sub_container}>

              <TextInput
                placeholder={
                  !on_user ? user.username == '' ? "Username" : ''
                    : ''
                }
                onChangeText={(value) => here_user(value)}
                onFocus={() => setFocus_user(true)}
                value={user.username}
                onBlur={() => setFocus_user(false)}
                placeholderTextColor={colors.gray}
                autoCorrect={false}
                style={styles.inputText} />

              <Image source={Images.user} style={styles.logo_user} />

            </View>
            {on_user_error ?
              <Text style={styles.errorText}>{'Please Enter Username'}</Text>
              : null

            }
            <Text style={styles.HedingText_pass}>{!on_email ? user.email != '' ? "Email" : ''
              : 'Email'}</Text>

            <View style={styles.sub_container}>

              <TextInput
                placeholder={
                  !on_email ? user.email == '' ? "Email" : ''
                    : ''
                }
                onFocus={() => setFocus_email(true)}
                onBlur={() => setFocus_email(false)}
                onChangeText={(value) => here_email(value)}
                value={user.email}
                placeholderTextColor={colors.gray}
                style={styles.inputText} />

              <Image source={Images.mail} style={styles.logo} />

            </View>

            {on_email_error ?
              <Text style={styles.errorText}>{'Please Enter Valid Email'}</Text>
              : null

            }


            <View style={styles.container_viewtop}>

              <RNPickerSelect
                placeholder={{ label: 'Select User Role', value: '', color: colors.dark_gray }}
                onValueChange={(value) => here_select_role(value)}
                items={props.user_role}
                style={pickerSelectStyles}
                Icon={() => {
                  return <Image source={Images.dropdown_arrow} style={styles.drop_down} />
                }}
              />
              <View style={{ width: width - 125, marginLeft: 15, height: 1, backgroundColor: 'gray' }} />

            </View>

            {on_role_error ?
              <Text style={styles.errorText}>{'Please Select User Role'}</Text>
              : null

            }

            <Text style={styles.HedingText_pass}>{!on_pass ? user.pass != '' ? "Password" : ''
              : 'Password'}</Text>

            <View style={styles.sub_container}>

              <TextInput
                placeholder={
                  !on_pass ? user.pass == '' ? "Password" : ''
                    : ''
                }
                onFocus={() => setFocus_pass(true)}
                onBlur={() => setFocus_pass(false)}
                onChangeText={(value) => here_pass(value)}
                value={user.pass}
                placeholderTextColor={colors.gray}
                secureTextEntry={on_secure}
                autoCorrect={false}
                style={styles.inputText} />

              <TouchableOpacity style={{
                right: 0,
                height: 20,
                width: 20,
                position: 'absolute',
                marginRight: 23,
                alignSelf: 'center'
              }} onPress={() => setSecurity(on_secure ? false : true)} >

                <Image source={user.pass != '' ? on_secure ? Images.pass_not_visible : Images.pass_visible : Images.lock} style={user.pass != '' ? styles.logo_pass : styles.logo_lock} />
              </TouchableOpacity>
            </View>
            {
              on_pass_error ?
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.errorText_pass}>{'Please Enter Password'}</Text>
                </View>
                : null
            }


            <Text style={styles.HedingText_pass}>{!on_confirm_pass ? user.confirm_pass != '' ? "Confirm Password" : ''
              : 'Confirm Password'}</Text>

            <View style={styles.sub_container}>

              <TextInput
                placeholder={
                  !on_confirm_pass ? user.confirm_pass == '' ? "Confirm Password" : ''
                    : ''
                }
                onFocus={() => setFocus_confirm_pass(true)}
                onBlur={() => setFocus_confirm_pass(false)}
                onChangeText={(value) => here__confirm_pass(value)}
                value={user.confirm_pass}
                placeholderTextColor={colors.gray}
                secureTextEntry={on_secure_confirm}
                autoCorrect={false}
                style={styles.inputText} />

              <TouchableOpacity style={{
                right: 0,
                height: 20,
                width: 20,
                position: 'absolute',
                marginRight: 23,
                alignSelf: 'center'
              }} onPress={() => setSecurity_confirm(on_secure_confirm ? false : true)} >

                <Image source={user.confirm_pass != '' ? on_secure_confirm ? Images.pass_not_visible : Images.pass_visible : Images.lock} style={user.confirm_pass != '' ? styles.logo_pass : styles.logo_lock} />
              </TouchableOpacity>
            </View>
            {
              on_confirm_pass_error ?
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.errorText_pass}>{'Please Enter Password'}</Text>
                </View>
                : null
            }


            <TouchableOpacity onPress={() => signup_button_pressed()} style={styles.login}>
              <Text style={styles.logintext_button}>REGISTER</Text>
            </TouchableOpacity>

            <Text style={styles.logintext_SOCIAL}>Or Register using social media</Text>
            <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 5, marginBottom: 20 }}>
              <TouchableOpacity onPress={() => onFacebookLogin()} style={styles.social_media}>

                <Image source={Images.facebook} style={styles.social_media} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => onOutlookLogin()} style={styles.social_media}>

                <Image source={Images.twitter_login} style={styles.social_media} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => onGmailLogin()} style={styles.social_media}>

                <Image source={Images.gmail} style={styles.social_media} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 10 }}>
            <Text style={styles.signUp_text_first}>{'Already have an account? '}</Text>
            <TouchableOpacity onPress={() => onSignInclick()}>
              <Text style={styles.signUp_text_click}>{'Login now'}</Text>
            </TouchableOpacity>
          </View>
          <Image source={Images.login_bottom_image} style={styles.bottom_image} />

        </ScrollView>


      </TouchableWithoutFeedback>



      <Spinner
        visible={props.isLoading}
        textContent={'Loading...'}
        color={'white'}
        overlayColor={'rgba(0, 0, 0, 0.45)'}
        textStyle={styles.spinnerTextStyle}
      />

    </View>

  );
});


const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'white',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    padding: 20,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.text_color_semi_black,
    borderColor: 'white',
    flex: 1,
    backgroundColor: '#ffffff'
  },
});



export default Sign_upComponent;
