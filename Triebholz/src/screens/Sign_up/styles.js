import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors, fonts, sizes } from '../../utils/theme';

import Icon from 'react-native-vector-icons/Ionicons';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default StyleSheet.create({
  container: {
    alignItems: 'stretch',
  },

  sub_container: {
    flexDirection: 'row',
    width:width-70,
  },

  sub_container_field: {
    backgroundColor:colors.white,
    borderRadius:10,
    marginLeft:30,
    marginRight:30,
    marginTop:30,
    shadowColor: colors.gray,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 8
  },

  back_image: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    height: 400,
    width: width,
    marginBottom: 40,
    backgroundColor:colors.main_dark_color
  },

  logo: {
    height: 11.5,
    width: 15,
    position:'absolute',
    right :0 ,
    marginRight:30,
    alignSelf:'center'
  },

  social_media: {
    height: 35,
    width: 35,
    margin:10
  },

  logo_user: {
    height: 15,
    width: 15,
    position:'absolute',
    right :0 ,
    marginRight:30,
    alignSelf:'center'
  },

  HedingText: {
    marginTop: 25,
    fontFamily: fonts.SemiBold,
    fontSize: sizes.regularLarge,
    color: colors.hint_color,
    marginLeft: 30
  },
 
  HedingText_pass: {
    marginTop: 10,
    fontFamily: fonts.SemiBold,
    fontSize: sizes.regularLarge,
    color: colors.hint_color,
    marginLeft: 30
  },
 
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  errorText: {
    marginTop: 5,
    fontFamily: fonts.SemiBold,
    fontSize: sizes.small,
    color: colors.red_failure,
    marginLeft: 30,
  },
  errorText_pass: {
    marginTop: 5,
    fontFamily: fonts.SemiBold,
    fontSize: sizes.small,
    color: colors.red_failure,
    marginLeft: 30,
    flex:1
  },

  signUp_text_first: {
    marginTop: 15,
    fontFamily: fonts.Regular,
    fontSize: sizes.small,
    color: colors.hint_color,
  },
  signUp_text_click: {
    marginTop: 15,
    fontFamily: fonts.Regular,
    fontSize: sizes.small,
    color: colors.main_dark_color,
  },

  forget_pass_Text: {
    marginTop: 15,
    fontFamily: fonts.Regular,
    fontSize: sizes.small,
    color: colors.black,
    marginLeft: 25,
    flex:0.9
  },

  inputText: {
    fontSize: sizes.regularLarge,
    height: Platform.OS == 'ios' ? 50 : 40,
    fontFamily: fonts.Regular,
    marginLeft:30,
    marginRight:30,
    borderBottomColor: colors.main_color_dull,
    color: colors.black,
    paddingRight:30,
    borderBottomWidth: 1,
    width:width -125
  },

  title_text: {
    color: colors.white,
    fontSize: sizes.medium,
    fontFamily: fonts.Regular,
    position: 'absolute',
    marginTop:200,
    alignSelf:'center'
  },

  logintitle: {
    color: colors.black,
    fontSize: 25,
    fontFamily: fonts.Bold,
    alignSelf:'center',
    marginTop:30
  },

  logintext: {
    color: colors.hint_color,
    fontSize: sizes.regular,
    fontFamily: fonts.Regular,
    alignSelf:'center',
    marginTop:5
  },

  logintext_SOCIAL: {
    color: colors.text_color_semi_black,
    fontSize: sizes.regular,
    fontFamily: fonts.Regular,
    alignSelf:'center',
    marginTop:10
  },

  logintext_button: {
    color: colors.white,
    fontSize: sizes.regular,
    fontFamily: fonts.Bold,
    alignSelf:'center',
  },

  login: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 50,
    marginBottom: 10,
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 60,
    marginRight: 60,
    marginTop: 35,
    backgroundColor: colors.main_dark_color,
  },

  spinnerTextStyle: {
    color: '#fff'
  },
  text_regular_14: {
    color: colors.white,
    marginTop: 20,
    fontSize: sizes.regularLarge,
    fontFamily: fonts.Regular
    // fontFamily:fonts.Bold
  },

  logo_new: {
    height: 95,
    width: 93,
    position:'absolute',
    marginLeft:-20,
    marginTop:-10,
  },

  bottom_image: {
    height: 105,
    width: width+30,
    marginLeft:-10,
    marginRight:-10,
    marginBottom:-10
  },

  logo_trieb: {
    height: 123,
    width: 124,
    marginTop:80,
    position:'absolute',
    alignSelf:'center'
  },

  logo_pass: {
    height: 15,
    width: 18,
  },

  logo_lock: {
    height: 15,
    width: 13,
  },

  drop_down: {
    height: 6,
    width: 10,
    marginTop:20,
  },

  container_viewtop: {
    alignItems: 'center',
    justifyContent:'center',
    fontSize: sizes.regularLarge,
    height: 50,
    borderRadius: 10,
    marginLeft:25,
    marginRight:40,
    marginTop:20,
    width:width-130,
    backgroundColor:'white'
  },
  
});
