import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import PolylineDirection from '@react-native-maps/polyline-direction';
import MapView, { Polyline, PROVIDER_GOOGLE } from 'react-native-maps';
import * as geolib from 'geolib';
import { ProgressBar, Colors } from 'react-native-paper';
import MyTripsContainer from '../MyTrips/MyTripsContainer';
import languages from '../../globals/languages';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const InboxComponent = memo((props) => {

  const [isVisible, setVisiable_Pop] = useState(false);
  // Storing Name & Pass
  const [airline, set_airline] = useState({
    name: '',
  });

  const onCancel_press = () => {
    setVisiable_Pop(false)
  }
  const add_flight_select = () => {
    props.add_flight_select()
  }

  const onBack = () => {
    props.onBack();
  }

  const ScanScreen = () => {
    props.ScanScreen();
  }

  const ImportProgram = () => {
    setVisiable_Pop(true)
  }

  const center = geolib.getCenter([
    { latitude: 28.2014833, longitude: -177.3813083 },
    { latitude: 39.849312, longitude: -104.673828 },
  ]);

  return (
    <ScrollView contentContainerStyle={styles.container}>

      <View style={styles.logo_map} >

        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 50, }}>

          <Text style={styles.logintitle}>{Language.lang.inbox}</Text>
          <TouchableOpacity onPress={() => onBack()} style={{
            position: 'absolute',
            right: 0,
            marginTop: 10,
            marginRight: 20
          }}>
            <Image source={Images.close} style={styles.logo_lock} />

          </TouchableOpacity>

        </View>
      </View>

      <View style={{ flexDirection: 'row', width: width, marginTop: 30, marginBottom: 20 }}>
        <Text style={styles.add_program}>{languages.lang.flight_alerts}</Text>
        <TouchableOpacity onPress={() => console.log('here')} style={{ flexDirection: 'row', alignSelf: 'flex-end', right: 0 }}>
          <Text style={styles.View_add_pop}>{Language.lang.view_all}</Text>
        </TouchableOpacity>
      </View>
      <View>


        <View style={styles.sub_container_column}>
          <View style={styles.sub_container}>
            <Text style={styles.flight_name}>{'JKF'}</Text>
            <Image source={Images.plane_color} style={styles.logo} />
            <Text style={styles.flight_name}>{'LFK'}</Text>
          </View>
          <Image source={Images.airoplane} style={styles.logo_aroplane} />

          <View style={styles.sub_container_gray}>
            <Text style={styles.flight_name_Data}>{'ACB1'}</Text>
            <View style={{ height: '100%', width: 1, backgroundColor: colors.gray }} />
            <Text style={styles.flight_name_Data}>{'01 MAY'}</Text>
            <View style={{ height: '100%', width: 1, backgroundColor: colors.gray, }} />
            <Text style={styles.flight_name_Data}>{'21:00PM'}</Text>
          </View>
        </View>

        <View style={styles.sub_container_column}>
          <View style={styles.sub_container}>
            <Text style={styles.flight_name}>{'JKF'}</Text>
            <Image source={Images.plane_color} style={styles.logo} />
            <Text style={styles.flight_name}>{'LFK'}</Text>
          </View>
          <Image source={Images.airoplane} style={styles.logo_aroplane} />

          <View style={styles.sub_container_gray}>
            <Text style={styles.flight_name_Data}>{'AAss0'}</Text>
            <View style={{ height: '100%', width: 1, backgroundColor: colors.gray }} />
            <Text style={styles.flight_name_Data}>{'01 MAY'}</Text>
            <View style={{ height: '100%', width: 1, backgroundColor: colors.gray, }} />
            <Text style={styles.flight_name_Data}>{'21:00PM'}</Text>
          </View>
        </View>
      </View>

      <View>

        <View style={{ flexDirection: 'row', width: width, marginTop: 10, marginBottom: 20 }}>
          <Text style={styles.add_program}>{languages.lang.travel_safe}</Text>
        </View>

        <View style={styles.sub_container_travel}>

          <Image source={Images.intersection} style={styles.logo_tavel} />
          <Image source={Images.safe_hand} style={styles.safe_hand_logo} />

          <Text style={styles.flight_travel_Safe}>{'The united states has updated their travel policy.If yo are planning a trip there, please read all the guidelines carefully.'}</Text>

          <Text style={styles.flight_travel_Safe_time}>{'04 MAY, 02:38PM'}</Text>

        </View>
      </View>


      <View>

        <View style={{ flexDirection: 'row', width: width, marginTop: 10, marginBottom: 20 }}>
          <Text style={styles.add_program}>{languages.lang.product_tips}</Text>
        </View>

        <View style={styles.sub_container_tips}>
          <View>
            <Text style={styles.flight_product_tips}>{'The united states has updated their travel policy.If yo are planning a trip there, please read all the guidelines carefully.'}</Text>

            <Text style={styles.flight_product_tips_time}>{'04 MAY, 02:38PM'}</Text>

            <View style={{ flex: 1, backgroundColor: colors.view_color, height: 1 }} />

          </View>          
          <View>
            <Text style={styles.flight_product_tips}>{'The united states has updated their travel policy.If yo are planning a trip there, please read all the guidelines carefully.'}</Text>

            <Text style={styles.flight_product_tips_time}>{'04 MAY, 02:38PM'}</Text>

            <View style={{ flex: 1, backgroundColor: colors.view_color, height: 1 }} />

          </View>
        </View>
      </View>


    </ScrollView>

  );
});

export default InboxComponent;
