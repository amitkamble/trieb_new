import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import InboxComponent from './InboxComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const InboxContainer = (props) => {

  const { navigation,  clearUserObj, route } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };
  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }

  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
      navigation.navigate('LoginContainer')
  };

  const ScanScreen = () => {
      navigation.navigate('ScanScreen')
  };

  const add_flight_select = () => {
      navigation.navigate('Add_Flight_manually')
  };

  const onBack = () => {
      navigation.goBack();
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);



  return (
    <InboxComponent props={props} login_button_pressed={login_button_pressed} onBack={onBack} 
    ScanScreen={ScanScreen} add_flight_select={add_flight_select}/>

  );
};
InboxContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
});


export default connect(mapStateToProps, mapDispatchToProps)(InboxContainer);
