import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors, fonts, sizes } from '../../utils/theme';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default StyleSheet.create({
  container: {
    flexGrow: 1,
  },

  sub_container: {
    flexDirection: 'row',
    padding: 10
  },

  sub_container_column: {
    flexDirection: 'column',
    margin: 20,
    marginTop:10,
    backgroundColor: colors.white,
    padding: 10,
    justifyContent:'center'
  },

  sub_container_tips: {
    flexDirection: 'column',
    margin: 20,
    marginTop:10,
    backgroundColor: colors.white,
    justifyContent:'center'
  },

  sub_container_travel: {
    flexDirection: 'column',
    margin: 20,
    marginTop:10,
    backgroundColor: colors.main_dark_color,
    padding: 10, },
    
  sub_container_gray: {
    flexDirection: 'row',
    padding: 10,
    alignItems:'center',
  },
    
  View_add_pop: {
    color: colors.white,
    borderColor: colors.main_dark_color,
    backgroundColor: colors.main_dark_color,
    borderWidth: 1,
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    paddingLeft: 20,
    paddingRight: 20,
    marginLeft: 20,
    marginRight: 20,
    paddingTop: Platform.OS == 'ios' ? 8 : 0,
    textAlignVertical: 'center',
    textAlign:'center',
    height: 34,
  }, 
  
  View_add_pop_new: {
    color: colors.white,
    borderColor: colors.main_dark_color,
    backgroundColor: colors.main_dark_color,
    borderWidth: 1,
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: Platform.OS == 'ios' ? 8 : 0,
    textAlignVertical: 'center',
    textAlign:'center',
    height: 34,
  },
  add_program: {
    color: colors.main_color_dark,
    fontSize: 16,
    fontFamily: fonts.SemiBold,
    marginLeft: 25,
    alignSelf: 'center',
    flex:1,
  },

  logo: {
    height: 30,
    width: 30,
    marginLeft:10,
    marginRight:10,
    alignSelf: 'center'
  },
  logo_aroplane: {
    width: 105,
    height: 30,
    marginRight:10,
    position:'absolute',
    right:0,
    alignSelf:'center'
  },
  logo_tavel: {
    width : width-28,
    maxHeight: 90,
    position:'absolute',
    right:0,
    top:0,
    marginTop:0, 
    marginRight:-25
  },
  
  safe_hand_logo: {
    width : 42.98,
    maxHeight: 49.88,
    position:'absolute',
    right:0,
    bottom:0,
    marginRight:20
  },
  
  logo_map: {
    height: 90,
    width: width,
    backgroundColor: colors.main_dark_color,
  },

  logo_lock: {
    height: 20,
    width: 20,
  },

  logintitle: {
    color: colors.white,
    fontSize: 18,
    fontFamily: fonts.SemiBold,
    alignSelf: 'center',
    marginLeft: 20
  },

  flight_name: {
    color: colors.main_dark_color,
    fontSize: 18,
    fontFamily: fonts.Bold,
    alignSelf: 'center',
  },

  flight_name_Data: {
    color: colors.main_color_dark,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    backgroundColor: colors.view_color,
    padding:10,
  },


  flight_travel_Safe: {
    color: colors.white,
    fontSize: 14,
    fontFamily: fonts.Monsterat_Regular,
    padding:10,
  },

  flight_travel_Safe_time: {
    color: colors.white,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    padding:10,
  },

  flight_product_tips: {
    color: colors.black,
    fontSize: 14,
    fontFamily: fonts.Monsterat_Regular,
    padding:20,
  },

  flight_product_tips_time: {
    color: colors.gray,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft:20,
    marginBottom:10
  },


});
