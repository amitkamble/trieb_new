import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors, fonts, sizes } from '../../utils/theme';

import Icon from 'react-native-vector-icons/Ionicons';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default StyleSheet.create({
  container: {
     flex:1,
     justifyContent:'center'
  },

  subcontainer: {
     opacity:0.9,
     flex:1, 
     height:height,
     width:width,
     position:'absolute'
  },

  logo_new: {
    height: 95,
    width: 93,
    position:'absolute',
    marginTop:-10,
    marginRight:-30,
    right:0,
    top:0
  },
  linearGradient: {
    opacity:0.6,
    height:height,
    width:width,
    position:'absolute',
    backgroundColor:'black'
  },
  logo_map: {
    height: height,
    width: width,
    resizeMode:'cover',
    position:'absolute'
  },

  logo_lock: {
    height: 135,
    width: 141,
    alignSelf:'center'
  },
  back_image: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: width,
    height:height,
    flex:1,
    position:'absolute',
    backgroundColor:colors.main_dark_color,
  },

  logintitle: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.Regular,
    alignSelf:'center',
    marginTop:30
  },

  text_login: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    marginBottom: 50,
    alignSelf: 'stretch',
    borderRadius: 25,
  },

  google_login: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 50,
    marginBottom: 20,
    alignSelf: 'stretch',
    borderRadius: 25,
    marginLeft: 90,
    marginRight: 90,
    backgroundColor: colors.white,
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 8,
  },

  fb_login: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 50,
    marginBottom: 20,
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: colors.main_dark_color,
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 8,
  },

  login: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 50,
    marginBottom: 20,
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: colors.white,
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 8,
  },

  logintext_button: {
    color: colors.white,
    fontSize: sizes.regular,
    fontFamily: fonts.Monsterat_SemiBold,
    alignSelf:'center',
  },

  logintext_button2: {
    color: colors.main_color_dark,
    fontSize: sizes.regular,
    fontFamily: fonts.Monsterat_SemiBold,
    alignSelf:'center',
  },

  import_button: {
    color: colors.main_dark_color,
    fontSize: 24,
    fontFamily: fonts.SemiBold,
    alignSelf:'center',
    marginTop:20
  },

  google_icon: {
    width:18,
    height:19,
    position:'absolute', 
    left:0, 
    marginLeft:20
  },

  import_icon: {
    width:36,
    height:39,
  },

  close_icon: {
    width:20,
    height:20,
  },

  fb_icon: {
    width:30,
    height:30,
    position:'absolute', 
    left:0, 
    marginLeft:15
  },

  google_text: {
    width:86,
    height:48,
    alignSelf:'center',
  },


  close_pop_up: {
    position:'absolute',
    right:0,
    top:0,
    marginRight:20 ,
    marginTop:60
  },
  view_flight_card: {
    margin: 20,
    backgroundColor: 'white',
    shadowColor: colors.gray,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 8,
    paddingBottom:30,
    borderRadius: 5,
  },
  logo_icon_country: {
    width: 20,
    height: 22,
    marginTop:5
  },
  logo_iconno_wifi: {
    width: 20,
    height: 15.86,
    marginTop:5
  },
  From_country_name: {
    color: colors.main_dark_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    marginLeft: 20,
    marginTop:5,
  },

  From_country_name1: {
    color: colors.gray,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    marginLeft: 20,
    marginTop:5,
  },
  
  logo_trieb: {
    height: 123,
    width: 124,
    alignSelf:'center',
    marginBottom:20,
  },
  name_trieb: {
    height: 31.68,
    width: 188.77,
    alignSelf:'center',
    marginBottom:50,

  },

});
