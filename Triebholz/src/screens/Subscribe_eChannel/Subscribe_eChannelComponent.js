import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView } from 'react-native';
import styles from './styles';
import { Images } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import languages from '../../globals/languages';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const Subscribe_eChannelComponent = memo((props) => {


  // Storing Name & Pass
  const [user, set_user] = useState({
    pass: '',
    confirm_pass: '',
  });

  const login_button_pressed = () => {
    // props.login_button_pressed();
  }
  const on_back_press = () => {
    props.handleBackButtonClick();
  }

  return (
    <View style={styles.container}>
      {/* <StatusBar translucent backgroundColor={colors.main_dark_color} /> */}

      {/* <Image source={Images.circle_login} style={styles.logo_new} /> */}

      <Image source={Images.flight_add} style={styles.logo_map} />

      <View colors={['#000000']} style={styles.linearGradient}>
      </View>

      <Image source={Images.logo_app} style={styles.logo_trieb} />
      
      <Image source={Images.app_name_white} style={styles.name_trieb} />

      <View style={styles.view_flight_card}>

        <View style={{ flexDirection: 'row', marginTop: 25, marginLeft: 20, marginRight: 20 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
            <Image source={Images.bell} style={styles.logo_icon_country} />
            <View style={{ flexDirection: 'column', justifyContent: 'flex-end', flex: 1 }}>
              <Text style={{ color: colors.main_dark_color, fontFamily: fonts.SemiBold, marginLeft: 20 }}>{'Flight Alerts'}</Text>
              <Text style={styles.From_country_name1}>{'Real time flight status : time/gate terminal changes'}</Text>

            </View>
          </View>

        </View>

        <View style={{ flexDirection: 'row', marginTop: 25, marginLeft: 20, marginRight: 20 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
            <Image source={Images.no_wifi} style={styles.logo_iconno_wifi} />
            <View style={{ flexDirection: 'column', justifyContent: 'flex-end', flex: 1 }}>
              <Text style={{ color: colors.main_dark_color, fontFamily: fonts.SemiBold, marginLeft: 20 }}>{'Offline access'}</Text>
              <Text style={styles.From_country_name1}>{'Get update with text messages.'}</Text>

            </View>
          </View>

        </View>

      </View>

      <View >
        <TouchableOpacity onPress={() => login_button_pressed()} style={styles.fb_login}>

          <Text style={styles.logintext_button}>{'Lifetime Member Rs.4000/-'}</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => login_button_pressed()} style={styles.login}>

          <Text style={styles.logintext_button2}>{'12 Months Rs.2000/-'}</Text>
        </TouchableOpacity>
      </View>

      <TouchableOpacity onPress={() => on_back_press()} style={styles.close_pop_up}>
        <Image source={Images.close} style={styles.close_icon} />
      </TouchableOpacity>
    </View>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

export default Subscribe_eChannelComponent;
