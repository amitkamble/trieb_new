import React, { memo, useState, useRef } from 'react';
import { View, FlatList, Dimensions, TextInput, Image, StyleSheet, Linking, Alert, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import { colors, fonts, sizes } from '../../utils/theme';
import MapView, { Polyline, PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import languages from '../../globals/languages';
import Spinner from 'react-native-loading-spinner-overlay';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const MyProfileComponent = memo((props) => {

  const [coordinates] = useState([
    { latitude: 28.2014833, longitude: -177.3813083 },
    { latitude: 39.849312, longitude: -104.673828 },
  ]);


  const view_all = () => {
    // props.login_button_pressed();
  }

  const setting = () => {
    props.setting();
  }

  const go_LeaderBoard = () => {
    props.go_LeaderBoard();
  }

  const StatisticDataContainer = () => {
    props.StatisticDataContainer();
  }


  return (
    <View >

      <TouchableOpacity onPress={() => console.log('here')} style={{ width: width, height: height - 100, flexGrow: 1, position: 'absolute' }}>
        <MapView
          provider={PROVIDER_GOOGLE}
          followUserLocation={true}
          showsUserLocation={true}
          zoomEnabled={true}
          zoomControlEnabled={false}
          style={{ width: width, height: height - 200, }}
          initialRegion={{
            latitude: 39.937328674782975,
            longitude: -143.92914396511438,
            latitudeDelta: Platform.OS == 'ios' ? 60 : 100,
            longitudeDelta: Platform.OS == 'ios' ? 60 : 50
          }}
          showsMyLocationButton={true}
        // customMapStyle={mapDarkStyle}
        >
          <Marker
            coordinate={{ latitude: 28.2014833, longitude: -177.3813083 }}
          >
            {/* <View> */}
            <Image source={Images.plane_green} style={styles.logo_plane} />

            {/* </View> */}

          </Marker>
          <Marker
            coordinate={{ latitude: 39.849312, longitude: -104.673828 }}
          >
            <View style={{ backgroundColor: colors.green_text_coot, width: 5, height: 5, borderRadius: 2.5 }}>

            </View>

          </Marker>
          {
            props.trips.map((item,i) => {
              i ==0 ? <Polyline
                coordinates={[
                  { latitude: 28.2014833, longitude: -177.3813083 },
                  { latitude: 39.849312, longitude: -104.673828 },
                ]}
                strokeColor={colors.main_color_dark} // fallback for when `strokeColors` is not supported by the map-provider
                strokeWidth={2}
                lineCap={'round'}
                geodesic={true}
                lineJoin={'round'}
                lineDashPhase={2000}
              /> : null
            })
          }

        </MapView>
      </TouchableOpacity>

      <ScrollView contentContainerStyle={{ flexGrow: 1, borderRadius: 5, paddingTop: -50, paddingBottom: 500, marginTop: height - 350 }}>

        <View style={{
          height: 270,
          width: width,
          flexDirection: 'row',
          justifyContent: 'center',
          position: 'absolute',
          backgroundColor: colors.main_dark_color,
          marginBottom: 90, marginTop: 50
        }}>


          <View style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            marginTop: 60,

          }}>
            <Text style={styles.tab_bar_text}>{'Pushkraj Jadhav'}</Text>

            <TouchableOpacity onPress={() => console.log()} style={{
              marginLeft: 15,
              alignItems: 'center'
            }}>
              <Image source={Images.share} style={styles.logo_share} />
            </TouchableOpacity>
          </View>
          <View style={{ position: 'absolute' }}>

          </View>
          <View style={{ position: 'absolute', justifyContent: 'center', left: 0, top: 0, width: width, marginTop: -50 }}>
            <TouchableOpacity onPress={() => console.log('here')} style={{ position: 'absolute', left: 0, top: 0, marginLeft: 20, marginTop: 25 }}>
              <Image source={Images.open_ar_earth} style={styles.logo_widget} />
            </TouchableOpacity>
            <Image source={Images.profile_image} style={styles.logo_icon_profile} />

            <TouchableOpacity onPress={() => setting()} style={{ position: 'absolute', right: 0, top: 0, marginRight: 20, marginTop: 25 }}>
              <Image source={Images.open_setting} style={styles.logo_widget} />
            </TouchableOpacity>
          </View>

        </View>

        <TouchableOpacity onPress={() => StatisticDataContainer()} style={styles.view_flight_card}>

          <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20, marginRight: 20 }}>
            <View style={{ justifyContent: 'center', flex: 0.4 }}>
              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <Image source={Images.take_off} style={styles.logo_icon_from} />
                <Text style={styles.From_country_name_side}>{props.data.flights}</Text>
              </View>
              <Text style={styles.From_country_name}>{languages.lang.flights}</Text>

            </View>
            <View style={{ justifyContent: 'center', flex: 0.4 }}>

              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <Image source={Images.country} style={styles.logo_icon_country} />
                <Text style={styles.From_country_name_side}>{props.data.countries}</Text>
              </View>
              <Text style={styles.From_country_name}>{languages.lang.countries}</Text>

            </View>
            <View style={{ justifyContent: 'center', flex: 0.4 }}>

              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <Image source={Images.airports} style={styles.logo_icon_airports} />
                <Text style={styles.From_country_name_side}>{props.data.airports}</Text>
              </View>
              <Text style={styles.From_country_name}>{languages.lang.airport}</Text>

            </View>

          </View>

          <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 25, marginLeft: 20, marginRight: 20 }}>
            <View style={{ justifyContent: 'center', flex: 0.4 }}>
              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <Image source={Images.airlines} style={styles.logo_icon_airline} />
                <Text style={styles.From_country_name_side}>{props.data.airlines}</Text>

              </View>
              <Text style={styles.From_country_name}>{languages.lang.airlines}</Text>

            </View>
            <View style={{ justifyContent: 'center', flex: 0.4, marginTop: 5 }}>

              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <Image source={Images.aircraft} style={styles.logo_icon_aircraft} />
                <Text style={styles.From_country_name_side}>{props.data.aircrafts}</Text>

              </View>
              <Text style={styles.From_country_name}>{languages.lang.aircraft}</Text>

            </View>
            <View style={{ justifyContent: 'center', flex: 0.4, }}>


            </View>

          </View>

        </TouchableOpacity>

        <View style={{ backgroundColor: colors.page_background }}>
          <View style={{ backgroundColor: colors.page_background, height: 90, width: width, marginTop: -70 }} />
          <View style={styles.view_leaderboard}>
            <TouchableOpacity onPress={() => go_LeaderBoard()} style={{ justifyContent: 'center', flex: 0.5 }}>

              <Text style={styles.leaderboard_text}>{languages.lang.leaderboard}</Text>

            </TouchableOpacity>
            <View style={{ height: 40, width: 1, backgroundColor: colors.view_color, alignSelf: 'center' }} />
            <View style={{ justifyContent: 'center', flex: 0.5 }}>
              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={styles.leaderboard_text}>{'Top ' + props.data.worldwide}</Text>
              </View>
              <Text style={styles.From_country_worldwide}>{languages.lang.worldwide}</Text>

            </View>
          </View>

          <View style={styles.view_achivments}>
            <Image source={Images.map} style={styles.logo_lock} />

            <Text style={styles.text_achivement}>{languages.lang.achivements}</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'center', marginLeft: 20, marginRight: 20 }}>
              <View style={{ justifyContent: 'center', flex: 0.5 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <Text style={styles.text_achivement_count}>{props.data.achievement_countries}</Text>
                </View>
                <Text style={styles.From_country_name}>{languages.lang.countries}</Text>

              </View>
              <View style={{ justifyContent: 'center', flex: 0.5, marginTop: 5 }}>

                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <Text style={styles.text_achivement_count}>{props.data.achievement_special}</Text>
                </View>
                <Text style={styles.From_country_name}>{languages.lang.special}</Text>

              </View>

            </View>
            <TouchableOpacity onPress={() => view_all()} style={styles.View_all_button}>
              <Text style={styles.logintext_button}>{languages.lang.view_all}</Text>
            </TouchableOpacity>

          </View>


          <View style={styles.view_wallet}>


            <View style={{ justifyContent: 'center', flex: 0.5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.leaderboard_wallet}>{languages.lang.wallets}</Text>
              </View>
              <Text style={styles.From_country_track_ponits}>{languages.lang.track_points}</Text>

            </View>
            <View style={{ height: 40, width: 1, backgroundColor: colors.view_color, alignSelf: 'center' }} />
            <View style={{ flex: 0.5, paddingLeft: 40 }}>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                <Text style={styles.leaderboard_text}>{'2000'}</Text>
              </View>
              <Text style={styles.From_country_loyalty}>{languages.lang.loyalty_points}</Text>

            </View>
            <TouchableOpacity onPress={() => console.log('here')} style={{
              width: 20,
              height: 20,
              right: 0,
              top: 0,
              marginTop: 8,
              marginRight: 5,
              position: 'absolute'
            }}>
              <Image source={Images.add} style={styles.logo_icon_add} />

            </TouchableOpacity>
          </View>

          <View style={styles.view_tips}>

            <View style={{ justifyContent: 'center', flex: 0.5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.leaderboard_wallet}>{languages.lang.tips}</Text>
              </View>
              <Text style={styles.From_create_video_hint}>{languages.lang.create_video_hint}</Text>

            </View>
            <View style={{ flex: 0.5, paddingLeft: 40 }}>
              <TouchableOpacity onPress={() => view_all()} style={styles.create_video_button}>
                <Text style={styles.logintext_button}>{languages.lang.create_video}</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>


      </ScrollView>

      <Spinner
        visible={props.isLoading}
        textContent={'Loading...'}
        color={'white'}
        overlayColor={'rgba(0, 0, 0, 0.45)'}
        textStyle={styles.spinnerTextStyle}
      />

    </View>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default MyProfileComponent;
