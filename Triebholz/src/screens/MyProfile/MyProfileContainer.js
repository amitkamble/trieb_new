import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import MyProfileComponent from './MyProfileComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const MyProfileContainer = (props) => {

  const { navigation, clearUserObj, route ,get_profile_data} = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };
  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }
  
  useEffect(() => {
    setLoading(false);
    Get_MY_profile();
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
    navigation.navigate('LoginContainer')
  };
  const onBack_click = () => {
    navigation.goBack();
  };
  const add_flight = () => {
    navigation.navigate('Add_Flight')
  };
  const set_widgets = () => {
    navigation.navigate('Widget')
  };

  const CheckList = () => {
    navigation.navigate('Checklist')
  };

  const setting = () => {
    navigation.navigate('Setting')
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);
  const [data, setData] = React.useState({});


  const ScanScreen = () => {
    navigation.navigate('ScanScreen')
  };

  const StatisticDataContainer = () => {
    navigation.navigate('StatisticDataContainer')
  };

  const ViewTips = () => {
    navigation.navigate('Tips')
  };
  const navigate_Review = () => {
    navigation.navigate('Reviews')
  };
  const go_LeaderBoard = () => {
    navigation.navigate('Leaderboard')
  };
  const [trips, setTrips] = useState([]);

  
  const Get_MY_profile = async () => {
    setLoading(true);

    let data = new FormData();
    data.append("user_id", 1);

    await get_profile_data(data)
      .then((response) => {
        setLoading(false);
        var data_response = response.payload.data;
        console.log("api_profile_Response" + JSON.stringify(data_response.trips));
        if(data_response.status == 200){
          setData(data_response)
          setTrips(data_response.trips)
        }else{

        }
      }).catch(error => {
        console.log('error' + JSON.stringify(error));
        Alert.alert(error.error.data)
        setLoading(false);
      });
  }


  return (
    <MyProfileComponent props={props} login_button_pressed={login_button_pressed} onBack_click={onBack_click}
      add_flight={add_flight} set_widgets={set_widgets} ScanScreen={ScanScreen} CheckList={CheckList}
      ViewTips={ViewTips} navigate_Review={navigate_Review} go_LeaderBoard={go_LeaderBoard} data={data}
      StatisticDataContainer={StatisticDataContainer}setting={setting} isLoading={isLoading} trips={trips}/>

  );
};
MyProfileContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
  get_profile_data: (params) => dispatch(Actions.get_profile_data(params)),

});


export default connect(mapStateToProps, mapDispatchToProps)(MyProfileContainer);
