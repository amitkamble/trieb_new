import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, Switch, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import PolylineDirection from '@react-native-maps/polyline-direction';
import MapView, { Polyline, PROVIDER_GOOGLE } from 'react-native-maps';
import * as geolib from 'geolib';
import { ProgressBar, Colors } from 'react-native-paper';
import MyTripsContainer from '../MyTrips/MyTripsContainer';
import languages from '../../globals/languages';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import { store } from '../../redux/actions/store'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import ToggleSwitch from 'toggle-switch-react-native'


const WidgetComponent = memo((props) => {


  const [isalert, setAlert] = useState(false);
  const [is_boarding_pass, setBoardingPass] = useState(false);
  const [isTravelAdvice, setTravelAdvice] = useState(false);
  const [isTravelDocs, setTravelDocs] = useState(false);
  const [isAircraft, setAircraft] = useState(false);
  const [isFrom_flight, setFromFlight] = useState(false);
  const [isTo_flight, setToFlight] = useState(false);
  const [isAirlines, setAirlines] = useState(false);
  const [isBaggage, setBaggage] = useState(false);
  const [isShopping, setShopping] = useState(false);
  const [isEnabled, setisEnabled] = useState(false);

  const ScanScreen = () => {
    props.ScanScreen();
  }

  const onBack = () => {
    props.onBack();
  }

  const center = geolib.getCenter([
    { latitude: 28.2014833, longitude: -177.3813083 },
    { latitude: 39.849312, longitude: -104.673828 },
  ]);

  return (
    <ScrollView contentContainerStyle={styles.container}>

      <View style={styles.logo_map} >

        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 50, }}>

          <Text style={styles.logintitle}>{Language.lang.setup_widget}</Text>
          <TouchableOpacity onPress={() => onBack()} style={{
            position: 'absolute',
            right: 0,
            marginTop: 10,
            marginRight: 20
          }}>
            <Image source={Images.close} style={styles.logo_lock} />

          </TouchableOpacity>

        </View>
      </View>


      <View style={{ flexDirection: 'row', marginLeft: 20, width: width - 40, backgroundColor: 'white', padding: 20, marginTop: 20 }}>
        <View style={{ width: 2, height: 62.5, position: 'absolute', backgroundColor: colors.main_dark_color }} />
        <Text style={styles.From_country_name}>{languages.lang.alerts}</Text>
        <View style={{ alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
          <ToggleSwitch
            isOn={props.isalert}
            onColor={colors.green_text_coot}
            offColor="gray"
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="small"
            onToggle={() => props.alertAction()}
          />
        </View>
      </View>

      <View style={{ flexDirection: 'row', marginLeft: 20, width: width - 40, backgroundColor: 'white', padding: 20, marginTop: 10 }}>
        <View style={{ width: 2, height: 62.5, position: 'absolute', backgroundColor: colors.main_dark_color }} />
        <Text style={styles.From_country_name}>{languages.lang.boarding_passes}</Text>
        <View style={{ alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
          <ToggleSwitch
            isOn={props.is_boarding_pass}
            onColor={colors.green_text_coot}
            offColor="gray"
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="small"
            onToggle={() => props.boarding_Action()}
          />
        </View>
      </View>

      <View style={{ flexDirection: 'row', marginLeft: 20, width: width - 40, backgroundColor: 'white', padding: 20, marginTop: 10 }}>
        <View style={{ width: 2, height: 62.5, position: 'absolute', backgroundColor: colors.main_dark_color }} />
        <Text style={styles.From_country_name}>{languages.lang.travel_advice}</Text>
        <View style={{ alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
          <ToggleSwitch
            isOn={props.isTravelAdvice}
            onColor={colors.green_text_coot}
            offColor="gray"
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="small"
            onToggle={() => props.travel_advice_Action()}
          />
        </View>
      </View>

      <View style={{ flexDirection: 'row', marginLeft: 20, width: width - 40, backgroundColor: 'white', padding: 20, marginTop: 10 }}>
        <View style={{ width: 2, height: 62.5, position: 'absolute', backgroundColor: colors.main_dark_color }} />
        <Text style={styles.From_country_name}>{languages.lang.travel_docs}</Text>
        <View style={{ alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
          <ToggleSwitch
            isOn={props.isTravelDocs}
            onColor={colors.green_text_coot}
            offColor="gray"
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="small"
            onToggle={() => props.travel_docs_Action()}
          />
        </View>
      </View>

      <View style={{ flexDirection: 'row', marginLeft: 20, width: width - 40, backgroundColor: 'white', padding: 20, marginTop: 10 }}>
        <View style={{ width: 2, height: 62.5, position: 'absolute', backgroundColor: colors.main_dark_color }} />
        <Text style={styles.From_country_name}>{languages.lang.aircraft}</Text>
        <View style={{ alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
          <ToggleSwitch
            isOn={props.isAircraft}
            onColor={colors.green_text_coot}
            offColor="gray"
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="small"
            onToggle={() => props.aircraft_Action()}
          />
        </View>
      </View>

      <View style={{ flexDirection: 'row', marginLeft: 20, width: width - 40, backgroundColor: 'white', padding: 20, marginTop: 10 }}>
        <View style={{ width: 2, height: 62.5, position: 'absolute', backgroundColor: colors.main_dark_color }} />
        <Text style={styles.From_country_name}>{ props.data_selected.code_name_from == undefined ? languages.lang.departure_airport  :props.data_selected.code_name_from + ', ' + props.data_selected.from_name}</Text>
        <View style={{ alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
          <ToggleSwitch
            isOn={props.isFrom_flight}
            onColor={colors.green_text_coot}
            offColor="gray"
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="small"
            onToggle={() => props.from_flight_Action()}
          />
        </View>
      </View>

      <View style={{ flexDirection: 'row', marginLeft: 20, width: width - 40, backgroundColor: 'white', padding: 20, marginTop: 10 }}>
        <View style={{ width: 2, height: 62.5, position: 'absolute', backgroundColor: colors.main_dark_color }} />
        <Text style={styles.From_country_name}>{props.data_selected.code_name_to == undefined ? languages.lang.arrival_airport  : props.data_selected.code_name_to + ', ' + props.data_selected.to_name}</Text>
        <View style={{ alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
          <ToggleSwitch
            isOn={props.isTo_flight}
            onColor={colors.green_text_coot}
            offColor="gray"
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="small"
            onToggle={() => props.to_flight_Action()}
          />
        </View>
      </View>

      <View style={{ flexDirection: 'row', marginLeft: 20, width: width - 40, backgroundColor: 'white', padding: 20, marginTop: 10 }}>
        <View style={{ width: 2, height: 62.5, position: 'absolute', backgroundColor: colors.main_dark_color }} />
        <Text style={styles.From_country_name}>{languages.lang.airlines}</Text>
        <View style={{ alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
          <ToggleSwitch
            isOn={props.isAirlines}
            onColor={colors.green_text_coot}
            offColor="gray"
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="small"
            onToggle={() => props.airlines_flight_Action()}
          />
        </View>
      </View>

      <View style={{ flexDirection: 'row', marginLeft: 20, width: width - 40, backgroundColor: 'white', padding: 20, marginTop: 10 }}>
        <View style={{ width: 2, height: 62.5, position: 'absolute', backgroundColor: colors.main_dark_color }} />
        <Text style={styles.From_country_name}>{languages.lang.baggage}</Text>
        <View style={{ alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
          <ToggleSwitch
            isOn={props.isBaggage}
            onColor={colors.green_text_coot}
            offColor="gray"
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="small"
            onToggle={() => props.baggeges_fight_Action()}
          />
        </View>
      </View>

      <View style={{ flexDirection: 'row', marginLeft: 20, width: width - 40, backgroundColor: 'white', padding: 20, marginTop: 10 }}>
        <View style={{ width: 2, height: 62.5, position: 'absolute', backgroundColor: colors.main_dark_color }} />
        <Text style={styles.From_country_name}>{languages.lang.shopping}</Text>
        <View style={{ alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
          <ToggleSwitch
            isOn={props.isShopping}
            onColor={colors.green_text_coot}
            offColor="gray"
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="small"
            onToggle={() => props.shopping_fight_Action()}
          />
        </View>
      </View>

    </ScrollView>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default WidgetComponent;
