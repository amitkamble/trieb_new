import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { colors, fonts, sizes } from '../../utils/theme';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const ScanScreenComponent = memo((props) => {

  const [value, setValue] = useState('');

  const onSelect = () => {
    if(value != ''){
      props.onSelect(value);

    }else{
      alert('Scan Doc First !')
    }
  }

  return (
    <QRCodeScanner
      cameraStyle = {{height:height-140, backgroundColor:'black'}}
      onRead={(values)=> setValue(values.data)}
      flashMode={RNCamera.Constants.FlashMode.off}
      reactivate = {true}
      reactivateTimeout = {1000}
      showMarker={true}
      clickable={false}
      bottomContent={
        <TouchableOpacity onPress={()=>onSelect()} style={styles.buttonTouchable}>
          <Text style={styles.buttonText}>OK. Got it!</Text>
        </TouchableOpacity>
      }
    />
  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default ScanScreenComponent;
