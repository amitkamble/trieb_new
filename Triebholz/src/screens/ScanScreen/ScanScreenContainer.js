import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import ScanScreenComponent from './ScanScreenComponent';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import * as Actions from '../../redux/actions/userSessionActions';

const ScanScreenContainer = (props) => {

  const { navigation,selected_data } = props;

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }

  const onSelect = async(value) => {
      // await selected_data(value)
      navigation.navigate('Select_FlightContainer',{value : value})
  };

  return (
    <ScanScreenComponent props={props} onSelect={onSelect}/>

  );
};
ScanScreenContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({}) => ({

});

const mapDispatchToProps = (dispatch) => ({
  selected_data: (params) => dispatch(Actions.selected_data(params))
});


export default connect(mapStateToProps, mapDispatchToProps)(ScanScreenContainer);
