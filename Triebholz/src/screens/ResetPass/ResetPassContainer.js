import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import ResetPassComponent from './ResetPassComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const ResetPassContainer = (props) => {

  const { navigation,  clearUserObj, route } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };
  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }

  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const reset_Pass = () => {
    navigation.navigate('Reset_success')
  };

  const onBack_click = () => {
    navigation.goBack();
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);



  return (
    <ResetPassComponent props={props} reset_Pass={reset_Pass} isLoading={isLoading} onBack_click={onBack_click}/>

  );
};
ResetPassContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
});


export default connect(mapStateToProps, mapDispatchToProps)(ResetPassContainer);
