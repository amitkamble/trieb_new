import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView } from 'react-native';
import styles from './styles';
import { Images } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import OTPInputView from '@twotalltotems/react-native-otp-input'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const ResetPassComponent = memo((props) => {


  // Storing Name & Pass
  const [user, set_user] = useState({
    pass: '',
    confirm_pass: '',
  });

  const reset_Pass = () => {

    if (user.pass.trim().length && user.confirm_pass.trim().length) {
      if (user.pass == user.confirm_pass) {
        if(user.pass.trim().length>5){
          props.reset_Pass(otp);

        }else{
          alert('Password length must be 6 characters !')

        }

      } else {
        alert('Password mismatch !')
      }

    }else if(!user.pass.trim().length && !user.confirm_pass.trim().length){
      setFocus_confirm_pass_error(true);
      setFocus_pass_error(true);

    }else if(!user.pass.trim().length && user.confirm_pass.trim().length){
      setFocus_confirm_pass_error(true);
      setFocus_pass_error(false);

    }else if(user.pass.trim().length && !user.confirm_pass.trim().length){
      setFocus_confirm_pass_error(false);
      setFocus_pass_error(true);

    }

  };

  const [on_pass_error, setFocus_pass_error] = useState(false);
  const [on_confirm_pass_error, setFocus_confirm_pass_error] = useState(false);
  
  const [on_secure, setSecurity] = useState(true);
  const [on_secure_confirm, setSecurity_confirm] = useState(true);
  //to set Focus for Password Field
  const [on_pass, setFocus_pass] = useState(false);

  const [on_confirm_pass, setFocus_confirm_pass] = useState(false);
 
  const onBack_click = () => {

    props.onBack_click();

  };

   //to check and set focus error field or password  
   const here_pass = (value) => {
    set_user({ ...user, ['pass']: value })
    if (value == '') {
      setFocus_pass_error(true)

    } else {
      setFocus_pass_error(false)

    }
  };
  //to check and set focus error field or password  
  const here__confirm_pass = (value) => {
    set_user({ ...user, ['confirm_pass']: value })
    if (value == '') {
      setFocus_confirm_pass_error(true)

    } else {
      setFocus_confirm_pass_error(false)

    }

  };

  // Storing Name & Pass
  const [otp, set_otp] = useState('');

  //to hide & show error for input_text
  const [on_user_error, setFocus_user_error] = useState(false);

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
      <StatusBar translucent backgroundColor={colors.main_dark_color} />
      <View style={{
        height: 100,
        width: width,
      }}>
        <View style={styles.back_image} />

        <Image source={Images.circle_login} style={styles.logo_new} />
        <TouchableOpacity onPress={() => onBack_click()} style={{
          position: 'absolute',
          marginTop: 50,
          marginLeft: 15,
        }}>
          <Image source={Images.back} style={styles.logo_back} />
        </TouchableOpacity>
      </View>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

        <View safe style={styles.container}>

          <StatusBar barStyle="light-content" translucent={true} />

          <Image source={Images.lock_reset_pass} style={styles.logo_mobile} />


          <Text style={styles.logintitle}>Reset Password</Text>

          <Text style={styles.logintext}>{'Password must be 6 characters'}</Text>
          <Text style={styles.mobile_notext}>{props.mobile_no}</Text>

          <View style={styles.sub_container_field}>

          <Text style={styles.HedingText_pass}>{!on_pass ? user.pass != '' ? "New Password" : ''
              : 'New Password'}</Text>
            <View style={styles.sub_container}>

              <TextInput
                placeholder={
                  !on_pass ? user.pass == '' ? "New Password" : ''
                    : ''
                }
                onFocus={() => setFocus_pass(true)}
                onBlur={() => setFocus_pass(false)}
                onChangeText={(value) => here_pass(value)}
                value={user.pass}
                placeholderTextColor={colors.gray}
                secureTextEntry={on_secure}
                autoCorrect={false}
                style={styles.inputText} />

              <TouchableOpacity style={{
                right: 0,
                height: 20,
                width: 20,
                position: 'absolute',
                marginRight: 23,
                alignSelf: 'center'
              }} onPress={() => setSecurity(on_secure ? false : true)} >

                <Image source={user.pass != '' ? on_secure ? Images.pass_not_visible : Images.pass_visible : Images.lock} style={user.pass != '' ? styles.logo_pass : styles.logo_lock} />
              </TouchableOpacity>
            </View>
            {
              on_pass_error ?
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.errorText_pass}>{'Please Enter Password'}</Text>
                </View>
                : null
            }


            <Text style={styles.HedingText_pass}>{!on_confirm_pass ? user.confirm_pass != '' ? "Confirm Password" : ''
              : 'Confirm Password'}</Text>

            <View style={styles.sub_container}>

              <TextInput
                placeholder={
                  !on_confirm_pass ? user.confirm_pass == '' ? "Confirm Password" : ''
                    : ''
                }
                onFocus={() => setFocus_confirm_pass(true)}
                onBlur={() => setFocus_confirm_pass(false)}
                onChangeText={(value) => here__confirm_pass(value)}
                value={user.confirm_pass}
                placeholderTextColor={colors.gray}
                secureTextEntry={on_secure_confirm}
                autoCorrect={false}
                style={styles.inputText} />

              <TouchableOpacity style={{
                right: 0,
                height: 20,
                width: 20,
                position: 'absolute',
                marginRight: 23,
                alignSelf: 'center'
              }} onPress={() => setSecurity_confirm(on_secure_confirm ? false : true)} >

                <Image source={user.confirm_pass != '' ? on_secure_confirm ? Images.pass_not_visible : Images.pass_visible : Images.lock} style={user.confirm_pass != '' ? styles.logo_pass : styles.logo_lock} />
              </TouchableOpacity>
            </View>
            {
              on_confirm_pass_error ?
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.errorText_pass}>{'Please Enter Password'}</Text>
                </View>
                : null
            }
            <TouchableOpacity onPress={() => reset_Pass()} style={styles.login}>
              <Text style={styles.logintext_button}>RESET PASSWORD</Text>
            </TouchableOpacity>

          </View>
          
        </View>


      </TouchableWithoutFeedback>


      <Spinner
        visible={props.isLoading}
        textContent={'Loading...'}
        color={'white'}
        overlayColor={'rgba(0, 0, 0, 0.45)'}
        textStyle={styles.spinnerTextStyle}
      />

    </ScrollView>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

export default ResetPassComponent;
