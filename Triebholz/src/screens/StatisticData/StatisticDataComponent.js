import React, { memo, useState, useRef } from 'react';
import { View, FlatList, Dimensions, TextInput, Image, StyleSheet, Linking, Alert, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import { colors, fonts, sizes } from '../../utils/theme';
import MapView, { Polyline, PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import languages from '../../globals/languages';
import {
  LineChart,
  BarChart,
} from "react-native-chart-kit";
import { ProgressBar, Colors } from 'react-native-paper';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const StatisticDataComponent = memo((props) => {

  const [coordinates] = useState([

    { latitude: 28.2014833, longitude: -177.3813083 },
    { latitude: 39.849312, longitude: -104.673828 },
  ]);

  const mapDarkStyle = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#868d9c"
        }
      ]
    },
    {
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },

    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#4c4d4f"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "administrative.country",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.locality",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#bdbdbd"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#181818"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#2c2c2c"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#8a8a8a"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#373737"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#3c3c3c"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#4e4e4e"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#4D648D"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#3d3d3d"
        }
      ]
    }
  ];
  const backClick = () => {
    props.onBack_click();
  }

  const data = {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    datasets: [
      {
        data: [20, 45, 28, 80, 99, 43, 20, 45, 28, 80, 99, 43]
      }
    ]
  };

  const go_LeaderBoard = () => {
    props.go_LeaderBoard();
  }

  const chartConfig = {
    backgroundGradientFrom: "#fff",
    backgroundGradientTo: "#fff",
    barPercentage: 0.3,
    height: 350,
    fillShadowGradient: `rgba(38, 51, 77)`,
    fillShadowGradientOpacity: 1,
    decimalPlaces: 0, // optional, defaults to 2dp
    color: (opacity = 1) => `rgba(38, 51, 77)`,
    labelColor: (opacity = 1) => `rgba(0, 0, 0, 1)`,

    style: {
      borderRadius: 16,
      fontFamily: "Bogle-Regular",
    },
    propsForBackgroundLines: {
      strokeWidth: 1,
      stroke: "#e3e3e3",
      strokeDasharray: "0",
    },
    propsForLabels: {
      fontFamily: "Bogle-Regular",
      fontSize: 11
    },
  };

  return (
    <View >


      <TouchableOpacity onPress={() => console.log('here')} style={{ width: width, height: height, flexGrow: 1, position: 'absolute' }}>
        <MapView
          provider={PROVIDER_GOOGLE}
          followUserLocation={true}
          showsUserLocation={true}
          zoomEnabled={true}
          zoomControlEnabled={false}
          style={{ width: width, height: height - 200, }}
          initialRegion={{
            latitude: 39.937328674782975,
            longitude: -143.92914396511438,
            latitudeDelta: Platform.OS == 'ios' ? 60 : 100,
            longitudeDelta: Platform.OS == 'ios' ? 60 : 50
          }}
          showsMyLocationButton={true}
          customMapStyle={mapDarkStyle}
        >
         
        </MapView>
      </TouchableOpacity>

      <ScrollView contentContainerStyle={{ flexGrow: 1, borderRadius: 5, paddingTop: -50, paddingBottom: 400, marginTop: height - 450 }}>


        <ScrollView horizontal style={styles.view_flight_card}>
          <BarChart
            style={{ marginLeft: 5, paddingRight: 10 }}
            data={data}
            width={width - 50}
            height={250}
            chartConfig={chartConfig}
            verticalLabelRotation={60}
            withHorizontalLabels={false}
            fromZero={true}
            withInnerLines={false}
            showBarTops={false}
            showValuesOnTopOfBars={true}
          />
        </ScrollView>

        <View style={{ backgroundColor: colors.page_background }}>

          <View style={styles.view_leaderboard}>
            <View style={{ justifyContent: 'center' }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.leaderboard_wallet}>{languages.lang.top_aircraft}</Text>
              </View>

            </View>
            <View style={{ flexDirection: 'row', width: width - 50 }}>
              <Text style={styles.From_country_track_ponits}>{'Mitsubishi Zero'}</Text>

              <ProgressBar style={{ borderRadius: 1, height: 15, width: 160, marginLeft: 5, marginTop: 15 }} progress={60 / 100} color={colors.main_color_dark} unfilledColor={colors.gray} />
              <Text style={styles.From_country_track_ponits2}>{'(10)'}</Text>

            </View>
          </View>


          <View style={styles.view_leaderboard}>
            <View style={{ justifyContent: 'center' }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.leaderboard_wallet}>{languages.lang.top_coutries_regions}</Text>
              </View>

            </View>
            <View style={{ flexDirection: 'row', width: width - 50 }}>
              <Text style={styles.From_country_track_ponits}>{'United States of America'}</Text>

              <ProgressBar style={{ borderRadius: 1, height: 15, width: 160, marginLeft: 5, marginTop: 15 }} progress={60 / 100} color={colors.main_color_dark} unfilledColor={colors.gray} />
              <Text style={styles.From_country_track_ponits2}>{'(10)'}</Text>

            </View>
          </View>
          
          <View style={{ backgroundColor: colors.page_background }}>

            <View style={styles.view_leaderboard}>
              <View style={{ justifyContent: 'center' }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.leaderboard_wallet}>{languages.lang.most_visited_airport}</Text>
                </View>

              </View>
              <View style={{ flexDirection: 'row', flex: 1 }}>
                <Text style={styles.From_country_track_ponits2}>{'JFK'}</Text>

                <ProgressBar style={{ borderRadius: 1, height: 15, width: width - 160, marginLeft: 15, marginTop: 15 }} progress={60 / 100} color={colors.main_color_dark} unfilledColor={colors.gray} />
                <Text style={styles.From_country_track_ponits2}>{'(10)'}</Text>

              </View>
              <View style={{ flexDirection: 'row', flex: 1 }}>
                <Text style={styles.From_country_track_ponits2}>{'JFK'}</Text>

                <ProgressBar style={{ borderRadius: 1, height: 15, width: width - 160, marginLeft: 15, marginTop: 15 }} progress={60 / 100} color={colors.main_color_dark} unfilledColor={colors.gray} />
                <Text style={styles.From_country_track_ponits2}>{'(10)'}</Text>

              </View>
            </View>
          </View>


          <View style={styles.view_leaderboard}>
            <View style={{ justifyContent: 'center' }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.leaderboard_wallet}>{languages.lang.top_airlines}</Text>
              </View>

            </View>
            <View style={{ flexDirection: 'row', width: width - 50 }}>
              <Text style={styles.From_country_track_ponits2}>{'W6'}</Text>

              <ProgressBar style={{ borderRadius: 1, height: 15, width: width - 160, marginLeft: 15, marginTop: 15 }} progress={60 / 100} color={colors.main_color_dark} unfilledColor={colors.gray} />
              <Text style={styles.From_country_track_ponits2}>{'(10)'}</Text>

            </View>
          </View>

        </View>


      </ScrollView>

      <View style={{ position: 'absolute', justifyContent: 'center', left: 0, top: 0, width: width, marginTop: 10 }}>
        <TouchableOpacity onPress={() => backClick()} style={{ position: 'absolute', left: 0, top: 0, marginLeft: 20, marginTop: 25 }}>
          <Image source={Images.back_round} style={styles.logo_widget} />
        </TouchableOpacity>

        <TouchableOpacity style={{ position: 'absolute', right: 0, top: 0, marginRight: 20, marginTop: 25 }}>
          <Image source={Images.share_blue_round} style={styles.logo_widget} />
        </TouchableOpacity>
      </View>
    </View>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default StatisticDataComponent;
