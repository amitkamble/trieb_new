import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import StatisticDataComponent from './StatisticDataComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const StatisticDataContainer = (props) => {

  const { navigation, clearUserObj, route } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };
  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }
  const [data, setTerminal_data] = React.useState([
    {
      name: 'Terminal 1',
      url: 'http://www.africau.edu/images/default/sample.pdf',
    },
    {
      name: 'Terminal 2',
      url: 'http://www.africau.edu/images/default/sample.pdf',
    },
    {
      name: 'Terminal 3',
      url: 'http://www.africau.edu/images/default/sample.pdf',
    },
    {
      name: 'Terminal 4',
      url: 'http://www.africau.edu/images/default/sample.pdf',
    },
  ]);

  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
    navigation.navigate('LoginContainer')
  };
  const onBack_click = () => {
    navigation.goBack();
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);


  const ScanScreen = () => {
    navigation.navigate('ScanScreen')
  };

  const ViewTips = () => {
    navigation.navigate('Tips')
  };
  const navigate_Review = () => {
    navigation.navigate('Reviews')
  };
  const go_LeaderBoard = () => {
    navigation.navigate('Leaderboard')
  };


  return (
    <StatisticDataComponent props={props} login_button_pressed={login_button_pressed} onBack_click={onBack_click}
       ScanScreen={ScanScreen} data={data}
      ViewTips={ViewTips} navigate_Review={navigate_Review} go_LeaderBoard={go_LeaderBoard}/>

  );
};
StatisticDataContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
});


export default connect(mapStateToProps, mapDispatchToProps)(StatisticDataContainer);
