import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import HomePageComponent from './HomePageComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import * as geolib from 'geolib';
import JulianDate from 'julian-date';

const HomePageContainer = (props) => {

  const { navigation, clearUserObj, route, latest_flight } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };
  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }
  const [data_upcomming, setDataUpcoming] = useState({});
  const [center, setCenter] = useState({});
  const [is_data_upcoming, SetUpcoming] = useState(false);
  // Storing Name & Pass
  //  var center;
  // Storing Name & Pass
  const [from_flight, setFromFlight] = useState({});
  var dayCount = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  const [to_flight, setToFlight] = useState({});
  useEffect(() => {
    setLoading(false);
   
    const unsubscribe = navigation.addListener('focus', () => {
      var j1 = julianIntToDate(225)
      console.log('here_date',(j1).toString())
      Get_MY_Trips();
      BackHandler.addEventListener('hardwareBackPress', (handleBackButtonClick));

    });
    return unsubscribe;
  }, []);

  function julianIntToDate(JD) {
    // convert a Julian number to a Gregorian Date.
    //    S.Boisseau / BubblingApp.com / 2014
    var y = 4716;
    var v = 3;
    var j = 1401;
    var u =  5;
    var m =  2;
    var s =  153;
    var n = 12;
    var w =  2;
    var r =  4;
    var B =  274277;
    var p =  1461;
    var C =  -38;
    var f = JD + j + Math.floor((Math.floor((4 * JD + B) / 146097) * 3) / 4) + C;
    var e = r * f + v;
    var g = Math.floor((e % p) / r);
    var h = u * g + w;
    var M = ((Math.floor(h / s) + m) % n) + 1;
    if(M%2 == 0 && M != 8){
      var D = Math.floor((h % s) / u) + 9;

    }else{
      var D = Math.floor((h % s) / u) + 8;

    }
    var Y = Math.floor(e / p) - y + Math.floor((n + m - M) / n) ;
    return new Date(Y,M,D);
}

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
    navigation.navigate('LoginContainer')
  };

  const MyTrips = () => {
    navigation.navigate('MyTrips')
  };

  const my_trips = () => {
    navigation.navigate('MyTrips')
  };

  const MyProfile = () => {
    navigation.navigate('MyProfile')
  };

  const MyInbox = () => {
    navigation.navigate('Inbox')
  };

  const Add_Flight = () => {
    navigation.navigate('Add_Flight')
  };

  const detailsPage = () => {
    navigation.navigate('MyTripsDetails', { data_selected: data_upcomming })
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);


  const Get_MY_Trips = async () => {
    setLoading(true);

    let data = new FormData();
    data.append("user_id", 1);

    await latest_flight(data)
      .then((response) => {
        setLoading(false);
        var data_response = response.payload.data.latest.trips[0];
        console.log("api_login_Response" + JSON.stringify(data_response));

        if (data_response == null || data_response == undefined) {
          SetUpcoming(false)
        } else {
          SetUpcoming(true)
          setDataUpcoming(data_response)
          setFromFlight(data_response.from_flight)
          setToFlight(data_response.to_flight)
          const center_value = geolib.getCenter([
            { latitude: data_response.from_flight.latitude, longitude: data_response.from_flight.longitude },
            { latitude: data_response.to_flight.latitude, longitude: data_response.to_flight.longitude },
          ]);

          setCenter(center_value);

          console.log(data_response.from_flight, data_response.to_flight)

        }


      }).catch(error => {
        console.log('error' + JSON.stringify(error));
        Alert.alert(error.error.data)
        setLoading(false);
      });
  }

  return (
    <HomePageComponent props={props} login_button_pressed={login_button_pressed} MyTrips={MyTrips}
      Add_Flight={Add_Flight} detailsPage={detailsPage} my_trips={my_trips} MyProfile={MyProfile} MyInbox={MyInbox}
      data_upcomming={data_upcomming} is_data_upcoming={is_data_upcoming} isLoading={isLoading}
      from_flight={from_flight} to_flight={to_flight} center={center} />

  );
};
HomePageContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
  latest_flight: (params) => dispatch(Actions.latest_trip(params))
});


export default connect(mapStateToProps, mapDispatchToProps)(HomePageContainer);
