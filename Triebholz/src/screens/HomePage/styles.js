import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors, fonts, sizes } from '../../utils/theme';

import Icon from 'react-native-vector-icons/Ionicons';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default StyleSheet.create({
  container: {
    flexGrow: 1,
  },

  logo_map: {
    height:600,
    width: width+40,
    position: 'absolute',
  },

  logo_lock: {
    height: 30,
    width: 30,
    position: 'absolute',
    right: 0,
    marginTop: 70,
    marginRight: 20
  },

  logo_icon: {
    height: 35,
    width: 35,
    marginBottom:20
  },

  logo_icon_message: {
    height: 32,
    width: 38.5,
    marginBottom:20
  },

  logo_icon_flight: {
    height: 42,
    width:150,
    resizeMode:'contain',
    position : 'absolute',
    alignSelf:'center',
  },

  logo_icon_country: {
    width:15,
    height: 8,
    alignSelf:'center',
    marginTop:3
  },

  logo_icon_next: {
    width:15,
    height: 15,
    marginTop:123,
    marginRight:120
  },

  logo_icon_pre: {
    width:15,
    height: 15,
    marginTop:123,
    marginLeft:120
  },

  logo_new: {
    height: 95,
    width: 93,
    position: 'absolute',
    marginTop: -35,
    marginRight: 50,
    right: 0,
    top: 0
  },

  logintitle: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.SemiBold,
    alignSelf: 'center',
    marginTop: 70,
    marginLeft: 20
  },


  From_flight: {
    color: colors.main_dark_color,
    fontSize: 24,
    fontFamily: fonts.SemiBold,
    marginTop: 5,
    marginLeft: 20,
    flex:1,
  },

  From_country_name: {
    color: colors.main_dark_color,
    fontSize: 12,
    fontFamily: fonts.Regular,
    marginLeft: 5,
    alignSelf:'center',
    marginTop:Platform.OS =='ios'? 3 : 0

  },

  journey_hours: {
    color: colors.main_dark_color,
    fontSize: 16,
    fontFamily: fonts.Monsterat_Regular,
    marginLeft: 5,
    textAlignVertical:'top'
  },

 
  To_flight: {
    color: colors.main_dark_color,
    fontSize: 24,
    fontFamily: fonts.SemiBold,
    marginTop: 5,
    paddingRight: 40,
    flex:1,
    textAlign:'right'
  },

  headingtitle: {
    color: colors.white,
    fontSize: 16,
    fontFamily: fonts.SemiBold,
    marginTop: 30,
    marginLeft: 20
  },

  card_date: {
    color: colors.white,
    fontSize: 13,
    backgroundColor:colors.main_color_dark,
    fontFamily: fonts.Monsterat_SemiBold,
    padding:5,
    paddingLeft:10,
    paddingRight:10,
    textAlignVertical:'center',
    borderBottomLeftRadius:5,
    borderBottomRightRadius:5,
    alignSelf:'center'
  },

  card_fight_name: {
    color: colors.main_dark_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    marginTop:20,
    alignSelf:'center',
    paddingLeft:20,
    paddingRight:20
  },

  from_time : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    marginTop:20,
    paddingLeft:20,
    flex:1
  },

  to_time : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    marginTop:20,
    paddingRight:30,
    flex:1,
    textAlign:'right'
  },

  check_in : {
    color: colors.black,
    fontSize: 16,
    fontFamily: fonts.Monsterat_Medium,
    textAlignVertical:'center',
    paddingLeft:20,
    flex:1
  },

  check_in_time : {
    color: colors.hint_color,
    fontSize: 14,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    paddingRight:25,
    flex:1,
    textAlign:'right'
  },
  on_time : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft:20,
    textAlignVertical:'center',
    flex:1,
  },
  terminal : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft:10,
    textAlignVertical:'center',
  },
  terminal_no : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft:5,
    textAlignVertical:'center',
  },
  card_title: {
    color: colors.main_dark_color,
    fontSize: sizes.regular,
    fontFamily: fonts.SemiBold,
  },

  view_home_card: {
    flex: 1,
    margin: 10,
    height: 140,
    backgroundColor: 'white',
    borderRadius: 5,
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 8,
    alignItems:'center',
    justifyContent:'center'
  },

  view_flight_card: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 5,
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 8,
  },

  View_all: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 50,
    paddingLeft:50,
    paddingRight:50,
    alignSelf: 'center',
    borderRadius: 5,
    marginTop: 30,
    marginBottom:20,
    backgroundColor: colors.main_dark_color,
  },

  View_add: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 50,
    width:115,
    alignSelf: 'flex-start',
    borderRadius: 5,
    marginTop: 15,
    marginLeft:20,
    marginBottom:20,
    backgroundColor: colors.main_dark_color,
  },

  logintext_button: {
    color: colors.white,
    fontSize: sizes.regular,
    fontFamily: fonts.SemiBold,
    alignSelf:'center',
  },

  spinnerTextStyle: {
    color: '#fff'
  },
});
