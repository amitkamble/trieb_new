import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import PolylineDirection from '@react-native-maps/polyline-direction';
import MapView, { Polyline, PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import * as geolib from 'geolib';
import { ProgressBar, Colors } from 'react-native-paper';
import MyTripsContainer from '../MyTrips/MyTripsContainer';
import languages from '../../globals/languages';
import Moment from 'moment';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const HomePageComponent = memo((props) => {

  const [data] = useState(true);
  const mapDarkStyle = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#868d9c"
        }
      ]
    },
    {
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },

    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#4c4d4f"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "administrative.country",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.locality",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#bdbdbd"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#181818"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#2c2c2c"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#8a8a8a"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#373737"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#3c3c3c"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#4e4e4e"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#4D648D"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#3d3d3d"
        }
      ]
    }
  ];
  const origin = { latitude: 19.363631, longitude: -99.182545 };
  const destination = { latitude: 19.2932543, longitude: -99.1794758 };
  const GOOGLE_MAPS_APIKEY = 'AIzaSyC4Js0D5FmUOT46Nly7iSF_iMrLLUGEmrc';
  const LATITUDE_DELTA = 200;
  const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);
  const [coordinates] = useState([
    { latitude: 28.2014833, longitude: -177.3813083 },
    { latitude: 39.849312, longitude: -104.673828 },
  ]);

  const [user, set_user] = useState({
    pass: '',
    confirm_pass: '',
  });

  const view_all = () => {
    props.my_trips();
  }
  const mydetails_page = () => {
    props.detailsPage();
  }

  const Add_Flight = () => {
    props.Add_Flight();
  }

  const MyProfile = () => {
    props.MyProfile();
  }
  const MyTrips = () => {
    props.MyTrips();
  }
  const MyInbox = () => {
    props.MyInbox();
  }

  // const center = geolib.getCenter([
  //   { latitude: props.data_upcomming.from_flight.latitude, longitude: props.data_upcomming.from_flight.longitude },
  //   { latitude: props.data_upcomming.to_flight.latitude, longitude: props.data_upcomming.to_flight.longitude  },
  // ]);
  // console.log(props.data_upcomming.from_flight.latitude)

  return (
    <ScrollView contentContainerStyle={styles.container}>

      <Image source={Images.home_back} style={styles.logo_map} />

      <Image source={Images.circle_login} style={styles.logo_new} />

      <View style={{ flexDirection: 'row' }}>

        <Text style={styles.logintitle}>{Language.lang.home_title}</Text>
        <Image source={Images.logo_app} style={styles.logo_lock} />

      </View>

      <View style={{ flexDirection: 'row', marginTop: 20, padding: 10 }}>
        <TouchableOpacity onPress={() => MyTrips()} style={styles.view_home_card}>
          <Image source={Images.worldwide} style={styles.logo_icon} />
          <Text style={styles.card_title}>{Language.lang.trips_card}</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => MyProfile()} style={styles.view_home_card}>
          <Image source={Images.user_home} style={styles.logo_icon} />
          <Text style={styles.card_title}>{Language.lang.profile_card}</Text>

        </TouchableOpacity>
        <TouchableOpacity onPress={() => MyInbox()} style={styles.view_home_card}>
          <Image source={Images.message} style={styles.logo_icon_message} />
          <Text style={styles.card_title}>{Language.lang.inbox_card}</Text>

        </TouchableOpacity>
      </View>
      {
        props.is_data_upcoming ?
          <View>

            <Text style={styles.headingtitle}>{Language.lang.upcoming_flight}</Text>

            <View style={styles.view_flight_card}>
              <TouchableOpacity onPress={() => mydetails_page()}>
                <Text style={styles.card_date}>{Moment(props.data_upcomming.take_of_date_time).format('DD MMM')}</Text>
                <Text style={styles.card_fight_name}>{'Flight ' + props.data_upcomming.flight_name}</Text>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.from_time}>{props.data_upcomming.take_of_time}</Text>
                  <Text style={styles.to_time}>{props.data_upcomming.landing_time}</Text>
                </View>

                <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center' }}>
                  <Text style={styles.From_flight}>{props.data_upcomming.code_name_from}</Text>
                  <Image source={Images.flight_icon_from_to} style={styles.logo_icon_flight} />
                  <Text style={styles.To_flight}>{props.data_upcomming.code_name_to}</Text>
                </View>

                <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20, marginRight: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', flex: 0.4 }}>
                    <Image source={Images.coutry1} style={styles.logo_icon_country} />
                    <Text style={styles.From_country_name}>{props.data_upcomming.from_name}</Text>
                  </View>

                  <View style={{ flexDirection: 'row', justifyContent: 'center', flex: 0.8 }}>
                    <Text style={styles.journey_hours}>{props.data_upcomming.hours_journey}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', flex: 0.4 }}>
                    <Image source={Images.coutry1} style={styles.logo_icon_country} />
                    <Text style={styles.From_country_name}>{props.data_upcomming.to_name}</Text>
                  </View>

                </View>
              </TouchableOpacity>

              <View style={{ width: width - 40, height: 200, marginTop: 10 }}>
                <MapView
                  provider={PROVIDER_GOOGLE}
                  followUserLocation={false}
                  showsUserLocation={false}
                  zoomEnabled={true}
                  zoomControlEnabled={false}
                  style={{ flex: 1 }}
                  initialRegion={{
                    latitude: props.center.latitude,
                    longitude: props.center.longitude,
                    latitudeDelta: Platform.OS == 'ios' ? 60 : 120,
                    longitudeDelta: Platform.OS == 'ios' ? 60 : 350
                  }}
                  zIndex={9}
                  showsMyLocationButton={false}
                  customMapStyle={mapDarkStyle}
                >
                  <Marker
                    coordinate={{ latitude: props.from_flight.latitude, longitude: props.from_flight.longitude }}
                  >
                    {/* <View> */}
                    <View style={{ backgroundColor: colors.green_text_coot, width: 5, height: 5, borderRadius: 2.5 }}>

                    </View>
                    {/* </View> */}

                  </Marker>
                  <Marker
                    coordinate={{ latitude: props.to_flight.latitude, longitude: props.to_flight.longitude }}
                  >
                    <View style={{ backgroundColor: colors.green_text_coot, width: 5, height: 5, borderRadius: 2.5 }}>

                    </View>

                  </Marker>
                  <Polyline
                    coordinates={
                      [
                        { latitude: props.from_flight.latitude, longitude: props.from_flight.longitude },
                        { latitude: props.to_flight.latitude, longitude: props.to_flight.longitude },
                      ]
                    }
                    strokeColor={'#71e856'} // fallback for when `strokeColors` is not supported by the map-provider
                    strokeWidth={2}
                    lineCap={'round'}
                    geodesic={true}
                    lineJoin={'round'}
                    lineDashPhase={2000}
                  />
                </MapView>
              </View>
              <Swiper showsPagination={false} style={{ height: 170 }} loop={false} showsButtons={true}
                nextButton={<Image source={Images.next_arrow} style={styles.logo_icon_next} />}
                prevButton={<Image source={Images.prev_arrow} style={styles.logo_icon_pre} />}
              >

                <View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                    <Text style={styles.check_in}>{Language.lang.check_in}</Text>
                    {/* <Text style={styles.check_in_time}>{'3 hrs 21 min'}</Text> */}
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                    <Text style={styles.on_time}>{Language.lang.on_time}</Text>
                    <Text style={styles.check_in_time}>{props.from_flight.check_in_time + 'min'}</Text>
                  </View>
                  <ProgressBar style={{ marginLeft: 20, marginRight: 20, marginBottom: 10 }} progress={0.25} color={colors.main_color_dark} />
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10, marginLeft: 10 }}>
                    <Text style={styles.terminal}>{Language.lang.terminal}</Text>
                    <Text style={styles.terminal_no}>{props.data_upcomming.from_terminal_no}</Text>
                    <View style={{ width: 1, height: 15, marginLeft: 15, backgroundColor: colors.gray }} />
                    <Text style={styles.terminal}>{Language.lang.gate}</Text>
                    <Text style={styles.terminal_no}>{props.data_upcomming.from_gate_no}</Text>
                  </View>
                </View>

                <View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                    <Text style={styles.check_in}>{Language.lang.boarding}</Text>
                    {/* <Text style={styles.check_in_time}>{'3 hrs 21 min'}</Text> */}
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                    <Text style={styles.on_time}>{Language.lang.on_time}</Text>
                    <Text style={styles.check_in_time}>{props.from_flight.check_in_time + 'min'}</Text>
                  </View>
                  <ProgressBar style={{ marginLeft: 20, marginRight: 20, marginBottom: 10 }} progress={0.5} color={colors.main_color_dark} />
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10, marginLeft: 10 }}>
                    <Text style={styles.terminal}>{Language.lang.terminal}</Text>
                    <Text style={styles.terminal_no}>{props.data_upcomming.from_terminal_no}</Text>
                    <View style={{ width: 1, height: 15, marginLeft: 15, backgroundColor: colors.gray }} />
                    <Text style={styles.terminal}>{Language.lang.gate}</Text>
                    <Text style={styles.terminal_no}>{props.data_upcomming.from_gate_no}</Text>
                  </View>
                </View>

                <View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                    <Text style={styles.check_in}>{Language.lang.take_off}</Text>
                    {/* <Text style={styles.check_in_time}>{'3 hrs 21 min'}</Text> */}
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                    <Text style={styles.on_time}>{Language.lang.on_time}</Text>
                    <Text style={styles.check_in_time}>{props.from_flight.check_in_time + 'min'}</Text>
                  </View>
                  <ProgressBar style={{ marginLeft: 20, marginRight: 20, marginBottom: 10 }} progress={0.75} color={colors.main_color_dark} />
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10, marginLeft: 10 }}>
                    <Text style={styles.terminal}>{Language.lang.terminal}</Text>
                    <Text style={styles.terminal_no}>{props.data_upcomming.from_terminal_no}</Text>
                    <View style={{ width: 1, height: 15, marginLeft: 15, backgroundColor: colors.gray }} />
                    <Text style={styles.terminal}>{Language.lang.gate}</Text>
                    <Text style={styles.terminal_no}>{props.data_upcomming.from_gate_no}</Text>
                  </View>
                </View>

                <View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                    <Text style={styles.check_in}>{Language.lang.landing}</Text>
                    {/* <Text style={styles.check_in_time}>{'3 hrs 21 min'}</Text> */}
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                    <Text style={styles.on_time}>{Language.lang.on_time}</Text>
                    <Text style={styles.check_in_time}>{props.to_flight.check_in_time + 'min'}</Text>
                  </View>
                  <ProgressBar style={{ marginLeft: 20, marginRight: 20, marginBottom: 10 }} progress={1.0} color={colors.main_color_dark} />
                  <Image source={Images.plane} style={{ width: 20, height: 20, position: 'absolute', right: 0, marginRight: 5, marginTop: Platform.OS == 'ios' ? 68 : 72 }} />
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10, marginLeft: 10 }}>
                    <Text style={styles.terminal}>{Language.lang.terminal}</Text>
                    <Text style={styles.terminal_no}>{props.data_upcomming.to_terminal_no}</Text>
                    <View style={{ width: 1, height: 15, marginLeft: 15, backgroundColor: colors.gray }} />
                    <Text style={styles.terminal}>{Language.lang.gate}</Text>
                    <Text style={styles.terminal_no}>{props.data_upcomming.to_gate_no}</Text>
                    <View style={{ width: 1, height: 15, marginLeft: 15, backgroundColor: colors.gray }} />
                    <Text style={styles.terminal}>{Language.lang.baggage_claim}</Text>
                    <Text style={styles.terminal_no}>{props.data_upcomming.baggage_claim_no}</Text>
                  </View>
                </View>

              </Swiper>
            </View>
            <TouchableOpacity onPress={() => view_all()} style={styles.View_all}>
              <Text style={styles.logintext_button}>{languages.lang.view_all_cap}</Text>
            </TouchableOpacity>
          </View>
          :
          <View>

            <Text style={styles.headingtitle}>{Language.lang.plan_trip}</Text>

            <View style={styles.view_flight_card}>
              <View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                  <Text style={styles.check_in}>{Language.lang.add_manul}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                  <Text style={styles.on_time}>{Language.lang.add_desc}</Text>
                </View>
                <TouchableOpacity onPress={() => Add_Flight()} style={styles.View_add}>
                  <Text style={styles.logintext_button}>{Language.lang.add_flight}</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.view_flight_card}>
              <View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                  <Text style={styles.check_in}>{Language.lang.share_invite}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                  <Text style={styles.on_time}>{Language.lang.share_desc}</Text>
                </View>
                <TouchableOpacity onPress={() => view_all()} style={styles.View_add}>
                  <Text style={styles.logintext_button}>{Language.lang.share}</Text>
                </TouchableOpacity>
              </View>
            </View>

          </View>
      }


      <Spinner
        visible={props.isLoading}
        textContent={'Loading...'}
        color={'white'}
        overlayColor={'rgba(0, 0, 0, 0.45)'}
        textStyle={styles.spinnerTextStyle}
      />
    </ScrollView>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default HomePageComponent;
