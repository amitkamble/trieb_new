import * as ActionTypes from './ActionTypes';
import { connect } from 'react-redux';
import axios from 'react-native-axios';
import ServiceComponent from '../Components/ServiceComponent';

const mapStateToProps = (state) => ({
    isLoading: state.serviceReducer.isLoading,
    status: state.serviceReducer.status,
    data: state.serviceReducer.data
});


const mapDispatchToProps = (dispatch) => ({
    callService: () => dispatch(callWebservice())
})

export const callWebservice = () => {
    return dispatch => {
        dispatch(serviceActionPending())
        axios.get('http://159.65.151.230/public/api/dashboard')
        // axios.get('https://www.reddit.com/r/reactjs.json')
        .then(response => {
            dispatch(serviceActionSuccess(response.data.data))
        })
        .catch(error => {
            dispatch(serviceActionError(error))
        });
    }
}

export const serviceActionPending = () => ({
    type: ActionTypes.SERVICE_PENDING
})

export const serviceActionError = (error) => ({
    type: ActionTypes.SERVICE_ERROR,
    error: error
})

export const serviceActionSuccess = (data) => ({
    type: ActionTypes.SERVICE_SUCCESS,
    data: data
})

export default connect(mapStateToProps, mapDispatchToProps)(ServiceComponent);