import React, { useEffect, useState, useCallback } from 'react';
import { AsyncStorage } from '@react-native-community/async-storage';

const ServiceComponent_Function = (props) => {


    const { navigation } = props;
    const { search, cureentLocation, isPersonOrInVideo, insuranceProvider } = props.route.params

    const setNavigation = () => {
        navigation.titleString = cureentLocation.name
        Navigation.setNavigationDoctorList(navigation)
        navigation.backButtonPress = () => {
            navigation.goBack()
        };
        navigation.filterButtonPress = () => {

            navigation.navigate('SelectFilterContainer', props.route.params)

        };

        navigation.locationButtonPress = () => {


        };
        navigation.sortButtonPress = () => {
            setShowActionSheet(true)
        };
    }


    React.useLayoutEffect(() => {

        setNavigation()

    }, []);


    const isFocused = useIsFocused();

    useEffect(() => {
        setNavigation()
        searchApiCallTemp(tempSort)

    }, [isFocused]);

    const [tempSort, setTempSort] = useState("");
    const [state, setState] = useState([]);
    const [filter, setFilter] = useState([]);
    const [spinnerVisible, setSpinnerVisible] = useState(false);
    const [showActionSheet, setShowActionSheet] = useState(false);
    const actionSheetAction = (index) => {

        setShowActionSheet(false)

        if (index == 0) {
            //Nearest Location
            setTempSort("Nearest Location")
            setTimeout(() => {
                searchApiCallTemp("Nearest Location")
            }, 100);
        } else if (index == 1) {
            //Doctor Availability(Time)
            setTempSort("Doctor Availability(Time)")
            setTimeout(() => {
                searchApiCallTemp("Doctor Availability(Time)")
            }, 100);

        }
        else {

        }

    };


    const searchApiCallTemp = (tempSort1) => {

        var tempsearch = search == undefined ? "" : search
        var tempisPersonOrInVideo = isPersonOrInVideo == undefined ? "" : isPersonOrInVideo
        var tempinsuranceProvider = insuranceProvider == undefined ? "" : insuranceProvider
        var array = []
        if (tempsearch != "") {
            array.push({ name: tempsearch, id: 1 })
        }
        if (tempisPersonOrInVideo != "") {
            array.push({ name: tempisPersonOrInVideo, id: 2 })
        }
        if (tempinsuranceProvider != "") {
            array.push({ name: tempinsuranceProvider, id: 3 })
        }
        if (tempSort1 != "") {
            array.push({ name: tempSort1, id: 4 })
        }
        console.log(array)
        setFilter(array)
        searchApiCall(search, cureentLocation.latitude, cureentLocation.longitude, tempSort1, isPersonOrInVideo == undefined ? "" : isPersonOrInVideo, insuranceProvider == undefined ? "" : insuranceProvider);
    }

    const searchApiCall = useCallback(async (search, latitude, longitude, sortoption, isPersonOrInVideo, insuranceProvider) => {

        setSpinnerVisible(true)


        var inPerson = ""
        var inVideo = ""

        if (isPersonOrInVideo == "In Video") {
            inPerson = ""
            inVideo = "Yes"
        } else if (isPersonOrInVideo == "In Person") {
            inPerson = "Yes"
            inVideo = ""
        } else {
            inPerson = ""
            inVideo = ""
        }
        var postData = { search: search, latitude: latitude, longitude: longitude, sort: sortoption, inPerson: inPerson, inVideo: inVideo, insuranceProvider: insuranceProvider }
        var ApiUrl = API.doctors_list
        console.log(ApiUrl)
        console.log(postWith(postData))
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                setSpinnerVisible(true)
                axios.post(ApiUrl, postData)
                    .then(response => {
                        console.log(response.data);
                        setState(response.data)
                        setTimeout(() => {
                            setSpinnerVisible(false)
                        }, 100);

                    })
                    .catch(error => {
                        setSpinnerVisible(false)
                        setTimeout(() => {
                            console.log(error)
                            ShowAlert(error.message)
                        }, 100);

                    });
            } else {
                setSpinnerVisible(false)
                setTimeout(() => {
                    ShowAlert(ValidationMessages.checkInterneMessage)
                }, 100);
            }
        });


    }, [])


    const rightArrowButtonPress = async (item,item1) => {
        AsyncStorage.setItem('isComeFromHomeOrAppointment', "Home");
        navigation.navigate('DoctorsDetailsContainer', { userId: item.id,selectedTime:item1 })
    };
    const crossButtonPress = (item) => {
        console.log(item)
        setFilter(filter.filter(function (itemT) {
            return itemT.name != item.name;
        }))
        var search1 = search
        var isPersonOrInVideo1 = isPersonOrInVideo
        var insuranceProvider1 = insuranceProvider
        var tempSort1 = tempSort


        if (item.id == 1) {
            search1 = ""
            props.route.params.search = ""

        }
        if (item.id == 2) {
            isPersonOrInVideo1 = ""
            props.route.params.isPersonOrInVideo = ""
        }
        if (item.id == 3) {
            insuranceProvider1 = ""
            props.route.params.insuranceProvider = ""
        }
        if (item.id == 4) {
            tempSort1 = ""
            setTempSort("")
        }
        searchApiCall(search1, cureentLocation.latitude, cureentLocation.longitude, tempSort1, isPersonOrInVideo1 == undefined ? "" : isPersonOrInVideo1, insuranceProvider1 == undefined ? "" : insuranceProvider1);

    };


    return (

        <DoctorsListComponent props={props} showActionSheet={showActionSheet} actionSheetAction={actionSheetAction} spinnerVisible={spinnerVisible} state={state} crossButtonPress={crossButtonPress} filter={filter} rightArrowButtonPress={rightArrowButtonPress} />);

};

const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(DoctorsListContainer);

