import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './Reducers/Index';
import ServiceAction from './Actions/ServiceCallAction';
import {LogBox } from 'react-native';

export default class Async_Redux extends Component {
    
    render() {
        LogBox.ignoreLogs(['Reanimated 2']);
        return (
            <Provider store={store}>
                <ServiceAction />
            </Provider>
        );
    }
}

