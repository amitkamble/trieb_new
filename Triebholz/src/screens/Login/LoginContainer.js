import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import LoginComponent from './LoginComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, View, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const LoginContainer = (props) => {

  const { navigation, clearUserObj, route, userlogin,alert_action,setUserSession } = props;
  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }

  useEffect(() => {
    setLoading(false);
    // BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    const unsubscribe = navigation.addListener('focus', () => {

      BackHandler.addEventListener('hardwareBackPress', (handleBackButtonClick));

    });

    function handleBackButtonClick() {
      Alert.alert("Alert!", "Are you sure you want close app?", [
        {
          text: "Cancel",
          onPress: () => null,
          style: "cancel"
        },
        { text: "YES", onPress: () => BackHandler.exitApp() }
        // { text: "YES", onPress: () => navigation.navigate('Demo') }
      ]);
      return true;
    }

    return unsubscribe;
  }, []);

  const login_button_pressed = async (user) => {
    setLoading(true);
    user_login(user);
  };

  const sign_up = () => {
    navigation.navigate('SignUp')
  };

  const ForgetPass = () => {
    navigation.navigate('ForgetPass')
  };


  const add_value = async() => {
    await alert_action()
    console.log(store.getState().userSession.alert);
  };


  // Login 
  const user_login = async (user) => {

    let data = new FormData();
    data.append("email", user.email);
    data.append("password", user.password);

    console.log("token_id" + JSON.stringify(data))

    await userlogin(data)
      .then((response) => {
        setLoading(false);
        var data_response = response.payload.data;
        console.log("api_login_Response" + JSON.stringify(data_response));

        if(data_response.status == 200){
          navigation.navigate('HomePage')
          setUserSession()
        }else{
          alert(data_response.message)
        }

      }).catch(error => {
        console.log('error' + JSON.stringify(error));
        Alert.alert(error.error.data)
        setLoading(false);
      });
  }

  // Setting 
  const [isLoading, setLoading] = React.useState(false);

  return (
    <LoginComponent props={props} login_button_pressed={login_button_pressed} isLoading={isLoading}
      sign_up={sign_up} ForgetPass={ForgetPass} add_value={add_value}/>

  );
};
LoginContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession,widgets }) => ({
  dashboard,
  userObj,
  widgets,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
  userlogin: (params) => dispatch(Actions.userlogin(params)),
  alert_action: () => dispatch(Actions.alert_action()),
  setUserSession: () => dispatch(Actions.setUserSession()),
  clearUserObj: () => dispatch(Actions.clearUserObj()),
  clearUserSession: () => dispatch(Actions.clearUserSession()),
});


export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
