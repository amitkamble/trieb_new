import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, Modal, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView } from 'react-native';
import styles from './styles';
import { Images } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const LoginComponent = memo((props) => {

  // Sign IN Button Pressed
  const onSignupclick = () => {
    props.sign_up();

  }
  // Sign IN Button Pressed
  const onForgetPassclick = () => {
    props.ForgetPass();

  }
  const login_button_pressed = () => {

    if (user.email.trim().length && user.password.trim().length) {
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
      if (reg.test(user.email) === false) {
        // alert("Please enter valid email");
        setFocus_user_error({ ...on_user_error, ['email']: true, ['password']: false })

      } else {
        props.login_button_pressed(user);
        setFocus_user_error({ ...on_user_error, ['email']: false, ['password']: false })
      }
    } else if (!user.email.trim().length && !user.password.trim().length) {
      setFocus_user_error({ ...on_user_error, ['email']: true, ['password']: true })
    } else if (!user.email.trim().length && user.password.trim().length) {
      setFocus_user_error({ ...on_user_error, ['email']: true, ['password']: false })
    } else if (user.email.trim().length && !user.password.trim().length) {
      setFocus_user_error({ ...on_user_error, ['email']: false, ['password']: true })
    }

  };

  // Storing Name & Pass
  const [user, set_user] = useState({
    email: 'vb@gmail.com',
    password: '123456'
  });

  //to set Focus for Username Field
  const [on_user, setFocus_user] = useState(false);

  //to set Focus for Password Field
  const [on_pass, setFocus_pass] = useState(false);

  //For Show & Hide Password
  const [on_secure, setSecurity] = useState(true);

  //to hide & show error for input_text
  const [on_user_error, setFocus_user_error] = useState({
    email: false,
    password: false
  });

  //to check and set focus error field or email
  const here_user = (value) => {
    set_user({ ...user, ['email']: value })
    if (value == '') {
      setFocus_user_error({ ...on_user_error, ['email']: true })

    } else {
      setFocus_user_error({ ...on_user_error, ['email']: false })

    }
    if (on_user) {
      setFocus_user_error({ ...on_user_error, ['email']: false })

    }
    if (on_pass) {
      setFocus_user_error({ ...on_user_error, ['password']: false })
    }

  };

  //to check and set focus error field or password  
  const here_pass = (value) => {
    set_user({ ...user, ['password']: value })
    if (value == '') {
      setFocus_user_error({ ...on_user_error, ['password']: true })

    } else {
      setFocus_user_error({ ...on_user_error, ['password']: false })

    }
    if (on_pass) {
      setFocus_user_error({ ...on_user_error, ['password']: false })

    }
    if (on_user) {
      setFocus_user_error({ ...on_user_error, ['email']: false })

    }
  };

  return (
    <ScrollView >
      <StatusBar translucent backgroundColor={colors.main_dark_color} />
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

        <View safe style={styles.container}>

          <StatusBar barStyle="light-content" translucent={true} />

          <View style={{
            height: 210,
            width: width,
          }}>
            <View style={styles.back_image} />

            <Image source={Images.circle_login} style={styles.logo_new} />

            <Image source={Images.logo_app} style={styles.logo_trieb} />


          </View>

          <View style={styles.sub_container_field}>
            <Text style={styles.logintitle}>Welcome</Text>

            <Text style={styles.logintext}>Please login to your account.</Text>

            {
              <Animated.Text style={styles.HedingText}>{!on_user ? user.email != '' ? "Email" : ''
                : 'Email'}</Animated.Text>

            }

            <View style={styles.sub_container}>


              <TextInput
                placeholder={
                  !on_user ? user.email == '' ? "Email" : ''
                    : ''
                }
                onChangeText={(value) => here_user(value)}
                onFocus={() => setFocus_user(true)}
                value={user.email}
                onBlur={() => setFocus_user(false)}
                placeholderTextColor={colors.gray}
                autoCorrect={false}
                style={styles.inputText} />

              <Image source={Images.mail} style={styles.logo} />

            </View>
            {on_user_error.email ?
              <Text style={styles.errorText}>{'Please Valid Email'}</Text>
              : <Text style={styles.errorText}>{''}</Text>

            }
            <Text style={styles.HedingText_pass}>{!on_pass ? user.password != '' ? "Password" : ''
              : 'Password'}</Text>

            <View style={styles.sub_container}>

              <TextInput
                placeholder={
                  !on_pass ? user.password == '' ? "Password" : ''
                    : ''
                }
                onFocus={() => setFocus_pass(true)}
                onBlur={() => setFocus_pass(false)}
                onChangeText={(value) => here_pass(value)}
                value={user.password}
                placeholderTextColor={colors.gray}
                secureTextEntry={on_secure}
                autoCorrect={false}
                style={styles.inputText} />

              <TouchableOpacity style={{
                right: 0,
                height: 20,
                width: 20,
                position: 'absolute',
                marginRight: 23,
                alignSelf: 'center'
              }} onPress={() => setSecurity(on_secure ? false : true)} >

                <Image source={user.password != '' ? on_secure ? Images.pass_not_visible : Images.pass_visible : Images.lock} style={user.password != '' ? styles.logo_pass : styles.logo_lock} />
              </TouchableOpacity>
            </View>
            {on_user_error.password ?
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.errorText_pass}>{'Please Enter Password'}</Text>

                <Text style={styles.forget_pass_Text}>{'Forget Password ?'}</Text>

              </View>
              : <View style={{ flexDirection: 'row' }}>
                <Text style={styles.errorText_pass}>{''}</Text>
                <TouchableOpacity style={{
                  marginTop: 15,
                  marginLeft: 25,
                  flex: 0.9,
                }} onPress={() => onForgetPassclick()}>

                  <Text style={styles.forget_pass_Text}>{'Forget Password ?'}</Text>
                </TouchableOpacity>
              </View>

            }
            <TouchableOpacity onPress={() => login_button_pressed()} style={styles.login}>
              <Text style={styles.logintext_button}>LOGIN</Text>
            </TouchableOpacity>

          </View>

          <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 10 }}>
            <Text style={styles.signUp_text_first}>{'Don’t have an account? '}</Text>
            <TouchableOpacity onPress={() => onSignupclick()}>
              <Text style={styles.signUp_text_click}>{'Register now'}</Text>

            </TouchableOpacity>

          </View>
          <Image source={Images.login_bottom_image} style={styles.bottom_image} />

        </View>


      </TouchableWithoutFeedback>


      <Spinner
        visible={props.isLoading}
        textContent={'Loading...'}
        color={'white'}
        overlayColor={'rgba(0, 0, 0, 0.45)'}
        textStyle={styles.spinnerTextStyle}
      />

    </ScrollView>

  );
});



export default LoginComponent;
