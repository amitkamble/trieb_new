

import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import SplashComponent from './SplashComponent';
import SplashScreen from 'react-native-splash-screen';
import { store } from '../../redux/actions/store'

var socket;
const SplashContainer = (props) => {

  useEffect(() => {

    SplashScreen.hide()
    const { navigation } = props;
    if (store.getState().userSession.isUserLoggedin) {

      navigation.navigate('HomePage')

    } else {
      navigation.navigate('Demo')

    }
  }, []);

  return (
    <SplashComponent props={props} />
  );

};

const mapStateToProps = ({ dashboard, userObj, userSession, widgets }) => ({
  dashboard,
  userObj,
  widgets,
  isUserLoggedin: userSession.isUserLoggedin,
});


const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(SplashContainer);

