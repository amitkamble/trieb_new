import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import PolylineDirection from '@react-native-maps/polyline-direction';
import MapView, { Polyline, PROVIDER_GOOGLE } from 'react-native-maps';
import * as geolib from 'geolib';
import { ProgressBar, Colors } from 'react-native-paper';
import { TabView, SceneMap } from 'react-native-tab-view';
import languages from '../../globals/languages';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const TipsComponent = memo((props) => {

  const [state, setState] = React.useState({
    index: 0,
    routes: [
      { key: 'first', title: languages.lang.food },
      { key: 'second', title: languages.lang.wifi },
      { key: 'third', title: languages.lang.outlets },
      { key: 'four', title: languages.lang.transportaion },
      { key: 'five', title: languages.lang.shops },
      { key: 'six', title: languages.lang.services }
    ]
  });

  const sports = [
    {
      label: languages.lang.food,
      value: languages.lang.food,
    },
    {
      label: languages.lang.wifi,
      value: languages.lang.wifi,
    },
    {
      label: languages.lang.outlets,
      value: languages.lang.outlets,
    },
    {
      label: languages.lang.transportaion,
      value: languages.lang.transportaion,
    },
    {
      label: languages.lang.shops,
      value: languages.lang.shops,
    },
    {
      label: languages.lang.services,
      value: languages.lang.services,
    },
  ];

  const [airline, set_airline] = useState({
    name: '',
  });

  const onCancel_press = () => {
    setVisiable_Pop(false)
  }
  const [data] = useState(true);
  const [data_upcoming] = useState(true);
  const [past_data] = useState(true);


  // Storing Name & Pass
  const [user, set_user] = useState({
    username: '',
    email: '',
    pass: '',
    confirm_pass: '',
    role: '',
  });

  const view_all = () => {
    props.detailsPage();
  }

  const add_flight = () => {
    props.add_flight();
  }
  //to check and set focus error field or password  
  const here_select_role = (value) => {
    set_user({ ...user, ['role']: value })

  };
  const [isVisible, setVisiable_Pop] = useState(false);

  const FirstRoute = () => (

    data_upcoming == true ?
      <View>
        <View style={{ flexDirection: 'row', marginRight: 20 }}>
          <View style={{ flex: 1 }}>
            <Text style={styles.card_date}>{'01  ' + Language.lang.May}</Text>

          </View>

          <Text style={styles.Flight_time}>{'5h 30m Left'}</Text>

        </View>
        <View style={styles.view_flight_card}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.from_time}>{Language.lang.from}</Text>
            <Text style={styles.to_time}>{Language.lang.to}</Text>
          </View>

          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.From_flight}>{'FLK'}</Text>
            <Image source={Images.flight_upcoming} style={styles.logo_icon_flight} />
            <Text style={styles.To_flight}>{'LAX'}</Text>
          </View>

          <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20, marginRight: 20 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
              <Image source={Images.coutry1} style={styles.logo_icon_country} />
              <Text style={styles.From_country_name}>{'New York'}</Text>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', flex: 1 }}>
              <Image source={Images.coutry1} style={styles.logo_icon_country} />
              <Text style={styles.From_country_name}>{'Los angels'}</Text>
            </View>

          </View>

          <View style={{ flexDirection: 'row', marginTop: 15, marginBottom: 10, borderWidth: 1, padding: 10, borderStyle: 'dashed', borderRadius: 5, borderColor: colors.main_color_dark, marginLeft: 20, marginRight: 20 }}>
            <View style={{ flexDirection: 'column', flex: 1 }}>
              <Text style={styles.From_country_name}>{Language.lang.flight}</Text>
              <Text style={{ color: colors.main_dark_color, marginLeft: 5, marginTop: 5, fontFamily: fonts.Monsterat_SemiBold }}>{'AA21'}</Text>
            </View>
            <View style={{ width: 1, backgroundColor: colors.main_color_dark, height: 15, position: 'absolute', marginTop: Platform.OS == 'ios' ? 30 : 35, marginLeft: 80 }} />
            <View style={{ flexDirection: 'column', flex: 1 }}>
              <Text style={styles.From_country_name}>{Language.lang.terminal}</Text>
              <Text style={{ color: colors.main_dark_color, marginLeft: 5, marginTop: 5, fontFamily: fonts.Monsterat_SemiBold }}>{'12A'}</Text>
            </View>
            <View style={{ width: 1, backgroundColor: colors.main_color_dark, height: 15, position: 'absolute', marginTop: Platform.OS == 'ios' ? 30 : 35, marginLeft: 180 }} />

            <View style={{ flexDirection: 'column', flex: 1 }}>
              <Text style={styles.From_country_name}>{Language.lang.boarding}</Text>
              <Text style={{ color: colors.main_dark_color, marginLeft: 5, marginTop: 5, fontFamily: fonts.Monsterat_SemiBold }}>{'16:40PM'}</Text>
            </View>
          </View>

          <View style={{ height: 1, backgroundColor: colors.view_color, marginTop: 10 }} />
          <TouchableOpacity onPress={() => view_all()} style={styles.View_all_details}>
            <Text style={styles.logintext_button}>View all details</Text>
          </TouchableOpacity>
        </View>

      </View>
      :
      <View>

        <Text style={styles.headingtitle}>{Language.lang.plan_trip}</Text>

        <View style={styles.view_flight_card}>
          <View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
              <Text style={styles.check_in}>{Language.lang.add_manul}</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
              <Text style={styles.on_time}>{Language.lang.add_desc}</Text>
            </View>
            <TouchableOpacity onPress={() => add_flight()} style={styles.View_add}>
              <Text style={styles.logintext_button}>{Language.lang.add_flight}</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.view_flight_card}>
          <View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
              <Text style={styles.check_in}>{Language.lang.share_invite}</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
              <Text style={styles.on_time}>{Language.lang.share_desc}</Text>
            </View>
            <TouchableOpacity onPress={() => view_all()} style={styles.View_add}>
              <Text style={styles.logintext_button}>{Language.lang.share}</Text>
            </TouchableOpacity>
          </View>
        </View>

      </View>
  );

  const SecondRoute = () => (
    <View style={{ marginTop: 20 }}>

      <View style={styles.view_flight_card}>

        <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20, marginRight: 20 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
            <Image source={Images.profile_image} style={styles.logo_icon_country} />
            <Text style={styles.From_country_name}>{'New York'}</Text>
          </View>

          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', flex: 1 }}>
            <Text style={styles.From_country_name}>{'10 Mar 2021'}</Text>
          </View>

        </View>
        <Text style={{ color: colors.main_color_dark, fontFamily: fonts.SemiBold, marginTop: 10, marginLeft: 20 }}>Its very clean you really love it</Text>

        <Image source={Images.image_flight} style={{ height: 150, width: width - 40, marginTop: 15, resizeMode: 'contain' }} />


        <View style={{ flexDirection: 'row', marginTop: 25, marginBottom: 15 }}>
          <TouchableOpacity onPress={() => console.log('here')} style={styles.social_media}>

            <Image source={Images.like} style={styles.logo_like} />

          </TouchableOpacity>
          <Text style={styles.From_like_text}>{'100000'}</Text>
          <View style={{ height: 20, width: 1, marginLeft: 15, marginRight: 5, backgroundColor: colors.main_dark_color }} />
          <TouchableOpacity onPress={() => console.log('here')} style={styles.social_media}>

            <Image source={Images.dislike} style={styles.logo_like} />
          </TouchableOpacity>
          <Text style={styles.From_like_text}>{'100000'}</Text>

          <TouchableOpacity onPress={() => console.log('here')} style={styles.logo_share}>

            <Image source={Images.share_blue} style={styles.logo_share} />
          </TouchableOpacity>
        </View>

      </View>

    </View>
  );

  const _handleIndexChange = index => setState({ ...state, ['index']: index });

  const _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View horizontal style={styles.tabBar}>
        <ScrollView horizontal>

          {props.navigationState.routes.map((route, i) => {

            return (

              <TouchableOpacity
                onPress={() => setState({ ...state, ['index']: i })}
                style={{ alignItems: "center", padding: 5 }}
              >

                <Text style={{ color: '#000', fontFamily: fonts.SemiBold }}>
                  {route.title}
                </Text>
                <View style={{ height: 3, width: 60, marginTop: Platform.OS == 'ios' ? 10 : 10, backgroundColor: state.index == i ? colors.main_color_dark : null }} />


              </TouchableOpacity>
            );
          })}
        </ScrollView>

      </View>
    );
  };

  //to check and set focus error field or password  
  const onBack_click = () => {
    props.onBack_click();
  };



  const _renderScene = () => SceneMap({
    first: () => SecondRoute(),
    second: () => SecondRoute(),
    third: () => SecondRoute(),
    four: () => SecondRoute(),
    five: () => SecondRoute(),
    six: () => SecondRoute(),
  });


  return (
    <ScrollView contentContainerStyle={styles.container}>

      <View style={{
        height: 100,
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
      }}>
        <View style={styles.back_image} />


        <TouchableOpacity onPress={() => onBack_click()} style={{
          marginLeft: 15,
          marginTop: 20
        }}>
          <Image source={Images.back} style={styles.logo_back} />
        </TouchableOpacity>
        <Text style={styles.tab_bar_text}>{Language.lang.tips}</Text>
        <TouchableOpacity onPress={() => setVisiable_Pop(!isVisible)} style={{ flex: 1, flexDirection: 'row', position: 'absolute', right: 0 }}>
          <Text style={styles.card_add_new}>{Language.lang.add_tips}</Text>
          <Text style={{ fontSize: 25, position: 'absolute', fontFamily: fonts.Monsterat_Regular, right: 0, color: 'white', marginTop: 30, marginRight: 27 }}>{'+'}</Text>

        </TouchableOpacity>
      </View>
      {
        isVisible ? <View style={{ height: height, opacity: 0.8, width: width, backgroundColor: colors.background_opacity, position: 'absolute' }} />
          : null
      }
      {
        !isVisible ?
          past_data == false ?

            null
            :
            <TabView
              navigationState={state}
              renderScene={_renderScene()}
              renderTabBar={_renderTabBar}
              onIndexChange={_handleIndexChange}
            />
          :

          <View style={styles.view_import_program}>

            <TouchableOpacity style={{ margin: 20 }}>
              <View style={{ flexDirection: 'row', flex: 1, marginTop: 10 }}>
                <Image source={Images.loyalty_prog} style={styles.logo_icon_pop_up} />
                <Text style={styles.add_program}>{languages.lang.add_program}</Text>
              </View>
            </TouchableOpacity>

            <TextInput
              placeholder={languages.lang.new_tip}
              onChangeText={(value) => set_airline({ ['airline']: value })}
              value={airline.name}
              placeholderTextColor={colors.gray}
              autoCorrect={false}
              style={styles.inputText} />

            <View style={styles.container_viewtop}>

              <RNPickerSelect
                placeholder={{ label: languages.lang.select_category, value: '', color: colors.dark_gray }}
                onValueChange={(value) => here_select_role(value)}
                items={sports}
                style={pickerSelectStyles}
                Icon={() => {
                  return <Image source={Images.dropdown_arrow} style={styles.drop_down} />
                }}
              />
              <View style={{ width: width - 40, height: 1, backgroundColor: 'gray' }} />

            </View>


            <View style={{ flexDirection: 'row', marginTop: 35, marginBottom: 40, borderWidth: 1, padding: 10, borderStyle: 'dashed', borderRadius: 5, borderColor: colors.main_color_dark, marginLeft: 20, marginRight: 20 }}>
              <View style={{ flexDirection: 'column', flex: 1 }}>
                <Image source={Images.attach_image} style={styles.logo_attach_pop_up} />
                <Text style={styles.From_country_name}>{Language.lang.select_image}</Text>
              </View>

            </View>
            <View style={{ flexDirection: 'row', flex: 1 }}>
              <TouchableOpacity onPress={() => onCancel_press()} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.card_add_new_button}>{Language.lang.cancel}</Text>

              </TouchableOpacity>
              <TouchableOpacity onPress={() => onCancel_press()} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.View_add_pop}>{Language.lang.send}</Text>

              </TouchableOpacity>
            </View>



          </View>

      }
    </ScrollView>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 0,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'white',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: width,
    padding: 20,
    marginLeft: 30,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default TipsComponent;
