import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView } from 'react-native';
import styles from './styles';
import { Images } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import languages from '../../globals/languages';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const Import_flightComponent = memo((props) => {


  // Storing Name & Pass
  const [user, set_user] = useState({
    pass: '',
    confirm_pass: '',
  });

  const login_button_pressed = () => {
    // props.login_button_pressed();
  }
  const on_back_press = () => {
    props.handleBackButtonClick();
  }

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor={colors.main_dark_color} />

      {/* <Image source={Images.circle_login} style={styles.logo_new} /> */}

      <Image source={Images.flight_add} style={styles.logo_map} />

      <LinearGradient colors={['#66000000', '#66000000', '#ffffff']} style={styles.linearGradient}>
      </LinearGradient>

      <LinearGradient colors={['#66000000', '#66000000', '#ffffff']} style={styles.linearGradient}>
      </LinearGradient>

      <View style={{ position: 'absolute', bottom: 0, width: width }}>
        <TouchableOpacity onPress={() => login_button_pressed()} style={styles.text_login}>
          <Image source={Images.import_icon} style={styles.import_icon} />
          <Text style={styles.import_button}>{languages.lang.import_flight_from}</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => login_button_pressed()} style={styles.google_login}>
          <Image source={Images.google} style={styles.google_icon} />
          <Image source={Images.text_google} style={styles.google_text} />
        </TouchableOpacity>

        <TouchableOpacity onPress={() => login_button_pressed()} style={styles.fb_login}>
          <Image source={Images.facebook_icon} style={styles.fb_icon} />

          <Text style={styles.logintext_button}>{languages.lang.facebook}</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => login_button_pressed()} style={styles.login}>
          <Image source={Images.gmail_icon} style={styles.fb_icon} />

          <Text style={styles.logintext_button}>{languages.lang.gmail}</Text>
        </TouchableOpacity>
      </View>


      <TouchableOpacity onPress={() => on_back_press()} style={styles.close_pop_up}>
          <Image source={Images.close_black} style={styles.close_icon} />
        </TouchableOpacity>
    </View>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

export default Import_flightComponent;
