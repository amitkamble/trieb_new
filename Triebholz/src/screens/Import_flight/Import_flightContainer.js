import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Import_flightComponent from './Import_flightComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const Import_flightContainer = (props) => {

  const { navigation,  clearUserObj, route } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };
  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }

  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
      navigation.navigate('LoginContainer')
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);



  return (
    <Import_flightComponent props={props} login_button_pressed={login_button_pressed} handleBackButtonClick={handleBackButtonClick}/>

  );
};
Import_flightContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
});


export default connect(mapStateToProps, mapDispatchToProps)(Import_flightContainer);
