import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView } from 'react-native';
import styles from './styles';
import { Images } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import OTPInputView from '@twotalltotems/react-native-otp-input'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const EnterOTPComponent = memo((props) => {

  // Sign IN Button Pressed
  const onSignupclick = () => {
    props.sign_up();

  }
  const Confirm_otp = () => {

    if (otp.trim().length) {
      if (otp.trim().length == 4) {
        setFocus_user_error(false)
        props.Confirm_otp(otp);
      } else {
        setFocus_user_error(true)

      }

    } else {
      setFocus_user_error(true)
    }
  };

  const resend_otp = () => {

    props.resend_otp();

  };

  const onBack_click = () => {

    props.onBack_click();

  };

  const setOtp = (code) => {

    set_otp(code)
    setFocus_user_error(false)

  };

  // Storing Name & Pass
  const [otp, set_otp] = useState('');

  //to hide & show error for input_text
  const [on_user_error, setFocus_user_error] = useState(false);

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
      <StatusBar translucent backgroundColor={colors.main_dark_color} />
      <View style={{
        height: 100,
        width: width,
      }}>
        <View style={styles.back_image} />

        <Image source={Images.circle_login} style={styles.logo_new} />
        <TouchableOpacity onPress={() => onBack_click()} style={{
          position: 'absolute',
          marginTop: 50,
          marginLeft: 15,
        }}>
          <Image source={Images.back} style={styles.logo_back} />
        </TouchableOpacity>
      </View>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

        <View safe style={styles.container}>

          <StatusBar barStyle="light-content" translucent={true} />

          <Image source={Images.mobile_otp} style={styles.logo_mobile} />


          <Text style={styles.logintitle}>A code has sent to your mobile number</Text>

          <Text style={styles.logintext}>{'Enter OTP below'}</Text>
          <Text style={styles.mobile_notext}>{props.mobile_no}</Text>

          <View style={styles.sub_container_field}>

            <OTPInputView
              style={{ width: '80%', height: 30 }}
              pinCount={4}
              // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
              // onCodeChanged = {code => { this.setState({code})}}
              autoFocusOnLoad
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              onCodeChanged={(code => {
                setOtp(code)
              })}
            />



            {on_user_error ?
              otp == '' ?
                <Text style={styles.errorText}>{'Please Enter OTP'}</Text>
                : <Text style={styles.errorText}>{'Please Enter Valid OTP'}</Text>
              : null
            }

            <TouchableOpacity onPress={() => Confirm_otp()} style={styles.login}>
              <Text style={styles.logintext_button}>SUBMIT</Text>
            </TouchableOpacity>

          </View>
          <Text style={styles.logintext}>{'Didn' + "'" + 't receive a OTP?'}</Text>
          <TouchableOpacity onPress={() => resend_otp()} style={styles.resend_otp}>
            <Text style={styles.resendtext_button}>RESEND CODE</Text>
          </TouchableOpacity>

        </View>


      </TouchableWithoutFeedback>


      <Spinner
        visible={props.isLoading}
        textContent={'Loading...'}
        color={'white'}
        overlayColor={'rgba(0, 0, 0, 0.45)'}
        textStyle={styles.spinnerTextStyle}
      />

    </ScrollView>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

export default EnterOTPComponent;
