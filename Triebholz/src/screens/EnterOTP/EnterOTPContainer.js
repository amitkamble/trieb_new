import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import EnterOTPComponent from './EnterOTPComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const EnterOTPContainer = (props) => {

  const { navigation,  clearUserObj, route } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };
  const { isComeFrom } = route;
  const { mobile_no } = route.params;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }

  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const Confirm_otp = () => {
    navigation.navigate('ResetPass')
  };

  const resend_otp = () => {
    alert('here')
  };

  const onBack_click = () => {
    navigation.goBack();
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);



  return (
    <EnterOTPComponent props={props} Confirm_otp={Confirm_otp} isLoading={isLoading} 
    resend_otp={resend_otp} mobile_no={mobile_no} onBack_click={onBack_click}/>

  );
};
EnterOTPContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
});


export default connect(mapStateToProps, mapDispatchToProps)(EnterOTPContainer);
