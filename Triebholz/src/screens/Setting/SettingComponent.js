import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, Switch, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import { colors, fonts, sizes } from '../../utils/theme';
import * as geolib from 'geolib';
import languages from '../../globals/languages';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import ToggleSwitch from 'toggle-switch-react-native'
import EditProfileComponent from '../EditProfile/EditProfileComponent';


const SettingComponent = memo((props) => {


  const EditProfile = () => {
    props.EditProfile();
  }

  const setUp_widgets = () => {
    props.setUp_widgets();
  }

  const manage_link_ac = () => {
    props.manage_link_ac();
  }

  const onBack = () => {
    props.onBack();
  }

  const center = geolib.getCenter([
    { latitude: 28.2014833, longitude: -177.3813083 },
    { latitude: 39.849312, longitude: -104.673828 },
  ]);

  return (
    <ScrollView contentContainerStyle={styles.container}>

      <View style={styles.logo_map} >

        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 30, }}>

          <Text style={styles.logintitle}>{Language.lang.setting}</Text>
          <TouchableOpacity onPress={() => onBack()} style={{
            position: 'absolute',
            right: 0,
            marginTop: 15,
            marginRight: 20
          }}>
            <Image source={Images.close} style={styles.logo_lock} />

          </TouchableOpacity>

        </View>
      </View>

      <Text style={styles.From_import_name}>{languages.lang.accounts}</Text>

      <View style={styles.view_flight_card}>

        <View style={{ flexDirection: 'column', marginTop: 15, marginLeft: 20, marginRight: 20 }}>
          <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
            <Image source={Images.google} style={styles.logo_icon_country} />
            <Text style={styles.name_sub}>{languages.lang.google}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=> manage_link_ac()} style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 15 }}>
            <Image source={Images.import_icon} style={styles.logo_icon_country} />
            <Text style={styles.name_sub}>{languages.lang.import_flight}</Text>
          </TouchableOpacity>

        </View>

      </View>

      <Text style={styles.From_import_name}>{languages.lang.general}</Text>

      <View style={styles.view_flight_card}>

        <View style={{ flexDirection: 'column', marginTop: 30, marginRight: 20 }}>
          <TouchableOpacity onPress={()=> setUp_widgets()} style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
            <Text style={styles.name_sub}>{languages.lang.set_up_widgets}</Text>
            <Image source={Images.right_arrow} style={styles.logo_right_arrow} />
          </TouchableOpacity>

          <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 30 }}>
            <Text style={styles.name_sub}>{languages.lang.set_up_notification}</Text>
            <Image source={Images.right_arrow} style={styles.logo_right_arrow} />
          </TouchableOpacity>

          <TouchableOpacity onPress={()=> EditProfile()} style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 30 }}>
            <Text style={styles.name_sub}>{languages.lang.edit_profile}</Text>
          </TouchableOpacity>
          <View style={{ flexDirection: 'row', flex: 1, backgroundColor: 'white', marginTop: 30 }}>
            <Text style={styles.name_sub}>{languages.lang.metric_system}</Text>
            <View style={{ alignSelf: 'center', position: 'absolute', right: 0 }}>
              <ToggleSwitch
                isOn={props.isalert}
                onColor={colors.green_text_coot}
                offColor="gray"
                labelStyle={{ color: "black", fontWeight: "900" }}
                size="small"
                onToggle={() => console.log('here')}
              />
            </View>
          </View>
          <View style={{ flexDirection: 'row', flex: 1, backgroundColor: 'white', marginTop: 30 }}>
            <Text style={styles.name_sub}>{languages.lang.subscribe_to_news}</Text>
            <View style={{ alignSelf: 'center', position: 'absolute', right: 0 }}>
              <ToggleSwitch
                isOn={props.isSubscribe}
                onColor={colors.green_text_coot}
                offColor="gray"
                labelStyle={{ color: "black", fontWeight: "900" }}
                size="small"
                onToggle={()=>console.log('here')}
              />
            </View>
          </View>

          <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 30 }}>
            <Text style={styles.name_sub}>{languages.lang.language}</Text>
            <Image source={Images.right_arrow} style={styles.logo_right_arrow} />
          </TouchableOpacity>

          <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 30, marginBottom: 20 }}>
            <Text style={styles.name_sub}>{languages.lang.forward_itineraries}</Text>
          </TouchableOpacity>
        </View>

      </View>

      <Text style={styles.From_import_name}>{languages.lang.theme}</Text>


      <View style={styles.view_flight_card}>

        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', justifyContent: 'center', flex: 1, marginTop: 20 }}>
          <Text style={styles.name_sub_row}>{languages.lang.match_system}</Text>
          <Text style={styles.name_sub_row_unselected}>{languages.lang.night_mode}</Text>
          <Text style={styles.name_sub_row_unselected}>{languages.lang.light_mode}</Text>
        </View>

      </View>

      <Text style={styles.From_import_name}>{languages.lang.calender}</Text>

      <View style={styles.view_flight_card}>

        <View style={{ flexDirection: 'row', flex: 1, backgroundColor: 'white', marginTop: 30 }}>
          <Text style={styles.name_sub}>{languages.lang.import_flight}</Text>
          <View style={{ alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
            <ToggleSwitch
              isOn={props.isalert}
              onColor={colors.green_text_coot}
              offColor="gray"
              labelStyle={{ color: "black", fontWeight: "900" }}
              size="small"
              onToggle={() => console.log('here')}
              />
          </View>
        </View>
        <View style={{ flexDirection: 'row', flex: 1, backgroundColor: 'white', marginTop: 30 }}>
          <Text style={styles.name_sub}>{languages.lang.export_flight}</Text>
          <View style={{ alignSelf: 'center', position: 'absolute', right: 0, marginRight: 20 }}>
            <ToggleSwitch
              isOn={props.isalert}
              onColor={colors.green_text_coot}
              offColor="gray"
              labelStyle={{ color: "black", fontWeight: "900" }}
              size="small"
              onToggle={() => console.log('here')}
              />
          </View>
        </View>
      </View>

      <Text style={styles.From_import_name}>{languages.lang.other}</Text>

      <View style={styles.view_flight_card}>

        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 30 }}>
          <Text style={styles.name_sub}>{languages.lang.sync_now}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 30 }}>
          <Text style={styles.name_sub}>{languages.lang.clear_cache}</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => props.alertAction()} style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 30 }}>
          <Text style={styles.name_sub}>{languages.lang.manage_subscription}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 30 }}>
          <Text style={styles.name_sub}>{languages.lang.earn_activation}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 30, marginBottom: 15 }}>
          <Text style={styles.name_sub}>{languages.lang.export_data}</Text>
        </TouchableOpacity>

      </View>

      <Text style={styles.From_import_name}>{languages.lang.about}</Text>

      <View style={styles.view_flight_card}>

        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 25, marginLeft: 20 }}>
          <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start'}}>
            <Image source={Images.facebook} style={styles.logo_icon_facebook} />
          </TouchableOpacity>

          <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: 10 }}>
            <Image source={Images.twitter_login} style={styles.logo_icon_facebook} />
          </TouchableOpacity>
        </View>

        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 30 }}>
          <Text style={styles.name_sub}>{languages.lang.privacy_policy}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 30 }}>
          <Text style={styles.name_sub}>{languages.lang.terms_of_use}</Text>
        </TouchableOpacity>

      </View>
      <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 20, marginRight: 20 }}>

        <TouchableOpacity style={styles.View_add}>
          <Text style={styles.logintext_button}>{Language.lang.rate_us}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.View_add_invite}>
          <Text style={styles.logintext_button_dark}>{Language.lang.delete_account}</Text>
        </TouchableOpacity>
      </View>

    </ScrollView>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default SettingComponent;
