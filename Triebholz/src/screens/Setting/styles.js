import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors, fonts, sizes } from '../../utils/theme';

import Icon from 'react-native-vector-icons/Ionicons';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default StyleSheet.create({
  container: {
    flexGrow: 1,
  },

  sub_container: {
    flexDirection: 'row',
    margin: 20,
    backgroundColor: colors.white,
    padding:10
  },
  logo_map: {
    height: 90,
    width: width,
    backgroundColor: colors.main_dark_color,
  },

  logo_lock: {
    height: 20,
    width: 20,
  },

  logo_icon: {
    height: 35,
    width: 35,
    marginBottom: 20
  },

  logo_icon_message: {
    height: 32,
    width: 38.5,
    marginBottom: 20
  },

  logo_icon_flight: {
    height: 42,
    flex: 0.5,
    resizeMode: 'contain',
    marginTop: -10
  },

  // logo_icon_country: {
  //   width: 27,
  //   height: 18,
  //   alignSelf: 'center',
  //   marginTop: 3
  // },

  logo_icon_account: {
    width: 22,
    height: 22,
    alignSelf: 'center',
    marginTop: 3
  },

  logo_icon_share: {
    width: 24,
    height: 20,
    alignSelf: 'center',
    marginTop: 3
  },

  logo_icon_next: {
    width: 15,
    height: 15,
    marginTop: 123,
    marginRight: 120
  },

  logo_icon_pre: {
    width: 15,
    height: 15,
    marginTop: 123,
    marginLeft: 120
  },

  logo_new: {
    height: 95,
    width: 93,
    position: 'absolute',
    marginTop: -35,
    marginRight: 50,
    right: 0,
    top: 0
  },

  logintitle: {
    color: colors.white,
    fontSize: 18,
    fontFamily: fonts.SemiBold,
    alignSelf: 'center',
    marginLeft: 20
  },

  From_flight: {
    color: colors.main_dark_color,
    fontSize: 24,
    fontFamily: fonts.SemiBold,
    marginTop: 5,
    marginLeft: 20,
    flex: 0.25,
  },

  From_country_name: {
    color: colors.main_color_dark,
    fontSize: 16,
    fontFamily: fonts.SemiBold,
  },

  From_import_name: {
    color: colors.main_color_dark,
    fontSize: 16,
    fontFamily: fonts.SemiBold,
    margin:20
  },

  journey_hours: {
    color: colors.main_dark_color,
    fontSize: 18,
    fontFamily: fonts.Monsterat_Regular,
    marginLeft: 5,
    textAlignVertical: 'top'
  },

  To_flight: {
    color: colors.main_dark_color,
    fontSize: 24,
    fontFamily: fonts.SemiBold,
    marginTop: 5,
    paddingLeft: 10,
    flex: 0.25,
  },

  headingtitle: {
    color: colors.white,
    fontSize: 16,
    fontFamily: fonts.SemiBold,
    marginTop: 30,
    marginLeft: 20
  },

  card_date: {
    color: colors.white,
    fontSize: 13,
    backgroundColor: colors.main_color_dark,
    fontFamily: fonts.Monsterat_SemiBold,
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    textAlignVertical: 'center',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    alignSelf: 'center'
  },

  card_fight_name: {
    color: colors.main_dark_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical: 'center',
    marginTop: 20,
    alignSelf: 'center',
    paddingLeft: 20,
    paddingRight: 20
  },

  from_time: {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical: 'center',
    marginTop: 20,
    paddingLeft: 20,
    flex: 1
  },

  to_time: {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical: 'center',
    marginTop: 20,
    paddingRight: 25,
    flex: 1,
    textAlign: 'right'
  },

  check_in: {
    color: colors.black,
    fontSize: 16,
    fontFamily: fonts.Monsterat_Medium,
    textAlignVertical: 'center',
    paddingLeft: 20,
    flex: 1
  },

  check_in_time: {
    color: colors.hint_color,
    fontSize: 14,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical: 'center',
    paddingRight: 25,
    flex: 1,
    textAlign: 'right'
  },
  on_time: {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft: 20,
    textAlignVertical: 'center',
    flex: 1,
  },
  terminal: {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft: 10,
    textAlignVertical: 'center',
  },
  terminal_no: {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft: 5,
    textAlignVertical: 'center',
  },
  card_title: {
    color: colors.main_dark_color,
    fontSize: sizes.regular,
    fontFamily: fonts.SemiBold,
  },

  view_home_card: {
    flex: 1,
    margin: 10,
    height: 140,
    backgroundColor: 'white',
    borderRadius: 5,
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 8,
    alignItems: 'center',
    justifyContent: 'center'
  },

  view_flight_card: {
    backgroundColor: 'white',
    borderRadius: 40,
    marginTop: 30,
    flex: 1, paddingTop:20
  },

  inputText: {
    fontSize: sizes.regularLarge,
    height: Platform.OS == 'ios' ? 50 : 40,
    fontFamily: fonts.Regular,
    marginLeft: 20,
    marginRight: 20,
    borderBottomColor: colors.main_color_dull,
    color: colors.black,
    borderBottomWidth: 1,
    flex: 1, 
    marginBottom:10
  },

  logo: {
    height: 15,
    width: 15,
    position: 'absolute',
    right: 0,
    bottom: 0,
    marginRight: 30,
    marginBottom: Platform.OS == 'ios' ? 35 : 30,
    alignSelf: 'center'
  },
  View_all: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 50,
    paddingLeft: 50,
    paddingRight: 50,
    alignSelf: 'center',
    borderRadius: 5,
    marginTop: 30,
    marginBottom: 20,
    backgroundColor: colors.main_dark_color,
  },

  View_add: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 50,
    width: 115,
    alignSelf: 'flex-start',
    borderRadius: 5,
    marginTop: 15,
    marginLeft: 20,
    marginBottom: 20,
    backgroundColor: colors.main_dark_color,
  },

  logintext_button: {
    color: colors.white,
    fontSize: sizes.regular,
    fontFamily: fonts.SemiBold,
    alignSelf: 'center',
  },

  
  view_flight_card: {
    margin: 20,
    backgroundColor: 'white',
    shadowColor: colors.gray,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 8,
    marginTop: 0,
    marginBottom: 10,
    paddingBottom:20
  },

  logo_icon_country: {
    width: 18,
    height: 19,
    marginTop: 3,
    marginRight:15
  },

  logo_icon_facebook: {
    width: 40,
    height: 40,
    marginTop: 3,
    marginRight:15
  },

  logo_icon_arrow: {
    width: 10,
    height: 10,
    position:'absolute',
    right:0,
    alignSelf:'center'
  },

  logo_right_arrow: {
    width: 8,
    height: 12,
    position:'absolute',
    right:0,
    alignSelf:'center'
  },

  name_sub : {
    alignSelf:'center',
    color: colors.main_dark_color, 
    fontFamily: fonts.SemiBold, 
    marginLeft: 20 
  },

  name_sub_row : {
    alignSelf:'center',
    color: colors.main_color_dark, 
    fontFamily: fonts.SemiBold, 
    flex:1,
    textAlign:'center'
  },

  name_sub_row_unselected : {
    alignSelf:'center',
    color: colors.gray, 
    fontFamily: fonts.SemiBold, 
    flex:1,
    textAlign:'center'
  },

  View_add: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 40,
    borderRadius: 5,
    flex:1,
    marginTop: 15,
    marginLeft: 20,
    marginBottom: 20,
    backgroundColor: colors.main_dark_color,
  },

  View_add_invite: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 40,
    borderRadius: 5,
    marginTop: 15,
    marginLeft: 20,  
    marginBottom: 20,
    flex:1,
    borderColor:colors.gray,
    borderWidth:1
  },

  logintext_button: {
    color: colors.white,
    fontSize: sizes.regular,
    fontFamily: fonts.SemiBold,
    alignSelf: 'center',
  },

  logintext_button_dark: {
    color: colors.dark_gray,
    fontSize: sizes.regular,
    fontFamily: fonts.SemiBold,
    alignSelf: 'center',
  },

});
