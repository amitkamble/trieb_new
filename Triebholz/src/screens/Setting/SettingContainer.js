import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import SettingComponent from './SettingComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const SettingContainer = (props) => {

  const { navigation, clearUserObj, route, alert_action, boarding_action, travel_advice_action, travel_docs_action,
    aircraft_action, from_flight_action, to_flight_action, airlines_flight_action, baggeges_fight_action,
    shopping_fight_action } = props;
  const [isalert, setAlert] = useState(store.getState().userSession.alert);
  const [isSubscribe, SetSubscribe] = useState(false);
  const [is_boarding_pass, setBoardingPass] = useState(store.getState().userSession.boarding_pass);
  const [isTravelAdvice, setTravelAdvice] = useState(store.getState().userSession.travel_advice);
  const [isTravelDocs, setTravelDocs] = useState(store.getState().userSession.travel_docs);
  const [isAircraft, setAircraft] = useState(store.getState().userSession.aircraft);
  const [isFrom_flight, setFromFlight] = useState(store.getState().userSession.from_flight);
  const [isTo_flight, setToFlight] = useState(store.getState().userSession.to_flight);
  const [isAirlines, setAirlines] = useState(store.getState().userSession.airlines);
  const [isBaggage, setBaggage] = useState(store.getState().userSession.baggeges);
  const [isShopping, setShopping] = useState(store.getState().userSession.shopping);

  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }

  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
    navigation.navigate('LoginContainer')
  };

  const EditProfile = () => {
    navigation.navigate('EditProfile')
  };

  const setUp_widgets = () => {
    navigation.navigate('Widget',{data_selected : ''})
  };

  const onBack = () => {
    navigation.goBack();
  };

  
  const manage_link_ac = () => {
    navigation.navigate('Import_flight')
};


  const alertAction = () => {
    // SetSubscribe(!isSubscribe)
    // if(!isSubscribe){
      navigation.navigate('Subscribe_eChannelContainer')
    // }
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);



  return (
    <SettingComponent props={props} login_button_pressed={login_button_pressed} onBack={onBack}
      EditProfile={EditProfile} alertAction={alertAction} isalert={isalert} is_boarding_pass={is_boarding_pass} isTravelAdvice={isTravelAdvice}
      isTravelDocs={isTravelDocs} isAircraft={isAircraft} isFrom_flight={isFrom_flight} isTo_flight={isTo_flight}
      isAirlines={isAirlines} isBaggage={isBaggage} isShopping={isShopping} isSubscribe={isSubscribe}
      setUp_widgets={setUp_widgets} manage_link_ac={manage_link_ac}/>
  );
};
SettingContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = () => ({
});

const mapDispatchToProps = (dispatch) => ({
  alert_action: () => dispatch(Actions.alert_action()),
  boarding_action: () => dispatch(Actions.boarding_action()),
  travel_advice_action: () => dispatch(Actions.travel_advice_action()),
  travel_docs_action: () => dispatch(Actions.travel_docs_action()),
  aircraft_action: () => dispatch(Actions.aircraft_action()),
  from_flight_action: () => dispatch(Actions.from_flight_action()),
  to_flight_action: () => dispatch(Actions.to_flight_action()),
  airlines_flight_action: () => dispatch(Actions.airlines_flight_action()),
  baggeges_fight_action: () => dispatch(Actions.baggeges_fight_action()),
  shopping_fight_action: () => dispatch(Actions.shopping_fight_action()),
});


export default connect(mapStateToProps, mapDispatchToProps)(SettingContainer);
