import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView } from 'react-native';
import styles from './styles';
import { Images } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import OTPInputView from '@twotalltotems/react-native-otp-input'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const Reset_successComponent = memo((props) => {


  // Storing Name & Pass
  const [user, set_user] = useState({
    pass: '',
    confirm_pass: '',
  });

  const login_button_pressed = () => {
    props.login_button_pressed();
  }

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor={colors.main_dark_color} />

      <Image source={Images.circle_login} style={styles.logo_new} />

      <Image source={Images.map} style={styles.logo_map} />
      
      <View style={styles.subcontainer}/>
      <Image source={Images.lock_reset_success} style={styles.logo_lock} />

      <Text style={styles.logintitle}>{'Your Password has been \n\t\t\t\tsuccessfully reset.'}</Text>

      <TouchableOpacity onPress={() => login_button_pressed()} style={styles.login}>
        <Text style={styles.logintext_button}>LOGIN</Text>
      </TouchableOpacity>

    </View>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

export default Reset_successComponent;
