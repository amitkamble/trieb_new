import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors, fonts, sizes } from '../../utils/theme';

import Icon from 'react-native-vector-icons/Ionicons';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default StyleSheet.create({
  container: {
     flex:1,
     backgroundColor:colors.main_dark_color,
     justifyContent:'center'
  },

  subcontainer: {
     backgroundColor:'#4D648D',
     opacity:0.9,
     flex:1, 
     height:height,
     width:width,
     position:'absolute'
  },

  logo_new: {
    height: 95,
    width: 93,
    position:'absolute',
    marginTop:-10,
    marginRight:-30,
    right:0,
    top:0
  },

  logo_map: {
    height: 300,
    width: width,
    position:'absolute',
    marginRight:0,
    resizeMode:'cover'
  },

  logo_lock: {
    height: 135,
    width: 141,
    alignSelf:'center'
  },
  back_image: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: width,
    height:height,
    flex:1,
    position:'absolute',
    backgroundColor:colors.main_dark_color,
  },

  logintitle: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.Regular,
    alignSelf:'center',
    marginTop:30
  },

  login: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 50,
    marginBottom: 50,
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 90,
    marginRight: 90,
    marginTop: 85,
    backgroundColor: colors.white,
  },

  logintext_button: {
    color: colors.main_dark_color,
    fontSize: sizes.regular,
    fontFamily: fonts.Bold,
    alignSelf:'center',
  },


});
