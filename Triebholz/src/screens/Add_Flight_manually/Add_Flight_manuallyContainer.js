import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Add_Flight_manuallyComponent from './Add_Flight_manuallyComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const Add_Flight_manuallyContainer = (props) => {

  const { navigation, clearUserObj, route, add_flight } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };
  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }

  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
    navigation.navigate('LoginContainer')
  };

  const ScanScreen = () => {
    navigation.navigate('ScanScreen')
  };

  const onBack = () => {
    navigation.goBack();
  };

  const add_flight_select = (data_fl) => {
    Add_Flight(data_fl)
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);


  const Add_Flight = async (data_flight) => {
    setLoading(true);

    console.log("data_flight" + JSON.stringify(data_flight))

    await add_flight(data_flight)
      .then((response) => {
        setLoading(false);
        var data_response = response.payload.data;

        if (data_response.status == '200') {
          navigation.navigate('HomePage')
          alert(data_response.message)

        } else {
          alert(data_response.message)
        }


      }).catch(error => {
        console.log('error' + JSON.stringify(error));
        Alert.alert(error.error.data)
        setLoading(false);
      });
  }


  return (
    <Add_Flight_manuallyComponent props={props} login_button_pressed={login_button_pressed} onBack={onBack}
      ScanScreen={ScanScreen} add_flight_select={add_flight_select} isLoading={isLoading} />

  );
};
Add_Flight_manuallyContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
  add_flight: (params) => dispatch(Actions.add_flight(params))

});


export default connect(mapStateToProps, mapDispatchToProps)(Add_Flight_manuallyContainer);
