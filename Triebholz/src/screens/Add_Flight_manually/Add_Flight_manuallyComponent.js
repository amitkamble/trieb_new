import React, { memo, useState, useRef } from 'react';
import { View, Alert, Dimensions, TextInput, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import { colors, fonts, sizes } from '../../utils/theme';
import * as geolib from 'geolib';
import languages from '../../globals/languages';
import Spinner from 'react-native-loading-spinner-overlay';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const Add_Flight_manuallyComponent = memo((props) => {

  // Storing Name & Pass
  const [airline, set_airline] = useState({
    carrierCode: '',
    flight_no: '',
    departure_time: '',
    departure_date: '',
    user_id: '1',
    class_trip: '',
    seat_no: '',
    booking_reference: '',
  });

  const onBack = () => {
    props.onBack();
  }

  const add_flight_select = () => {

    if(airline.carrierCode != '' && airline.flight_no != '' && airline.departure_time != '' && airline.departure_date != '' ){
      let data = new FormData();
      data.append("carrierCode", airline.carrierCode);
      data.append("flight_no", airline.flight_no);
      data.append("departure_time", airline.departure_time);
      data.append("departure_date", airline.departure_date);
      data.append("user_id", 1);
      data.append("class_trip", '');
      data.append("seat_no", '');
      data.append("booking_reference", '');
  
      // console.log('here',JSON.stringify(data))
         props.add_flight_select(data);

    }else{
      alert('Please Fill All Data')
    }
   
  }

  return (
    <ScrollView contentContainerStyle={styles.container}>

      <View style={styles.logo_map} >

        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 50, }}>

          <Text style={styles.logintitle}>{Language.lang.add_manul}</Text>
          <TouchableOpacity onPress={() => onBack()} style={{
            position: 'absolute',
            right: 0,
            marginTop: 10,
            marginRight: 20
          }}>
            <Image source={Images.close} style={styles.logo_lock} />

          </TouchableOpacity>

        </View>
      </View>

      <View style={styles.sub_container}>

        <TextInput
          placeholder={languages.lang.carrierCode}
          onChangeText={(value) => set_airline({...airline, ['carrierCode']: value })}
          value={airline.carrierCode}
          placeholderTextColor={colors.gray}
          autoCorrect={false}
          style={styles.inputText} />

        <TextInput
          placeholder={languages.lang.flight_no}
          onChangeText={(value) => set_airline({...airline, ['flight_no']: value })}
          value={airline.flight_no}
          placeholderTextColor={colors.gray}
          autoCorrect={false}
          style={styles.inputText} />

        <TextInput
          placeholder={languages.lang.departure_time}
          onChangeText={(value) => set_airline({ ...airline, ['departure_time']: value })}
          value={airline.departure_time}
          placeholderTextColor={colors.gray}
          autoCorrect={false}
          style={styles.inputText} />

        <TextInput
          placeholder={languages.lang.departure_date}
          onChangeText={(value) => set_airline({ ...airline, ['departure_date']: value })}
          value={airline.departure_date}
          placeholderTextColor={colors.gray}
          autoCorrect={false}
          style={styles.inputText} />


      </View>
      <TouchableOpacity onPress={() => add_flight_select()} style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
        <Text style={styles.View_add_pop}>{Language.lang.submit}</Text>

      </TouchableOpacity>


      <Spinner
        visible={props.isLoading}
        textContent={'Loading...'}
        color={'white'}
        overlayColor={'rgba(0, 0, 0, 0.45)'}
        textStyle={styles.spinnerTextStyle}
      />
    </ScrollView>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default Add_Flight_manuallyComponent;
