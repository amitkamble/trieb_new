import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import MyTripsDetailsComponent from './MyTripsDetailsComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import { Alert, BackHandler, NativeModules, } from 'react-native';
import * as geolib from 'geolib';

const MyTripsDetailsContainer = (props) => {

  const { navigation, clearUserObj, route } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };
  const { data_selected } = route.params;
  const [center ,setCenter] = useState({});

  const [from_flight, setFromFli] = useState({});

  const [to_flight, setToFli] = useState({});


  const [data, setTerminal_data] = React.useState([
    {
      name: 'Terminal 1',
      url: 'http://www.africau.edu/images/default/sample.pdf',
    },
    {
      name: 'Terminal 2',
      url: 'http://www.africau.edu/images/default/sample.pdf',
    },
    {
      name: 'Terminal 3',
      url: 'http://www.africau.edu/images/default/sample.pdf',
    },
    {
      name: 'Terminal 4',
      url: 'http://www.africau.edu/images/default/sample.pdf',
    },
  ]);

  const [isalert, setAlert] = useState(store.getState().userSession.alert);
  const [is_boarding_pass, setBoardingPass] = useState(store.getState().userSession.boarding_pass);
  const [isTravelAdvice, setTravelAdvice] = useState(store.getState().userSession.travel_advice);
  const [isTravelDocs, setTravelDocs] = useState(store.getState().userSession.travel_docs);
  const [isAircraft, setAircraft] = useState(store.getState().userSession.aircraft);
  const [isFrom_flight, setFromFlight] = useState(store.getState().userSession.from_flight);
  const [isTo_flight, setToFlight] = useState(store.getState().userSession.to_flight);
  const [isAirlines, setAirlines] = useState(store.getState().userSession.airlines);
  const [isBaggage, setBaggage] = useState(store.getState().userSession.baggeges);
  const [isShopping, setShopping] = useState(store.getState().userSession.shopping);

  const [flight_data, SetFlightData] = useState([]);

  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

    console.log('data', JSON.stringify(data_selected))
    SetFlightData(data_selected)

    const center_value = geolib.getCenter([
      { latitude: data_selected.from_flight.latitude, longitude: data_selected.from_flight.longitude },
      { latitude: data_selected.to_flight.latitude, longitude: data_selected.to_flight.longitude  },
    ]);

    
    setFromFli(data_selected.from_flight)
    setToFli( data_selected.to_flight )

    setCenter(center_value);
    const unsubscribe = navigation.addListener('focus', () => {
      
      setAlert(store.getState().userSession.alert);
      setBoardingPass(store.getState().userSession.boarding_pass);
      setTravelAdvice(store.getState().userSession.travel_advice);
      setTravelDocs(store.getState().userSession.travel_docs);
      setAircraft(store.getState().userSession.aircraft);
      setFromFlight(store.getState().userSession.from_flight);
      setToFlight(store.getState().userSession.to_flight);
      setAirlines(store.getState().userSession.airlines);
      setBaggage(store.getState().userSession.baggeges);
      setShopping(store.getState().userSession.shopping);
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);

    });
    return unsubscribe;

  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
    navigation.navigate('LoginContainer')
  };
  const onBack_click = () => {
    navigation.goBack();
  };
  const add_flight = () => {
    navigation.navigate('Add_Flight')
  };
  const set_widgets = () => {
    navigation.navigate('Widget',{data_selected :  data_selected})
  };

  const CheckList = () => {
    navigation.navigate('Checklist')
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);


  const ScanScreen = () => {
    navigation.navigate('ScanScreen')
  };

  const ViewTips = () => {
    navigation.navigate('Tips')
  };
  const navigate_Review = () => {
    navigation.navigate('Reviews')
  };


  return (
    <MyTripsDetailsComponent props={props} login_button_pressed={login_button_pressed} onBack_click={onBack_click}
      add_flight={add_flight} set_widgets={set_widgets} ScanScreen={ScanScreen} CheckList={CheckList} data={data}
      ViewTips={ViewTips} navigate_Review={navigate_Review} isalert={isalert} is_boarding_pass={is_boarding_pass} isTravelAdvice={isTravelAdvice}
      isTravelDocs={isTravelDocs} isAircraft={isAircraft} isFrom_flight={isFrom_flight} isTo_flight={isTo_flight}
      isAirlines={isAirlines} isBaggage={isBaggage} isShopping={isShopping} flight_data={flight_data}
      center={center} from_flight={from_flight} to_flight ={to_flight}/>

  );
};
MyTripsDetailsContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession }) => ({
  dashboard,
  userObj,
  isUserLoggedin: userSession.isUserLoggedin,
});

const mapDispatchToProps = (dispatch) => ({
});


export default connect(mapStateToProps, mapDispatchToProps)(MyTripsDetailsContainer);
