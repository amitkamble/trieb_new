import React, { memo, useState, useRef } from 'react';
import { View, FlatList, Dimensions, TextInput, Image, StyleSheet, Linking, Alert, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import AppIntroSlider from 'react-native-app-intro-slider';
import images from '../../globals/images';
import Swiper from 'react-native-swiper'
import { colors, fonts, sizes } from '../../utils/theme';
import LinearGradient from 'react-native-linear-gradient';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import Animated from 'react-native-reanimated';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
import RNPickerSelect from 'react-native-picker-select';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import PolylineDirection from '@react-native-maps/polyline-direction';
import MapView, { Polyline, PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import * as geolib from 'geolib';
import { ProgressBar, Colors } from 'react-native-paper';
import { TabView, SceneMap } from 'react-native-tab-view';
import languages from '../../globals/languages';
import Pie from 'react-native-pie'
import { store } from '../../redux/actions/store'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const MyTripsDetailsComponent = memo((props) => {
  const [isVisible, setVisiable] = useState(false);
  const [isVisible_pop, setVisiable_Pop] = useState(false);
  const [isVisible_note, setVisiable_note] = useState(false);
  const [isVisible_main, setVisiable_main] = useState(true);
  const [isVisible_aircraft, setVisiable_aircraft] = useState(false);
  const [airline, set_airline] = useState({
    name: '',
  });
  var isAlert = store.getState().userSession.alert;
  const [state, setState] = React.useState({
    index: 0,
    routes: [
      { key: 'first', title: 'Upcoming' },
      { key: 'second', title: 'Past' }
    ]
  });
  const [alert, setAlert] = useState(false);
  const [boarding_pass, set_boardingPass] = useState(false);
  const [travel_advice, set_Travel_advice] = useState(false);
  const [travel_docs, setTravel_docs] = useState(false);
  const [aircraft, setAircraft] = useState(false);
  const [from_flight, setFrom_Flight] = useState(false);
  const [to_flight, setTo_Flight] = useState(false);
  const [baggage, set_Baggeges] = useState(false);
  const [airlines, setAirlines] = useState(false);
  const mapDarkStyle = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#868d9c"
        }
      ]
    },
    {
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },

    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#4c4d4f"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "administrative.country",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.locality",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#bdbdbd"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#181818"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#2c2c2c"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#8a8a8a"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#373737"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#3c3c3c"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#4e4e4e"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#4D648D"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#3d3d3d"
        }
      ]
    }
  ];
  const LATITUDE_DELTA = 200;
  const [coordinates] = useState([

    { latitude: 28.2014833, longitude: -177.3813083 },
    { latitude: 39.849312, longitude: -104.673828 },
  ]);
  const chart_wh = 250
  const series = [123, 321, 123, 789, 537]
  const sliceColor = ['#F44336', '#2196F3', '#FFEB3B', '#4CAF50', '#FF9800']


  const view_all = () => {
    // props.login_button_pressed();
  }

  const add_flight = () => {
    props.add_flight();
  }

  const set_widgets = () => {
    props.set_widgets();
  }

  const CheckList = () => {
    props.CheckList();
  }

  const ViewTips = () => {
    props.ViewTips();
  }

  //to check and set focus error field or password  
  const onBack_click = () => {
    props.onBack_click();
  };

  const onCancel_press = () => {
    setVisiable_Pop(false)
    setVisiable_main(true)
    setVisiable(false)
  }

  const onCancel_press_note = () => {
    setVisiable_note(false)
    setVisiable_main(true)
    setVisiable(false)
  }

  const onCancel_press_air = () => {
    setAircraft(false)
    setVisiable_main(true)
    setVisiable(false)
  }

  const onCancel_press_aircraft = () => {
    setVisiable_aircraft(false)
    setVisiable_main(true)
    setVisiable(false)
  }

  const add_flight_select = () => {
    // props.add_flight_select()
    setVisiable_Pop(false)
    setVisiable_note(false)
    setVisiable_main(true)
    setVisiable(false)
  }


  const ScanScreen = () => {
    props.ScanScreen();
  }
  const navigate_Review = () => {
    props.navigate_Review();
  }

  const setVisiable_pop = () => {
    setVisiable_Pop(true)
    setVisiable_main(false)
    setVisiable(true)
  }

  const setVisiableAircraft = () => {
    setVisiable_aircraft(true)
    setVisiable_main(false)
    setVisiable(true)
  }

  const set_new_note = () => {
    setVisiable_note(true)
    setVisiable_main(false)
    setVisiable(true)
  }


  const renderItemDr = ({ item, index }) => (


    <TouchableOpacity onPress={() => Linking.openURL(item.url)} style={{
      marginLeft: 15,
      marginTop: 10,
      height: 90,
      padding: 10,
      backgroundColor: colors.white, borderRadius: 10, alignItems: 'center'
    }}>

      <Image source={Images.maps} style={styles.maps_icon} />

      <Text style={styles.view_all_from}>{item.name}</Text>
    </TouchableOpacity>


  );

  return (
    <View style={styles.container}>

      <View style={{
        height: 100,
        width: width,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.main_dark_color
      }}>
        <View style={styles.back_image} />

        <TouchableOpacity onPress={() => onBack_click()} style={{
          marginLeft: 15,
          marginTop: 20
        }}>
          <Image source={Images.back} style={styles.logo_back} />
        </TouchableOpacity>
        <Text style={styles.tab_bar_text}>{Language.lang.trips_card_case_sen}</Text>
        <TouchableOpacity onPress={() => set_widgets()} style={{ position: 'absolute', right: 0, top: 0, marginTop: 55, marginRight: 80 }}>
          <Image source={Images.widget} style={styles.logo_widget} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => add_flight()} style={{ position: 'absolute', right: 0, top: 0, marginTop: 52, marginRight: 45 }}>
          <Image source={Images.share} style={styles.logo_share} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => add_flight()} style={{ position: 'absolute', right: 0, top: 0, marginTop: 52, marginRight: 10 }}>
          <Image source={Images.notification} style={styles.logo_nitification} />
        </TouchableOpacity>
      </View>

      <ScrollView contentContainerStyle={{ flexGrow: 1, paddingBottom: isVisible ? 0 : 100 }}>

        <View style={styles.view_flight_card}>
          <Text style={styles.card_fight_name}>{'Flight ' + props.flight_data.flight_name}</Text>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.from_time}>{props.flight_data.take_of_time}</Text>
            <Text style={styles.to_time}>{props.flight_data.landing_time}</Text>
          </View>

          <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center' }}>
            <Text style={styles.From_flight}>{props.flight_data.code_name_from}</Text>
            <Image source={Images.flight_icon_from_to} style={styles.logo_icon_flight} />
            <Text style={styles.To_flight}>{props.flight_data.code_name_to}</Text>
          </View>

          <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20, marginRight: 20 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', flex: 0.4 }}>
              <Image source={Images.coutry1} style={styles.logo_icon_country} />
              <Text style={styles.From_country_name}>{props.flight_data.from_name}</Text>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'center', flex: 0.8 }}>
              <Text style={styles.journey_hours}>{props.flight_data.hours_journey}</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'center', flex: 0.4 }}>
              <Image source={Images.coutry1} style={styles.logo_icon_country} />
              <Text style={styles.From_country_name}>{props.flight_data.to_name}</Text>
            </View>

          </View>
          <View style={{ width: width - 40, height: 200, marginTop: 10 }}>
            <MapView
              provider={PROVIDER_GOOGLE}
              followUserLocation={false}
              showsUserLocation={false}
              zoomEnabled={true}
              zoomControlEnabled={false}
              style={{ flex: 1 }}
              initialRegion={{
                latitude: props.center.latitude,
                longitude: props.center.longitude,
                latitudeDelta: Platform.OS == 'ios' ? 60 : 120,
                longitudeDelta: Platform.OS == 'ios' ? 60 : 350
              }}
              zIndex={9}
              showsMyLocationButton={false}
              customMapStyle={mapDarkStyle}
            >

              <Marker
                coordinate={{ latitude: props.from_flight.latitude, longitude: props.from_flight.longitude }}
              >
                {/* <View> */}
                <View style={{ backgroundColor: colors.green_text_coot, width: 5, height: 5, borderRadius: 2.5 }}>

                </View>
                {/* </View> */}

              </Marker>
              <Marker
                coordinate={{ latitude: props.to_flight.latitude, longitude: props.to_flight.longitude }}
              >
                <View style={{ backgroundColor: colors.green_text_coot, width: 5, height: 5, borderRadius: 2.5 }}>

                </View>

              </Marker>

              <Polyline
                coordinates={
                  [
                    { latitude: props.from_flight.latitude, longitude: props.from_flight.longitude },
                    { latitude: props.to_flight.latitude, longitude: props.to_flight.longitude },
                  ]
                }
                strokeColor={'#71e856'} // fallback for when `strokeColors` is not supported by the map-provider
                strokeWidth={2}
                lineCap={'round'}
                geodesic={true}
                lineJoin={'round'}
                lineDashPhase={2000}
              />
            </MapView>
          </View>
          <Swiper showsPagination={false} style={{ height: 170 }} loop={false} showsButtons={true}
            nextButton={<Image source={Images.next_arrow} style={styles.logo_icon_next} />}
            prevButton={<Image source={Images.prev_arrow} style={styles.logo_icon_pre} />}
          >

            <View>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                <Text style={styles.check_in}>{Language.lang.check_in}</Text>
                {/* <Text style={styles.check_in_time}>{'3 hrs 21 min'}</Text> */}
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                <Text style={styles.on_time}>{Language.lang.on_time}</Text>
                <Text style={styles.check_in_time}>{props.flight_data.check_in_time}</Text>
              </View>
              <ProgressBar style={{ marginLeft: 20, marginRight: 20, marginBottom: 10 }} progress={0.25} color={colors.main_color_dark} />
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10, marginLeft: 10 }}>
                <Text style={styles.terminal}>{Language.lang.terminal}</Text>
                <Text style={styles.terminal_no}>{props.flight_data.from_terminal_no}</Text>
                <View style={{ width: 1, height: 15, marginLeft: 15, backgroundColor: colors.gray }} />
                <Text style={styles.terminal}>{Language.lang.gate}</Text>
                <Text style={styles.terminal_no}>{props.flight_data.from_gate_no}</Text>
              </View>
            </View>

            <View>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                <Text style={styles.check_in}>{Language.lang.boarding}</Text>
                {/* <Text style={styles.check_in_time}>{'3 hrs 21 min'}</Text> */}
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                <Text style={styles.on_time}>{Language.lang.on_time}</Text>
                <Text style={styles.check_in_time}>{props.flight_data.boarding_time}</Text>
              </View>
              <ProgressBar style={{ marginLeft: 20, marginRight: 20, marginBottom: 10 }} progress={0.5} color={colors.main_color_dark} />
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10, marginLeft: 10 }}>
                <Text style={styles.terminal}>{Language.lang.terminal}</Text>
                <Text style={styles.terminal_no}>{props.flight_data.from_terminal_no}</Text>
                <View style={{ width: 1, height: 15, marginLeft: 15, backgroundColor: colors.gray }} />
                <Text style={styles.terminal}>{Language.lang.gate}</Text>
                <Text style={styles.terminal_no}>{props.flight_data.from_gate_no}</Text>
              </View>
            </View>

            <View>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                <Text style={styles.check_in}>{Language.lang.take_off}</Text>
                {/* <Text style={styles.check_in_time}>{'3 hrs 21 min'}</Text> */}
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                <Text style={styles.on_time}>{Language.lang.on_time}</Text>
                <Text style={styles.check_in_time}>{props.flight_data.take_of_time}</Text>
              </View>
              <ProgressBar style={{ marginLeft: 20, marginRight: 20, marginBottom: 10 }} progress={0.75} color={colors.main_color_dark} />
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10, marginLeft: 10 }}>
                <Text style={styles.terminal}>{Language.lang.terminal}</Text>
                <Text style={styles.terminal_no}>{props.flight_data.from_terminal_no}</Text>
                <View style={{ width: 1, height: 15, marginLeft: 15, backgroundColor: colors.gray }} />
                <Text style={styles.terminal}>{Language.lang.gate}</Text>
                <Text style={styles.terminal_no}>{props.flight_data.from_gate_no}</Text>
              </View>
            </View>

            <View>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                <Text style={styles.check_in}>{Language.lang.landing}</Text>
                {/* <Text style={styles.check_in_time}>{props.flight_data.boarding_time}</Text> */}
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                <Text style={styles.on_time}>{Language.lang.on_time}</Text>
                <Text style={styles.check_in_time}>{props.flight_data.landing_time}</Text>
              </View>
              <ProgressBar style={{ marginLeft: 20, marginRight: 20, marginBottom: 10 }} progress={1.0} color={colors.main_color_dark} />
              <Image source={Images.plane} style={{ width: 20, height: 20, position: 'absolute', right: 0, marginRight: 5, marginTop: Platform.OS == 'ios' ? 68 : 72 }} />
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10, marginLeft: 10, width: width, }}>
                <Text style={styles.terminal}>{Language.lang.terminal}</Text>
                <Text style={styles.terminal_no}>{props.flight_data.to_terminal_no}</Text>
                {/* <View style={{ width: 1, height: 15, marginLeft: 15, backgroundColor: colors.gray }} /> */}
                {/* <Text style={styles.terminal}>{Language.lang.gate}</Text>
                <Text style={styles.terminal_no}>{props.flight_data.to_gate_no}</Text> */}
                <View style={{ width: 1, height: 15, marginLeft: 15, backgroundColor: colors.gray }} />
                <Text style={styles.terminal}>{Language.lang.baggage_claim}</Text>
                <Text style={styles.terminal_no}>{props.flight_data.baggage_claim_no}</Text>
              </View>

            </View>

          </Swiper>
        </View>

        {
          isVisible_main ?
            <View style={styles.view_flight_card}>

              {/* Alert            */}
              {props.isalert ?
                <TouchableOpacity onPress={() => setAlert(!alert)} style={styles.View_all}>
                  <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                    <Image source={Images.bell} style={styles.logo_bell} />
                    <Text style={styles.bottom_options_text}>{languages.lang.alerts}</Text>
                    <TouchableOpacity onPress={() => setAlert(!alert)} style={styles.logo_down_arrow_touch}>
                      <Image source={!alert ? Images.down_arrow : Images.up_arrow} style={styles.logo_down_arrow2} />
                    </TouchableOpacity>
                  </View>
                </TouchableOpacity>
                : null
              }
              {/* Alert View Hide & Show           */}
              {
                props.isalert ?
                  alert ?
                    <View style={styles.View_expandable}>
                      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                        <Text style={styles.view_text}>{props.flight_data.alerts[0].alert_title}</Text>
                        <Text style={styles.view_sub_text}>{'Arrival gate changed'}</Text>
                      </View>
                      <View style={{ height: 1, backgroundColor: colors.gray, marginTop: 10, width: width - 60 }} />
                      <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 10, marginRight: 20 }}>
                        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center' }}>
                          <Image source={Images.share_blue} style={styles.logo_icon_share} />
                          <Text style={styles.alert_text}>{languages.lang.share}</Text>
                        </TouchableOpacity>

                        <View style={{ height: 15, width: 1, alignSelf: 'center', marginLeft: 20, marginRight: 20, backgroundColor: colors.gray }} />
                        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center' }}>
                          <Image source={Images.view_more} style={styles.logo_icon_more} />
                          <Text style={styles.alert_text}>{languages.lang.view_more}</Text>
                        </TouchableOpacity>

                      </View>
                    </View>
                    : null
                  : null
              }
              {/* Alert            */}

              {/* Boarding pass            */}
              {props.is_boarding_pass ?
                <TouchableOpacity onPress={() => set_boardingPass(!boarding_pass)} style={styles.View_all}>
                  <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                    <Image source={Images.boarding_pass} style={styles.logo_pass} />
                    <Text style={styles.bottom_options_text}>{languages.lang.boarding_passes}</Text>
                    <TouchableOpacity onPress={() => set_boardingPass(!boarding_pass)} style={styles.logo_down_arrow_touch}>
                      <Image source={!boarding_pass ? Images.down_arrow : Images.up_arrow} style={styles.logo_down_arrow2} />
                    </TouchableOpacity>
                  </View>
                </TouchableOpacity>
                : null
              }
              {/* Boarding pass Hide & show View         */}

              {
                props.is_boarding_pass ?
                  boarding_pass ?
                    <View style={styles.View_expandable}>
                      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 8 }}>
                        <Text style={styles.pass_text}>{languages.lang.class}</Text>
                        <Text style={styles.pass_sub_text}>{props.flight_data.boarding_pass.class_trip}</Text>
                      </View>
                      <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                        <Text style={styles.pass_text}>{languages.lang.seat}</Text>
                        <Text style={styles.pass_sub_text}>{props.flight_data.boarding_pass.seat_no}</Text>
                      </View>
                      <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 8, }}>
                        <Text style={styles.pass_text}>{languages.lang.booking_reference}</Text>
                        <Text style={styles.pass_sub_text}>{props.flight_data.boarding_pass.booking_reference}</Text>
                      </View>
                      <Image source={Images.add_to_calender} style={styles.logo_icon_share_calender} />

                      <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginBottom: 8, }}>
                        <Text style={styles.pass_sub_text}>{'No boarding pass attached'}</Text>
                      </View>
                      <View style={{ height: 1, backgroundColor: colors.gray, marginTop: 10, width: width - 60 }} />
                      <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 10, marginRight: 20 }}>
                        <TouchableOpacity onPress={() => ScanScreen()} style={{ flexDirection: 'row', justifyContent: 'center' }}>
                          <Image source={Images.scan} style={styles.logo_icon_scanner} />
                          <Text style={styles.alert_text}>{languages.lang.scan}</Text>
                        </TouchableOpacity>

                        <View style={{ height: 15, width: 1, alignSelf: 'center', marginLeft: 20, marginRight: 20, backgroundColor: colors.gray }} />
                        <TouchableOpacity onPress={() => setVisiable_pop()} style={{ flexDirection: 'row', justifyContent: 'center' }}>
                          <Image source={Images.edit} style={styles.logo_icon_scanner} />
                          <Text style={styles.alert_text}>{languages.lang.edit}</Text>
                        </TouchableOpacity>

                      </View>
                    </View>
                    : null
                  : null
              }
              {/* Boarding pass            */}

              {/* Travel Advice           */}
              {props.isTravelAdvice ?
                <TouchableOpacity onPress={() => set_Travel_advice(!travel_advice)} style={styles.View_all}>
                  <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                    <Image source={Images.travel_advice} style={styles.logo_advice} />
                    <Text style={styles.bottom_options_text}>{languages.lang.travel_advice}</Text>
                    <TouchableOpacity onPress={() => set_Travel_advice(!travel_advice)} style={styles.logo_down_arrow_touch}>
                      <Image source={!travel_advice ? Images.down_arrow : Images.up_arrow} style={styles.logo_down_arrow2} />
                    </TouchableOpacity>
                  </View>
                </TouchableOpacity>
                : null
              }
              {/* Travel Advice   Hide and show View        */}
              {props.isTravelAdvice ?
                travel_advice ?
                  props.flight_data.travel_advice.lenght < 0 ?
                    <View style={styles.View_expandable}>
                      <Text style={styles.advice_text}>{languages.lang.free_to_enter}</Text>

                      <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginBottom: 8, }}>
                        <Text style={styles.advice_sub_text}>{props.flight_data.travel_advice[0].advice_description}</Text>
                      </View>
                      <View style={{ height: 1, backgroundColor: colors.gray, width: width - 60, marginTop: 10, marginBottom: 10 }} />
                      <Text style={styles.advice_text}>{languages.lang.view_all}</Text>

                    </View>
                    : null
                  : null
                : null
              }
              {/* Travel Advice           */}

              {/* Travel Docs           */}
              {
                props.isTravelDocs ?
                  <TouchableOpacity onPress={() => setTravel_docs(!travel_docs)} style={styles.View_all}>
                    <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                      <Image source={Images.travel_doc} style={styles.logo_docs} />
                      <Text style={styles.bottom_options_text}>{languages.lang.travel_docs}</Text>
                      <TouchableOpacity onPress={() => setTravel_docs(!travel_docs)} style={styles.logo_down_arrow_touch}>
                        <Image source={!travel_docs ? Images.down_arrow : Images.up_arrow} style={styles.logo_down_arrow2} />
                      </TouchableOpacity>
                    </View>
                  </TouchableOpacity>
                  : null
              }

              {/* Travel Docs  Hide & show View         */}
              {
                props.isTravelDocs ?
                  travel_docs ?
                    <View style={styles.View_expandable}>
                      <Image source={Images.check_list_top} style={styles.logo_icon_travel_doc} />

                      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 10, marginBottom: 8 }}>
                        <Text style={styles.pass_text}>{languages.lang.store_doc_here}</Text>
                      </View>
                      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: 8 }}>
                        <Text style={styles.pass_sub_text}>{languages.lang.attach_booking_doc}</Text>
                      </View>

                      <View style={{ height: 1, backgroundColor: colors.gray, marginTop: 10, width: width - 60 }} />
                      <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 10, marginRight: 20 }}>
                        <TouchableOpacity onPress={() => set_new_note()} style={{ flexDirection: 'row', justifyContent: 'center' }}>
                          <Image source={Images.new_note} style={styles.logo_icon_note} />
                          <Text style={styles.alert_text}>{languages.lang.New_Note}</Text>
                        </TouchableOpacity>

                        <View style={{ height: 15, width: 1, alignSelf: 'center', marginLeft: 20, marginRight: 20, backgroundColor: colors.gray }} />
                        <TouchableOpacity onPress={() => CheckList()} style={{ flexDirection: 'row', justifyContent: 'center' }}>
                          <Image source={Images.checklist} style={styles.logo_icon_checklist} />
                          <Text style={styles.alert_text}>{languages.lang.checklist}</Text>
                        </TouchableOpacity>

                      </View>
                    </View>
                    : null
                  : null
              }
              {/* Travel Docs           */}
              {
                props.isAircraft ?
                  <TouchableOpacity onPress={() => setAircraft(!aircraft)} style={styles.View_all}>
                    <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                      <Image source={Images.aircraft} style={styles.logo_aircraft} />
                      <Text style={styles.bottom_options_text}>{languages.lang.aircraft}</Text>
                      <Image source={Images.down_arrow} style={styles.logo_down_arrow} />
                    </View>
                  </TouchableOpacity>
                  : null
              }
              {
                props.isAircraft ?
                  aircraft ?
                    <View style={styles.View_expandable}>
                      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 8 }}>
                        <Text style={styles.pass_text}>{languages.lang.aircraft + ' : '}</Text>
                        <Text style={styles.pass_sub_text}>{props.flight_data.aircraft.aircraft_name}</Text>
                      </View>
                      <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                        <Text style={styles.pass_text}>{languages.lang.tail_number + ' : '}</Text>
                        <Text style={styles.pass_sub_text}>{props.flight_data.aircraft.tail_no}</Text>
                      </View>
                      <Image source={Images.airoplane} style={styles.logo_icon_aeroplane} />

                      <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 18, marginTop: 20 }}>
                        {props.flight_data.aircraft.business ?
                          <TouchableOpacity onPress={() => onCancel_press_note()} style={{ flex: 1, flexDirection: 'column', }}>
                            <Text style={styles.card_add_new_aircraft}>{'First class'}</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', position: 'absolute', marginBottom: 8, marginLeft: 30, marginTop: 30 }}>
                              {props.flight_data.aircraft.aircraft_class[0].wifi == 'true' ?
                                <Image source={Images.wifi} style={styles.logo_down_wifi} />
                                // console.log(props.flight_data.aircraft[0].aircraft_class[0].wifi)
                                : null
                              }
                              {props.flight_data.aircraft.aircraft_class[0].video == 'true' ?

                                <Image source={Images.youtube} style={styles.logo_down_youtube} />
                                : null}
                              {props.flight_data.aircraft.aircraft_class[0].sleeper == 'true' ?

                                <Image source={Images.seat} style={styles.logo_down_seat} />
                                : null}
                              {props.flight_data.aircraft.aircraft_class[0].charger == 'true' ?

                                <Image source={Images.charger} style={styles.logo_down_charger} />
                                : null}

                            </View>
                          </TouchableOpacity>
                          : null}
                        {props.flight_data.aircraft.economy == 'true' ?

                          <TouchableOpacity onPress={() => onCancel_press_note()} style={{ flex: 1, flexDirection: 'row', }}>
                            <Text style={styles.card_add_new_aircraft}>{'Economy'}</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', position: 'absolute', marginBottom: 8, marginLeft: 30, marginTop: 30 }}>
                              {props.flight_data.aircraft.aircraft_class[1].wifi == 'true' ?
                                <Image source={Images.wifi} style={styles.logo_down_wifi} />
                                // console.log(props.flight_data.aircraft[0].aircraft_class[0].wifi)
                                : null
                              }
                              {props.flight_data.aircraft.aircraft_class[1].video == 'true' ?

                                <Image source={Images.youtube} style={styles.logo_down_youtube} />
                                : null}
                              {props.flight_data.aircraft.aircraft_class[1].sleeper == 'true' ?

                                <Image source={Images.seat} style={styles.logo_down_seat} />
                                : null}

                              {props.flight_data.aircraft.aircraft_class[1].charger == 'true' ?

                                <Image source={Images.charger} style={styles.logo_down_charger} />
                                : null}
                            </View>
                          </TouchableOpacity>
                          : null
                        }
                      </View>
                      {props.flight_data.aircraft.premium_economy == 'true' ?

                        <TouchableOpacity onPress={() => onCancel_press_note()} style={{ flexDirection: 'row' }}>
                          <Text style={styles.card_add_new_aircraft1}>{'Premium Economy'}</Text>
                          <View style={{ flexDirection: 'row', alignItems: 'center', position: 'absolute', marginBottom: 8, marginLeft: 30, marginTop: 30 }}>
                            {props.flight_data.aircraft.aircraft_class[2].wifi == 'true' ?
                              <Image source={Images.wifi} style={styles.logo_down_wifi} />
                              // console.log(props.flight_data.aircraft[0].aircraft_class[0].wifi)
                              : null
                            }
                            {props.flight_data.aircraft.aircraft_class[2].video == 'true' ?

                              <Image source={Images.youtube} style={styles.logo_down_youtube} />
                              : null}
                            {props.flight_data.aircraft.aircraft_class[2].sleeper == 'true' ?

                              <Image source={Images.seat} style={styles.logo_down_seat} />
                              : null}

                            {props.flight_data.aircraft.aircraft_class[2].charger == 'true' ?

                              <Image source={Images.charger} style={styles.logo_down_charger} />
                              : null}
                          </View>
                        </TouchableOpacity>
                        : null
                      }

                      <View style={{ height: 1, backgroundColor: colors.gray, marginTop: 10, width: width - 60 }} />
                      <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 10, marginRight: 20 }}>
                        <TouchableOpacity onPress={() => setVisiableAircraft()} style={{ flexDirection: 'row', justifyContent: 'center' }}>
                          <Image source={Images.edit} style={styles.logo_icon_scanner} />
                          <Text style={styles.alert_text}>{languages.lang.edit}</Text>
                        </TouchableOpacity>

                      </View>
                    </View>
                    : null
                  : null
              }
              {
                props.isFrom_flight ?
                  <TouchableOpacity onPress={() => setFrom_Flight(!from_flight)} style={styles.View_all}>
                    <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                      <Image source={Images.take_off} style={styles.logo_aircraft} />
                      <Text style={styles.bottom_options_text}>{props.flight_data.code_name_from + ', ' + props.flight_data.from_name}</Text>
                      <Image source={Images.down_arrow} style={styles.logo_down_arrow} />
                    </View>
                  </TouchableOpacity>
                  : null
              }
              {
                props.isFrom_flight ?
                  from_flight ?
                    <View style={styles.View_expandable}>
                      <View style={{ flexDirection: 'row', marginBottom: 8, marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 5, marginBottom: 8 }}>

                          <Pie
                            radius={20}
                            sections={[
                              {
                                percentage: 45,
                                color: '#FDCD02',
                              },
                              {
                                percentage: 55,
                                color: '#ffffff',
                              },
                            ]}
                            strokeCap={'butt'}
                          />
                          <View style={{ flexDirection: 'column', marginBottom: 8 }}>
                            <Text style={styles.from_text_plane}>{props.flight_data.from_flight.check_in_time + 'min'}</Text>
                            <Text style={styles.from_text_plane_check_in}>{languages.lang.check_in}</Text>
                          </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginBottom: 8 }}>

                          <Pie
                            radius={20}
                            sections={[
                              {
                                percentage: 45,
                                color: '#FDCD02',
                              },
                              {
                                percentage: 55,
                                color: '#ffffff',
                              },
                            ]}
                            strokeCap={'butt'}
                          />
                          <View style={{ flexDirection: 'column', marginBottom: 8 }}>
                            <Text style={styles.from_text_plane}>{props.flight_data.from_flight.passport_time + 'min'}</Text>
                            <Text style={styles.from_text_plane_check_in}>{languages.lang.passport}</Text>
                          </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginBottom: 8 }}>

                          <Pie
                            radius={20}
                            sections={[
                              {
                                percentage: 45,
                                color: '#FDCD02',
                              },
                              {
                                percentage: 55,
                                color: '#ffffff',
                              },
                            ]}
                            strokeCap={'butt'}
                          />
                          <View style={{ flexDirection: 'column', marginBottom: 8 }}>
                            <Text style={styles.from_text_plane}>{props.flight_data.from_flight.security_check_time + 'min'}</Text>
                            <Text style={styles.from_text_plane_check_in}>{languages.lang.security}</Text>
                          </View>
                        </View>
                      </View>

                      <View style={{ height: 1, backgroundColor: colors.gray, marginTop: 10, width: width - 60 }} />

                      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                        <Text style={styles.alert_text}>{languages.lang.tips}</Text>
                      </View>
                      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>

                        <Image source={Images.profile_image} style={styles.profile_image} />
                        <Text style={styles.pass_sub_text}>{' Nice & air train is simple'}</Text>

                      </View>
                      <TouchableOpacity onPress={() => ViewTips()} style={{
                        marginLeft: 15,
                        marginTop: 10
                      }}>

                        <Text style={styles.view_all_air}>{languages.lang.view_all}</Text>
                      </TouchableOpacity>
                      <View style={{ height: 1, backgroundColor: colors.gray, marginTop: 10, width: width - 60 }} />

                      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                        <Text style={styles.alert_text}>{languages.lang.airport_maps}</Text>
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 20 }}>
                        <FlatList
                          horizontal
                          data={props.data}
                          renderItem={renderItemDr}
                          keyExtractor={item => item.scheme_code}
                        />
                      </View>
                    </View>

                    : null
                  : null
              }
              {
                props.isTo_flight ?
                  <TouchableOpacity onPress={() => setTo_Flight(!to_flight)} style={styles.View_all}>
                    <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                      <Image source={Images.landing} style={styles.logo_landing} />
                      <Text style={styles.bottom_options_text}>{props.flight_data.code_name_to + ', ' + props.flight_data.to_name}</Text>
                      <Image source={Images.down_arrow} style={styles.logo_down_arrow} />
                    </View>
                  </TouchableOpacity>
                  : null
              }
              {
                props.isTo_flight ?
                  to_flight ?
                    <View style={styles.View_expandable}>
                      <View style={{ flexDirection: 'row', marginBottom: 8, marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 5, marginBottom: 8 }}>

                          <Pie
                            radius={20}
                            sections={[
                              {
                                percentage: 45,
                                color: '#FDCD02',
                              },
                              {
                                percentage: 55,
                                color: '#ffffff',
                              },
                            ]}
                            strokeCap={'butt'}
                          />
                          <View style={{ flexDirection: 'column', marginBottom: 8 }}>
                            <Text style={styles.from_text_plane}>{props.flight_data.to_flight.check_in_time + 'min'}</Text>
                            <Text style={styles.from_text_plane_check_in}>{languages.lang.check_in}</Text>
                          </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginBottom: 8 }}>

                          <Pie
                            radius={20}
                            sections={[
                              {
                                percentage: 45,
                                color: '#FDCD02',
                              },
                              {
                                percentage: 55,
                                color: '#ffffff',
                              },
                            ]}
                            strokeCap={'butt'}
                          />
                          <View style={{ flexDirection: 'column', marginBottom: 8 }}>
                            <Text style={styles.from_text_plane}>{props.flight_data.to_flight.passport_time + 'min'}</Text>
                            <Text style={styles.from_text_plane_check_in}>{languages.lang.passport}</Text>
                          </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginBottom: 8 }}>

                          <Pie
                            radius={20}
                            sections={[
                              {
                                percentage: 45,
                                color: '#FDCD02',
                              },
                              {
                                percentage: 55,
                                color: '#ffffff',
                              },
                            ]}
                            strokeCap={'butt'}
                          />
                          <View style={{ flexDirection: 'column', marginBottom: 8 }}>
                            <Text style={styles.from_text_plane}>{props.flight_data.to_flight.security_check_time + 'min'}</Text>
                            <Text style={styles.from_text_plane_check_in}>{languages.lang.security}</Text>
                          </View>
                        </View>
                      </View>

                      <View style={{ height: 1, backgroundColor: colors.gray, marginTop: 10, width: width - 60 }} />

                      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                        <Text style={styles.alert_text}>{languages.lang.tips}</Text>
                      </View>
                      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>

                        <Image source={Images.profile_image} style={styles.profile_image} />
                        <Text style={styles.pass_sub_text}>{' Nice & air train is simple'}</Text>

                      </View>
                      <TouchableOpacity onPress={() => ViewTips()} style={{
                        marginLeft: 15,
                        marginTop: 10
                      }}>

                        <Text style={styles.view_all_air}>{languages.lang.view_all}</Text>
                      </TouchableOpacity>
                      <View style={{ height: 1, backgroundColor: colors.gray, marginTop: 10, width: width - 60 }} />

                      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                        <Text style={styles.alert_text}>{languages.lang.airport_maps}</Text>
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 20 }}>
                        <FlatList
                          horizontal
                          data={props.data}
                          renderItem={renderItemDr}
                          keyExtractor={item => item.scheme_code}
                        />
                      </View>
                    </View>
                    : null
                  : null
              }
              {
                props.isAirlines ?
                  <TouchableOpacity onPress={() => setAirlines(!airlines)} style={styles.View_all}>
                    <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                      <Image source={Images.airlines} style={styles.logo_airlines} />
                      <Text style={styles.bottom_options_text}>{languages.lang.airlines}</Text>
                      <Image source={Images.down_arrow} style={styles.logo_down_arrow} />
                    </View>
                  </TouchableOpacity>
                  : null
              }
              {
                props.isAirlines ?
                  airlines ?
                    <View style={styles.View_expandable}>
                      <Text style={styles.airline_text}>{'American Airlines'}</Text>

                      <View style={{ flexDirection: 'row', marginTop: 20, flex: 1, marginLeft: 10, alignItems: 'center' }}>

                        <Text style={styles.pass_airline_text}>{languages.lang.flight}</Text>
                        <View style={{ marginLeft: 80, marginRight: 20, position: 'absolute' }}>
                          <ProgressBar style={{ borderRadius: 5, height: 8, width: width / 1.9 }} progress={props.flight_data.aircraft.airline.flight_percentage / 100} color={colors.yello_text_coot} unfilledColor={colors.gray} />

                        </View>

                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 20, flex: 1, marginLeft: 10, alignItems: 'center' }}>

                        <Text style={styles.pass_airline_text}>{languages.lang.food}</Text>

                        <View style={{ marginLeft: 80, marginRight: 20, position: 'absolute' }}>
                          <ProgressBar style={{ borderRadius: 5, height: 8, width: width / 1.9 }} progress={props.flight_data.aircraft.airline.flight_percentage / 100} color={colors.yello_text_coot} unfilledColor={colors.gray} />

                        </View>
                      </View>

                      <View style={{ flexDirection: 'row', marginTop: 20, flex: 1, marginLeft: 10, alignItems: 'center' }}>

                        <Text style={styles.pass_airline_text}>{languages.lang.aircraft}</Text>

                        <View style={{ marginLeft: 80, marginRight: 20, position: 'absolute' }}>
                          <ProgressBar style={{ borderRadius: 5, height: 8, width: width / 1.9 }} progress={props.flight_data.aircraft.airline.aircraft_percentage / 100} color={colors.yello_text_coot} unfilledColor={colors.gray} />

                        </View>
                      </View>

                      <View style={{ flexDirection: 'row', marginTop: 20, flex: 1, marginLeft: 10, alignItems: 'center' }}>

                        <Text style={styles.pass_airline_text}>{languages.lang.craw}</Text>

                        <View style={{ marginLeft: 80, marginRight: 20, position: 'absolute' }}>
                          <ProgressBar style={{ borderRadius: 5, height: 8, width: width / 1.9 }} progress={props.flight_data.aircraft.airline.crew_percentage / 100} color={colors.green_text_coot} unfilledColor={colors.gray} />

                        </View>
                      </View>


                      <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 25 }}>
                        <TouchableOpacity onPress={() => console.log('here')} style={styles.social_media}>

                          <Image source={Images.twitter} style={styles.logo_twitter} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => console.log('here')} style={styles.social_media}>

                          <Image source={Images.phone_call} style={styles.logo_call} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => console.log('here')} style={styles.social_media}>

                          <Image source={Images.globe} style={styles.logo_globe} />
                        </TouchableOpacity>
                      </View>
                      <View style={{ height: 1, backgroundColor: colors.gray, marginTop: 15, width: width - 60 }} />

                      <TouchableOpacity onPress={() => navigate_Review()} style={{ flexDirection: 'row', alignItems: 'center', flex: 1, marginTop: 10 }}>
                        <Text style={styles.bag_text_airlines}>{languages.lang.view_all_review}</Text>
                        <Text style={styles.airlines_text}>{'17722 reviews'}</Text>
                      </TouchableOpacity>

                    </View>
                    : null
                  : null
              }
              {props.isBaggage ?
                <TouchableOpacity onPress={() => set_Baggeges(!baggage)} style={styles.View_all}>
                  <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                    <Image source={Images.baggae} style={styles.logo_bag} />
                    <Text style={styles.bottom_options_text}>{languages.lang.baggage}</Text>
                    <Image source={Images.down_arrow} style={styles.logo_down_arrow} />
                  </View>
                </TouchableOpacity>
                : null
              }
              {
                props.isBaggage ?
                  baggage ?
                    <View style={styles.View_expandable}>

                      <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>

                        <View style={{ alignItems: 'flex-start', marginLeft: 10, flex: 1, marginTop: 10 }}>

                          <Text style={styles.pass_sub_text}>{languages.lang.type}</Text>
                          <Text style={styles.pass_sub_text_bag}>{languages.lang.personal_item}</Text>
                          <Text style={styles.pass_sub_text_bag}>{languages.lang.carry_on}</Text>
                          <Text style={styles.pass_sub_text_bag}>{languages.lang.checked}</Text>

                        </View>
                        <View style={{ alignItems: 'center', flex: 1, marginTop: 10 }}>

                          <Text style={styles.pass_sub_text}>{languages.lang.weight}</Text>
                          <Text style={styles.pass_sub_text_bag}>{props.flight_data.baggage.personal_item_weight + 'kg'}</Text>
                          <Text style={styles.pass_sub_text_bag}>{props.flight_data.baggage.carry_on_weight + 'kg'}</Text>
                          <Text style={styles.pass_sub_text_bag}>{props.flight_data.baggage.checked_weight + 'kg'}</Text>

                        </View>
                        <View style={{ alignItems: 'center', flex: 1, marginTop: 10 }}>

                          <Text style={styles.pass_sub_text}>{languages.lang.size_cm}</Text>
                          <Text style={styles.pass_sub_text_bag}>{props.flight_data.baggage.personal_item_cm}</Text>
                          <Text style={styles.pass_sub_text_bag}>{props.flight_data.baggage.carry_on_cm}</Text>
                          <Text style={styles.pass_sub_text_bag}>{props.flight_data.baggage.checked_cm}</Text>

                        </View>
                      </View>

                      <View style={{ height: 1, backgroundColor: colors.gray, marginTop: 15, width: width - 60 }} />

                      <TouchableOpacity onPress={() =>
                        Alert.alert(
                          'Baggages Rules',
                          '\nA passenger residing abroad for more than one year, on return to India, shall be allowed clearance free of duty in his bona fide baggage of jewellery upto a weight, of twenty grams with a value cap of fifty thousand rupees',
                          [
                            { text: 'OK', onPress: () => console.log('Ok Pressed') },
                          ],
                          { cancelable: false }
                        )} style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                        <Text style={styles.bag_text}>{languages.lang.bag_rules}</Text>
                      </TouchableOpacity>

                    </View>
                    : null
                  : null
              }
              {props.isShopping ?
                <TouchableOpacity onPress={() => view_all()} style={styles.View_all}>
                  <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                    <Image source={Images.shopping} style={styles.logo_shopping} />
                    <Text style={styles.bottom_options_text}>{languages.lang.shopping}</Text>
                    <Image source={Images.down_arrow} style={styles.logo_down_arrow} />
                  </View>
                </TouchableOpacity>
                : null
              }
            </View>
            :
            null
        }
      </ScrollView>
      {
        isVisible ? <View style={{ height: height, opacity: 0.8, width: width, backgroundColor: colors.background_opacity, position: 'absolute' }} />
          : null
      }
      {
        !isVisible_pop ?
          null
          :
          <View style={styles.view_import_program}>

            <TouchableOpacity style={{ margin: 20 }}>
              <View style={{ flexDirection: 'row', flex: 1, marginTop: 10 }}>
                <Image source={Images.boarding_pass} style={styles.logo_icon_pass} />
                <Text style={styles.add_program}>{languages.lang.boarding_pass}</Text>
                <Image source={Images.paperclip} style={styles.logo_icon_attachment} />

              </View>
            </TouchableOpacity>

            <View style={{ width: width - 40, marginLeft: 20, marginRight: 20, height: 1, backgroundColor: colors.view_color }} />
            <TextInput
              placeholder={languages.lang.class}
              onChangeText={(value) => set_airline({ ['airline']: value })}
              value={airline.name}
              placeholderTextColor={colors.gray}
              autoCorrect={false}
              style={styles.inputText} />
            <TextInput
              placeholder={languages.lang.seat}
              onChangeText={(value) => set_airline({ ['airline']: value })}
              value={airline.name}
              placeholderTextColor={colors.gray}
              autoCorrect={false}
              style={styles.inputText} />
            <TextInput
              placeholder={languages.lang.booking_reference}
              onChangeText={(value) => set_airline({ ['airline']: value })}
              value={airline.name}
              placeholderTextColor={colors.gray}
              autoCorrect={false}
              style={styles.inputText} />


            <View style={{ flexDirection: 'row', flex: 1, marginTop: 40 }}>
              <TouchableOpacity onPress={() => onCancel_press()} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.card_add_new_pop}>{Language.lang.cancel}</Text>

              </TouchableOpacity>
              <TouchableOpacity onPress={() => add_flight_select()} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.View_add_pop}>{Language.lang.save}</Text>

              </TouchableOpacity>
            </View>

          </View>

      }
      {
        !isVisible_note ?
          null
          :
          <View style={styles.view_import_program}>

            <TouchableOpacity style={{ margin: 20 }}>
              <View style={{ flexDirection: 'row', flex: 1, marginTop: 10 }}>
                <Image source={Images.new_note} style={styles.logo_icon_note_pop} />
                <Text style={styles.add_program}>{languages.lang.New_Note}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{ alignSelf: 'center', margin: 20, height: 45, width: 150, borderWidth: 1, alignItems: 'center', justifyContent: 'center', borderColor: colors.main_dark_color }}>
              <View style={{ flexDirection: 'row' }}>
                <Image source={Images.paperclip} style={styles.logo_icon_attachment_note} />
                <Text style={styles.add_program}>{languages.lang.attach_file}</Text>
              </View>
            </TouchableOpacity>
            <TextInput
              placeholder={languages.lang.note_text}
              onChangeText={(value) => set_airline({ ['airline']: value })}
              value={airline.name}
              placeholderTextColor={colors.gray}
              autoCorrect={false}
              style={styles.inputText} />
            <View style={{ flexDirection: 'row', flex: 1, marginTop: 40 }}>
              <TouchableOpacity onPress={() => onCancel_press_note()} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.card_add_new_pop}>{Language.lang.cancel}</Text>

              </TouchableOpacity>
              <TouchableOpacity onPress={() => add_flight_select()} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.View_add_pop}>{Language.lang.save}</Text>

              </TouchableOpacity>
            </View>

          </View>

      }
      {
        !isVisible_aircraft ?
          null
          :
          <View style={styles.view_import_program}>

            <TouchableOpacity style={{ margin: 20 }}>
              <View style={{ flexDirection: 'row', flex: 1, marginTop: 10 }}>
                <Image source={Images.aircraft} style={styles.logo_aircraft} />
                <Text style={styles.add_program}>{languages.lang.aircraft}</Text>
              </View>
            </TouchableOpacity>
            <TextInput
              placeholder={languages.lang.aircraft}
              onChangeText={(value) => set_airline({ ['airline']: value })}
              value={airline.name}
              placeholderTextColor={colors.gray}
              autoCorrect={false}
              style={styles.inputText} />
            <TextInput
              placeholder={languages.lang.tail_number}
              onChangeText={(value) => set_airline({ ['airline']: value })}
              value={airline.name}
              placeholderTextColor={colors.gray}
              autoCorrect={false}
              style={styles.inputText} />
            <View style={{ flexDirection: 'row', flex: 1, marginTop: 40 }}>
              <TouchableOpacity onPress={() => onCancel_press_aircraft()} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.card_add_new_pop}>{Language.lang.cancel}</Text>

              </TouchableOpacity>
              <TouchableOpacity onPress={() => onCancel_press_aircraft()} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.View_add_pop}>{Language.lang.save}</Text>

              </TouchableOpacity>
            </View>

          </View>

      }
    </View>

  );
});



const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    fontFamily: fonts.SemiBold,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    borderRadius: 8,
    color: colors.gray,
    borderColor: 'white',
    width: 120,
    padding: 20,
    marginLeft: 15,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
});

const pathOptions = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  geodesic: true,
  zIndex: 2
};
export default MyTripsDetailsComponent;
