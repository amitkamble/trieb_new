import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors, fonts, sizes } from '../../utils/theme';

import Icon from 'react-native-vector-icons/Ionicons';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default StyleSheet.create({
  container: {
    flexGrow: 1,
  },

  logo_map: {
    height:600,
    width: width+40,
    position: 'absolute',
  },

  logo_lock: {
    height: 30,
    width: 30,
    position: 'absolute',
    right: 0,
    marginTop: 70,
    marginRight: 20
  },

  logo_icon: {
    height: 35,
    width: 35,
    marginBottom:20
  },

  logo_icon_message: {
    height: 32,
    width: 38.5,
    marginBottom:20
  },

  logo_icon_flight: {
    height: 42,
    width:150,
    marginBottom:20,
    resizeMode:'contain',
    position : 'absolute',
    alignSelf:'center',
  },


  logo_icon_country: {
    width:15,
    height: 8,
    alignSelf:'center',
    marginTop:3
  },

  logo_icon_pass: {
    width:20,
    height: 14.59,
    alignSelf:'center',
  },
  logo_icon_note_pop: {
    width:16,
    height: 15,
    alignSelf:'center',
  },

  logo_icon_attachment: {
    width:16.88,
    height: 18.96,
    position:'absolute',
    right:0, 
    marginRight:10
  },

  logo_icon_attachment_note: {
    width:16.88,
    height: 18.96,
  },

  logo_icon_share: {
    width:15,
    height: 16,
    alignSelf:'center',
  },
  
  logo_icon_scanner: {
    width:16,
    height: 16,
    alignSelf:'center',
  },

  logo_icon_note: {
    width:16,
    height: 15,
    alignSelf:'center',
  },

  logo_icon_checklist: {
    width:12.02,
    height: 14.14,
    alignSelf:'center',
  },

  logo_icon_share_calender: {
    width:35,
    height: 35,
    position:'absolute',
    right : 0,
    top : 0,
    marginRight:10,
    marginTop:20
  },

  logo_icon_aeroplane: {
    width:210.18,
    height: 59.46,
    marginRight:10,
    marginTop:20,
    alignSelf:'center'
  },

  profile_image: {
    width:30,
    height: 30,
    marginLeft:15,
    marginRight:10,
    borderRadius:40
  },

  maps_icon: {
    width:30,
    height: 30,
    marginTop:5,
    borderRadius:40
  },

  logo_icon_travel_doc: {
    width:18.3,
    height: 20.2,
    top : 0,
    alignSelf:'center',
    marginTop:15
  },
  
  logo_icon_more: {
    width:16,
    height: 16,
    alignSelf:'center',
  },

  logo_icon_next: {
    width:15,
    height: 15,
    marginTop:123,
    marginRight:120
  },

  logo_icon_pre: {
    width:15,
    height: 15,
    marginTop:123,
    marginLeft:120
  },

  logo_back: {
    height: 13,
    width: 18,
  },

  logo_share: {
    height: 20,
    width: 19,
  },

  logo_widget: {
    height: 14,
    width: 21,
  },
  logo_nitification: {
    height: 20,
    width: 17,
  },

  logo_bell: {
    height: 20,
    width: 18,
  },

  logo_pass: {
    width: 20,
    height: 14.56,
  },

  logo_advice: {
    width: 20,
    height: 19,
  },

  logo_docs: {
    width: 20,
    height: 20.95,
  },
  logo_aircraft: {
    width: 20,
    height: 16,
  },
  logo_landing: {
    width: 20,
    height: 15,
  },

  logo_airlines: {
    width: 13.72,
    height: 22.59,
  },

  logo_bag: {
    width: 20,
    height: 30.69,
  },

  logo_shopping: {
    width: 22.49,
    height: 21.89,
  },

  logo_down_arrow: {
    height: 6,
    width: 12,
    marginTop:5,
    position:'absolute',
    right:0,
  },

  logo_down_arrow2: {
    height: 6,
    width: 12,
  },

  logo_down_wifi: {
    width: 14.22,
    height: 11.72,
  },

  logo_down_youtube: {
    width: 18,
    height: 11,
    marginLeft:10
  },

  logo_down_seat: {
    width: 10.06,
    height: 12.26,
    marginLeft:10
  },

  logo_down_charger: {
    width: 12.06,
    height: 12.26,
    marginLeft:10
  },

  logo_down_arrow_touch: {
    marginTop:5,
    position:'absolute',
    right:0,
  },

  back_image: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    height: 200,
    width: width,
    position:'absolute',
    backgroundColor: colors.main_dark_color
  },

  logintitle: {
    color: colors.white,
    fontSize: 22,
    fontFamily: fonts.SemiBold,
    alignSelf: 'center',
    marginTop: 70,
    marginLeft: 20
  },

  From_flight: {
    color: colors.main_dark_color,
    fontSize: 24,
    fontFamily: fonts.SemiBold,
    marginTop: 5,
    marginLeft: 20,
    flex:1,
  },

  From_country_name: {
    color: colors.main_dark_color,
    fontSize: 12,
    fontFamily: fonts.Regular,
    marginLeft: 5,
    marginTop:Platform.OS =='ios'? 5 : 3
  },

  alert_text: {
    color: colors.main_dark_color,
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    marginLeft: 15,
  },

  bag_text: {
    color: colors.main_dark_color,
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    marginLeft: 10,
  },

  bag_text_airlines: {
    color: colors.main_dark_color,
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    marginLeft: 10,
    flex:1
  },

  airlines_text: {
    color: colors.gray,
    fontSize: 13,
    fontFamily: fonts.Monsterat_Regular,
    marginLeft: 10,
    textAlign:'right',
    flex:1
  },

  airline_text: {
    color: colors.main_dark_color,
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    marginLeft: 10,
    marginTop:10
  },

  advice_text: {
    color: colors.main_dark_color,
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    marginLeft: 10,
  },

  bottom_options_text: {
    color: colors.main_dark_color,
    fontSize: 16,
    fontFamily: fonts.Regular,
    marginLeft: 50,
    position:'absolute',
  },

  Flight_time: {
    color: colors.dark_gray,
    fontSize: 12,
    fontFamily: fonts.Monsterat_SemiBold,
    marginLeft: 5,
    flex:1,
    textAlign:'right',
    alignSelf:'center',
    marginTop:15
  },

  journey_hours: {
    color: colors.main_dark_color,
    fontSize: 16,
    fontFamily: fonts.Monsterat_Regular,
    marginLeft: 5,
    textAlignVertical:'top'
  },

  To_flight: {
    color: colors.main_dark_color,
    fontSize: 24,
    fontFamily: fonts.SemiBold,
    marginTop: 5,
    paddingRight: 40,
    flex:1,
    textAlign:'right'
  },

  headingtitle: {
    color: colors.black,
    fontSize: 16,
    fontFamily: fonts.SemiBold,
    marginTop: 30,
    marginLeft: 20,
    marginBottom:20
  },

  tabBar: {
    flexDirection: 'row',
    marginLeft:10,
    marginTop:20
  },
  card_date: {
    color: colors.white,
    fontSize: 14,
    backgroundColor:colors.main_color_dark,
    fontFamily: fonts.Monsterat_SemiBold,
    paddingLeft:10,
    paddingRight:10,
    width:80,
    marginLeft:20,
    paddingTop:Platform.OS=='ios'? 8 : 0,
    textAlignVertical:'center',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    marginTop:20,
    height:34,
  },
  card_add_new: {
    color: colors.white,
    borderColor:'white', borderWidth:1,
      fontSize: 14,
    fontFamily: fonts.Regular,
    paddingLeft:10,
    paddingRight:10,
    width:110,
    marginLeft:20,
    marginRight:20,
    paddingTop:Platform.OS=='ios'? 8 : 0,
    textAlignVertical:'center',
    marginTop:30,
    height:34,
  },
  card_date_past: {
    color: colors.white,
    fontSize: 14,
    backgroundColor:colors.gray,
    fontFamily: fonts.Monsterat_SemiBold,
    paddingLeft:10,
    paddingRight:10,
    width:80,
    marginLeft:20,
    paddingTop:Platform.OS=='ios'? 8 : 0,
    textAlignVertical:'center',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    marginTop:20,
    height:34,
  },

  card_fight_name: {
    color: colors.main_dark_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    marginTop:20,
    alignSelf:'center',
    paddingLeft:20,
    paddingRight:20
  },

  from_time : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    marginTop:20,
    paddingLeft:20,
    flex:1
  },

  to_time : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    marginTop:20,
    paddingRight:35,
    flex:1,
    textAlign:'right'
  },

  check_in : {
    color: colors.black,
    fontSize: 16,
    fontFamily: fonts.Monsterat_Medium,
    textAlignVertical:'center',
    paddingLeft:20,
    flex:1
  },

  view_text : {
    color: colors.black,
    fontSize: 13,
    fontFamily: fonts.Monsterat_Medium,
    textAlignVertical:'center',
    paddingLeft:10,
    flex:1
  },
  pass_text : {
    color: colors.black,
    fontSize: 13,
    fontFamily: fonts.Monsterat_Medium,
    textAlignVertical:'center',
    paddingLeft:10,
  },
  from_text_plane : {
    color: colors.black,
    fontSize: 10,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    paddingLeft:10,
    marginTop:5
  },
  from_text_plane_check_in : {
    color: colors.gray,
    fontSize: 10,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    paddingLeft:10,
    marginTop:5
  },
  pass_sub_text : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    paddingRight:10,
    textAlign:'right'
  },
  pass_airline_text : {
    color: colors.main_color_dark,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingRight:10,
    textAlign:'right'
  },
  pass_sub_text_bag : {
    color: colors.main_dark_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    paddingRight:10,
    marginTop:15
  },
  view_all_from : {
    color: colors.main_color_dark,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    marginTop:20
  },
  view_all_air : {
    color: colors.main_color_dark,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    marginTop:10
  },
  view_map_name : {
    color: colors.main_color_dark,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'bottom',
    paddingRight:10,
  },
  advice_sub_text : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    paddingRight:10,
    marginTop:5
  },
  check_in_time : {
    color: colors.hint_color,
    fontSize: 14,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    paddingRight:25,
    flex:1,
    textAlign:'right'
  },
  view_sub_text : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    textAlignVertical:'center',
    paddingRight:10,
    flex:1,
    textAlign:'right'
  },
  on_time : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft:20,
    textAlignVertical:'center',
    flex:1,
  },
  terminal : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft:10,
    textAlignVertical:'center',
  },
  terminal_no : {
    color: colors.hint_color,
    fontSize: 12,
    fontFamily: fonts.Monsterat_Regular,
    paddingLeft:5,
    textAlignVertical:'center',
  },
  card_title: {
    color: colors.main_dark_color,
    fontSize: sizes.regular,
    fontFamily: fonts.SemiBold,
  },

  view_home_card: {
    flex: 1,
    margin: 10,
    height: 140,
    backgroundColor: 'white',
    borderRadius: 5,
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 8,
    alignItems:'center',
    justifyContent:'center'
  },

  view_flight_card: {
    margin: 20,
    backgroundColor: 'white',
    shadowColor: colors.gray,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 8,
    marginTop:0,
  },

  view_import_program: {
    backgroundColor: 'white',

    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
        marginTop: 30,
    paddingTop: 20,
    position: 'absolute',
    bottom: 0,
    width: width,
    paddingBottom: 50
  },
  inputText: {
    fontSize: sizes.regularLarge,
    height: Platform.OS == 'ios' ? 50 : 40,
    fontFamily: fonts.Regular,
    marginLeft: 20,
    marginRight: 20,
    borderBottomColor: colors.gray,
    color: colors.black,
    borderBottomWidth: 1,
    flex: 1,
    marginTop:20
  },
  inputTextGray: {
    fontSize: sizes.regularLarge,
    height: Platform.OS == 'ios' ? 50 : 40,
    fontFamily: fonts.Regular,
    marginLeft: 20,
    marginRight: 20,
    borderBottomColor: colors.main_color_dull,
    color: colors.gray,
    borderBottomWidth: 1,
    flex: 1,
    marginBottom: 10,
    marginTop: 20,
    alignSelf: 'center'
  },

  View_add_pop: {
    color: colors.white,
    borderColor: colors.main_dark_color,
    backgroundColor: colors.main_dark_color,
    borderWidth: 1,
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 20,
    marginRight: 20,
    paddingTop: Platform.OS == 'ios' ? 10 : 0,
    textAlignVertical: 'center',
    textAlign:'center',
    height: 40,
    flex:1
  },
  add_program: {
    color: colors.main_color_dark,
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    marginLeft: 15,
    alignSelf: 'center',
  },

  card_add_new_pop: {
    color: colors.main_color_dark,
    borderColor: colors.main_color_dark,
    borderWidth: 1,
    fontSize: 14,
    fontFamily: fonts.SemiBold,
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 20,
    marginRight: 20,
    paddingTop: Platform.OS == 'ios' ? 10 : 0,
    textAlignVertical: 'center',
    textAlign:'center',
    height: 40,
    flex:1
  },
  
  card_add_new_aircraft: {
    color: colors.main_color_dark,
    borderColor: colors.main_color_dark,
    borderWidth: 1,
    fontSize: 12,
    fontFamily: fonts.Regular,
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 20,
    marginRight: 20,
    paddingTop: Platform.OS == 'ios' ? 5 : 5,
    height: 50,
    flex:1
  }, 
  card_add_new_aircraft1: {
    color: colors.main_color_dark,
    borderColor: colors.main_color_dark,
    borderWidth: 1,
    fontSize: 12,
    fontFamily: fonts.Regular,
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 20,
    marginRight: 20,
    paddingTop: Platform.OS == 'ios' ? 5 : 5,
    height: 50,
  },
  
  View_all: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 60,
    paddingLeft:20,
    paddingRight:30,
    alignSelf: 'center',
    backgroundColor: colors.white,
    borderBottomColor:colors.expnadable_view_back,
    borderBottomWidth:1
  },

  View_expandable: {
    justifyContent: 'center',
    fontSize: sizes.regularLarge,
    padding:10,
    backgroundColor: colors.expnadable_view_back,
    borderBottomColor:colors.expnadable_view_back,
    borderBottomWidth:1
  },

  View_all_details: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 40,
    paddingLeft:30,
    paddingRight:30,
    alignSelf: 'center',
    borderRadius: 5,
    marginTop: 10,
    marginBottom:10,
    backgroundColor: colors.main_dark_color,
  },

  View_add: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: sizes.regularLarge,
    height: 50,
    width:115,
    alignSelf: 'flex-start',
    borderRadius: 5,
    marginTop: 15,
    marginLeft:20,
    marginBottom:20,
    backgroundColor: colors.main_dark_color,
  },

  logintext_button: {
    color: colors.white,
    fontSize: sizes.regular,
    fontFamily: fonts.SemiBold,
    alignSelf:'center',
  },

  tab_bar_text: {
    color: colors.white,
    fontSize: sizes.regular16,
    fontFamily: fonts.SemiBold,
    marginTop:18,
    marginLeft:20
  },

  logo_twitter : {
    width: 18.76, 
    height:16.42
  },

  logo_call : {
    width: 16.88, 
    height:16.91
  },

  logo_globe : {
    width: 16.91, 
    height:16.91
  },

  social_media : {
    width: 25, 
    height:25,
    marginLeft:10
  },

});
