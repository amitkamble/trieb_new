import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Select_FlightComponent from './Select_FlightComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import Moment from 'moment';

const Select_FlightContainer = (props) => {

  const { navigation, route, search_flight_scan, search_flight, add_flight } = props;
  const Options = { query: { "employeenumber": "manoj-cont", "firstname": "Demo ", "lastname": "Demo user" } };

  const { request_data } = route.params;
  const { value } = route.params;

  useEffect(() => {
    setLoading(false);
    const unsubscribe = navigation.addListener('focus', () => {
      if (request_data != undefined && request_data != null && request_data != '') {
        searchFlight(request_data);
      } else {
        // searchFlight(value);
        console.log(value)

        var string_new = value.replace(/\s\s+/g, ' ');
        var myArray = string_new.split(' ');
        var from_name = myArray[2].substring(0, 3)
        var to_name = myArray[2].substring(3, 6)
        var airline_name = myArray[2].substring(6)
        var myArray2 = myArray[4].split('Y');
        var number = parseInt(myArray2[0]);
        var j1 = julianIntToDate(number)
        var cureentDate = Moment(j1).format('MM-DD')
        var year = new Date().getFullYear();
        var New_data = year +'-' + cureentDate;


        searchScanFlight(myArray[3],New_data,airline_name ,from_name, to_name)
      }
      BackHandler.addEventListener('hardwareBackPress', (handleBackButtonClick));
    });
    return unsubscribe;
  }, []);


  function julianIntToDate(JD) {
    // convert a Julian number to a Gregorian Date.
    //    S.Boisseau / BubblingApp.com / 2014
    var y = 4716;
    var v = 3;
    var j = 1401;
    var u = 5;
    var m = 2;
    var s = 153;
    var n = 12;
    var w = 2;
    var r = 4;
    var B = 274277;
    var p = 1461;
    var C = -38;
    var f = JD + j + Math.floor((Math.floor((4 * JD + B) / 146097) * 3) / 4) + C;
    var e = r * f + v;
    var g = Math.floor((e % p) / r);
    var h = u * g + w;
    var M = ((Math.floor(h / s) + m) % n) + 1;
    if (M % 2 == 0 && M != 8) {
      var D = Math.floor((h % s) / u) + 9;

    } else {
      var D = Math.floor((h % s) / u) + 8;

    }
    var Y = Math.floor(e / p) - y + Math.floor((n + m - M) / n);
    return new Date(Y, M, D);
  }

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
    navigation.navigate('LoginContainer')
  };
  const onBack_click = () => {
    navigation.goBack();
  };
  const add_flights = () => {
    navigation.navigate('Add_Flight')
  };

  const detailsPage = (item) => {
    navigation.navigate('MyTripsDetails', { data_selected: item })
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);
  const [is_data, setData_found] = React.useState(false);
  const [data, setData] = React.useState([]);

  const searchFlight = async (request) => {
    setLoading(true);

    let data = new FormData();
    data.append("flight_no", request.flight_no);
    data.append("departure_date", request.departure_date);
    data.append("carrierCode", request.carrierCode);

    console.log('data', JSON.stringify(data))
    await search_flight(data)
      .then((response) => {
        setLoading(false);
        var data_response = response.payload.data;
        console.log("api_login_Response" + JSON.stringify(data_response));
        if (data_response.status == 200) {
          setData(data_response.trips)
          setData_found(true)
        } else {

          setData_found(false)

        }
      }).catch(error => {
        console.log('error' + JSON.stringify(error));
        Alert.alert(error.error.data)
        setLoading(false);
      });
  }

  const searchScanFlight = async (flight_no, departure_date,carrierCode,code_name_from,code_name_to) => {
    setLoading(true);

    let data = new FormData();
    data.append("flight_no", flight_no);
    data.append("departure_date", departure_date);
    data.append("carrierCode", carrierCode);
    data.append("code_name_from", code_name_from);
    data.append("code_name_to", code_name_to);

    console.log('data', JSON.stringify(data))
    await search_flight_scan(data)
      .then((response) => {
        setLoading(false);
        var data_response = response.payload.data;
        console.log("api_login_Response" + JSON.stringify(data_response));
        if (data_response.status == 200) {
          setData(data_response.trips)
          setData_found(true)
        } else {
          alert(data_response.message)

          setData_found(false)

        }
      }).catch(error => {
        console.log('error' + JSON.stringify(error));
        Alert.alert(error.error.data)
        setLoading(false);
      });
  }


  const add_selected_flight = async (data_flight) => {
    setLoading(true);

    console.log("data_flight" + JSON.stringify(data_flight))

    await add_flight(data_flight)
      .then((response) => {
        setLoading(false);
        var data_response = response.payload.data;

        if (data_response.status == '200') {
          navigation.navigate('HomePage')
          alert(data_response.message)

        } else {
          alert(data_response.message)
        }


      }).catch(error => {
        console.log('error' + JSON.stringify(error));
        Alert.alert(error.error.data)
        setLoading(false);
      });
  }

  return (
    <Select_FlightComponent props={props} login_button_pressed={login_button_pressed} onBack_click={onBack_click}
      add_flights={add_flights} detailsPage={detailsPage} isLoading={isLoading} is_data={is_data} data={data}
      add_selected_flight={add_selected_flight} />

  );
};
Select_FlightContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = ({ dashboard, userObj, userSession, myTrips }) => ({
});

const mapDispatchToProps = (dispatch) => ({
  search_flight: (params) => dispatch(Actions.search_flight(params)),
  search_flight_scan: (params) => dispatch(Actions.search_flight_scan(params)),
  add_flight: (params) => dispatch(Actions.add_flight(params))

});


export default connect(mapStateToProps, mapDispatchToProps)(Select_FlightContainer);
