import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, FlatList, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import { colors, fonts, sizes } from '../../utils/theme';
import Spinner from 'react-native-loading-spinner-overlay';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const Select_FlightComponent = memo((props) => {

  const [state, setState] = React.useState({
    index: 0,
    routes: [
      { key: 'first', title: 'Upcoming' },
      { key: 'second', title: 'Past' }
    ]
  });

  // Storing Name & Pass
  const [user, set_user] = useState({
    pass: '',
    confirm_pass: '',
  });

  // const view_all = (item) => {
  //   props.detailsPage(item);
  // }

  const add_flight = () => {
    props.add_flights();
  }

  const add_selected_flight = (item) => {
    
    let data = new FormData();
    data.append("take_of_date_time", item.take_of_date_time);
    data.append("landing_date_time", item.landing_date_time);
    data.append("flight_name", item.flight_name);
    data.append("take_of_time", item.take_of_time);
    data.append("landing_time", item.landing_time);
    data.append("departure_time", item.departure_time);
    data.append("code_name_from", item.code_name_from);
    data.append("code_name_to", item.code_name_to);
    data.append("from_name", item.from_name);
    data.append("to_name", item.to_name);
    data.append("hours_journey", item.hours_journey);
    data.append("check_in_time", item.check_in_time);
    data.append("from_terminal_no", item.from_terminal_no);
    data.append("to_terminal_no", item.to_terminal_no);
    data.append("from_gate_no", item.from_gate_no);
    data.append("to_gate_no", item.to_gate_no);
    data.append("boarding_time", item.boarding_time);
    data.append("user_id", 1);

    props.add_selected_flight(data)
  }



  const renderItemDr = ({ item, index }) => (

    <TouchableOpacity activeOpacity={1.0}>

      <View style={{ flexDirection: 'row', marginRight: 20 }}>
        <View style={{ flex: 0.5 }}>
          <Text style={styles.card_date}>{item.airline_name}</Text>

        </View>

      </View>
      <View style={styles.view_flight_card}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.from_time}>{Language.lang.from}</Text>
          <Text style={styles.to_time}>{Language.lang.to}</Text>
        </View>

        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.From_flight}>{item.code_name_from}</Text>
          <Image source={Images.flight_upcoming} style={styles.logo_icon_flight} />
          <Text style={styles.To_flight}>{item.code_name_to}</Text>
        </View>

        <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20, marginRight: 20 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
            <Image source={Images.coutry1} style={styles.logo_icon_country} />
            <Text style={styles.From_country_name}>{item.from_name}</Text>
          </View>

          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', flex: 1 }}>
            <Image source={Images.coutry1} style={styles.logo_icon_country} />
            <Text style={styles.From_country_name}>{item.to_name}</Text>
          </View>

        </View>

        <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20, marginRight: 20 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1 }}>
            <Text style={styles.From_country_time}>{item.take_of_time}</Text>
          </View>

          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', flex: 1 }}>
            <Text style={styles.From_country_time}>{item.landing_time}</Text>
          </View>

        </View>

        <View style={{ flexDirection: 'row', marginTop: 15, marginBottom: 10, borderWidth: 0, borderColor: colors.main_color_dark, marginLeft: 20, marginRight: 20 }}>
          <View style={{ flexDirection: 'column', flex: 1 }}>
            <Text style={styles.From_country_name}>{Language.lang.flight}</Text>
            <Text style={{ color: colors.main_dark_color, marginLeft: 5, marginTop: 5, fontFamily: fonts.Monsterat_SemiBold }}>{item.flight_name}</Text>
          </View>
        </View>

        <TouchableOpacity onPress={() => add_selected_flight(item)} style={styles.login}>
          <Text style={styles.logintext_button}>Add Flight</Text>
        </TouchableOpacity>

      </View>

    </TouchableOpacity>
  );

  //to check and set focus error field or password  
  const onBack_click = () => {
    props.onBack_click();
  };


  return (
    <ScrollView contentContainerStyle={styles.container}>

      <View style={{
        height: 100,
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
      }}>
        <View style={styles.back_image} />


        <TouchableOpacity onPress={() => onBack_click()} style={{
          marginLeft: 15,
          marginTop: 20
        }}>
          <Image source={Images.back} style={styles.logo_back} />
        </TouchableOpacity>
        <Text style={styles.tab_bar_text}>{Language.lang.select_flight}</Text>

      </View>
      {

        props.is_data == true ?
          <View>
            {/* <Text style={styles.headingtitle}>{Language.lang.upcoming_flight}</Text> */}

            <FlatList
              data={props.data}
              renderItem={renderItemDr}
              keyExtractor={item => item.trip_id}

            />
          </View>

          :
          <View>

            <Text style={styles.headingtitle}>{Language.lang.flight_not_found}</Text>

            <View style={styles.view_flight_card}>
              <View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
                  <Text style={styles.check_in}>{Language.lang.add_manul}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                  <Text style={styles.on_time}>{Language.lang.add_desc}</Text>
                </View>
                <TouchableOpacity onPress={() => add_flight()} style={styles.View_add}>
                  <Text style={styles.logintext_button}>{Language.lang.add_flight}</Text>
                </TouchableOpacity>
              </View>
            </View>

          </View>

      }


      <Spinner
        visible={props.isLoading}
        textContent={'Loading...'}
        color={'white'}
        overlayColor={'rgba(0, 0, 0, 0.45)'}
        textStyle={styles.spinnerTextStyle}
      />

    </ScrollView>

  );
});


export default Select_FlightComponent;
