import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import EditProfileComponent from './EditProfileComponent';
import * as Actions from '../../redux/actions/userSessionActions';
import { Navigation } from '../../utils/theme';
import * as CryptoJS from "crypto-js";
import { store } from '../../redux/actions/store'
import axios from 'axios';
import { Alert, BackHandler, NativeModules, } from 'react-native';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const EditProfileContainer = (props) => {

  const { navigation, clearUserObj, route, alert_action, boarding_action, travel_advice_action, travel_docs_action,
    aircraft_action, from_flight_action, to_flight_action, airlines_flight_action, baggeges_fight_action,
    shopping_fight_action } = props;
  const [isalert, setAlert] = useState(store.getState().userSession.alert);
  const [is_boarding_pass, setBoardingPass] = useState(store.getState().userSession.boarding_pass);
  const [isTravelAdvice, setTravelAdvice] = useState(store.getState().userSession.travel_advice);
  const [isTravelDocs, setTravelDocs] = useState(store.getState().userSession.travel_docs);
  const [isAircraft, setAircraft] = useState(store.getState().userSession.aircraft);
  const [isFrom_flight, setFromFlight] = useState(store.getState().userSession.from_flight);
  const [isTo_flight, setToFlight] = useState(store.getState().userSession.to_flight);
  const [isAirlines, setAirlines] = useState(store.getState().userSession.airlines);
  const [isBaggage, setBaggage] = useState(store.getState().userSession.baggeges);
  const [isShopping, setShopping] = useState(store.getState().userSession.shopping);

  const { isComeFrom } = route;
  if (isComeFrom === 'logout') {
    clearUserObj();
  }

  useEffect(() => {
    setLoading(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }
  const login_button_pressed = () => {
    navigation.navigate('LoginContainer')
  };

  const ScanScreen = () => {
    navigation.navigate('ScanScreen')
  };

  const onBack = () => {
    navigation.goBack();
  };

  const alertAction = () => {
    alert_action()
    setAlert(store.getState().userSession.alert)
    console.log(store.getState().userSession.alert)
  };

  const boarding_Action = () => {
    boarding_action()
    setBoardingPass(store.getState().userSession.boarding_pass)
  };

  const travel_advice_Action = () => {
    travel_advice_action()
    setTravelAdvice(store.getState().userSession.travel_advice)
  };

  const travel_docs_Action = () => {
    travel_docs_action()
    setTravelDocs(store.getState().userSession.travel_docs)
  };

  const aircraft_Action = () => {
    aircraft_action()
    setAircraft(store.getState().userSession.aircraft)
  };

  const from_flight_Action = () => {
    from_flight_action()
    setFromFlight(store.getState().userSession.from_flight)
  };

  const to_flight_Action = () => {
    to_flight_action()
    setToFlight(store.getState().userSession.to_flight)
  };

  const airlines_flight_Action = () => {
    airlines_flight_action()
    setAirlines(store.getState().userSession.airlines)
  };

  const baggeges_fight_Action = () => {
    baggeges_fight_action()
    setBaggage(store.getState().userSession.baggeges)
  };

  const shopping_fight_Action = () => {
    shopping_fight_action()
    setShopping(store.getState().userSession.shopping)
  };

  // Setting 
  const [isLoading, setLoading] = React.useState(false);



  return (
    <EditProfileComponent props={props} login_button_pressed={login_button_pressed} onBack={onBack}
      ScanScreen={ScanScreen} alertAction={alertAction} isalert={isalert} is_boarding_pass={is_boarding_pass} isTravelAdvice={isTravelAdvice}
      isTravelDocs={isTravelDocs} isAircraft={isAircraft} isFrom_flight={isFrom_flight} isTo_flight={isTo_flight}
      isAirlines={isAirlines} isBaggage={isBaggage} isShopping={isShopping} boarding_Action={boarding_Action}
      travel_advice_Action={travel_advice_Action} travel_docs_Action={travel_docs_Action} aircraft_Action={aircraft_Action}
      from_flight_Action={from_flight_Action} to_flight_Action={to_flight_Action} airlines_flight_Action={airlines_flight_Action}
      baggeges_fight_Action={baggeges_fight_Action} shopping_fight_Action={shopping_fight_Action} />
  );
};
EditProfileContainer.navigationOptions = {
  header: null,
};

const mapStateToProps = () => ({
});

const mapDispatchToProps = (dispatch) => ({
  alert_action: () => dispatch(Actions.alert_action()),
  boarding_action: () => dispatch(Actions.boarding_action()),
  travel_advice_action: () => dispatch(Actions.travel_advice_action()),
  travel_docs_action: () => dispatch(Actions.travel_docs_action()),
  aircraft_action: () => dispatch(Actions.aircraft_action()),
  from_flight_action: () => dispatch(Actions.from_flight_action()),
  to_flight_action: () => dispatch(Actions.to_flight_action()),
  airlines_flight_action: () => dispatch(Actions.airlines_flight_action()),
  baggeges_fight_action: () => dispatch(Actions.baggeges_fight_action()),
  shopping_fight_action: () => dispatch(Actions.shopping_fight_action()),
});


export default connect(mapStateToProps, mapDispatchToProps)(EditProfileContainer);
