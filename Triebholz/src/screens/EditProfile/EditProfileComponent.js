import React, { memo, useState, useRef } from 'react';
import { View, StatusBar, Dimensions, Switch, Image, StyleSheet, ActivityIndicator, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import styles from './styles';
import { Images, Language } from '../../utils/theme';
import { colors, fonts, sizes } from '../../utils/theme';
import * as geolib from 'geolib';
import languages from '../../globals/languages';
import FloatingTitleTextInput from "react-native-floating-title-text-input"
// import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import ToggleSwitch from 'toggle-switch-react-native'


const EditProfileComponent = memo((props) => {

  const onBack = () => {
    props.onBack();
  }
  const [filePath, setFilePath] = useState({});
  const [file_name, setName] = useState(null);

  // Storing Name & Pass
  const [user, set_user] = useState({
    username: '',
    email: '',
    dob: '',
    country: '',
    address: '',
    citizenship: '',
    contact_no: '',
    job: '',
  });

  
  const selectFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],

      });
      console.log('res : ' + JSON.stringify(res));
      setName(res.uri);

      setFilePath(res);

    } catch (err) {
      // setSingleFile(null);
      if (DocumentPicker.isCancel(err)) {
      } else {
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  }
  return (
    <ScrollView contentContainerStyle={styles.container}>

      <View style={styles.logo_map} >

        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 30, }}>

          <Text style={styles.logintitle}>{Language.lang.edit_profile}</Text>
          <TouchableOpacity onPress={() => onBack()} style={{
            position: 'absolute',
            right: 0,
            marginTop: 15,
            marginRight: 20
          }}>
            <Image source={Images.close} style={styles.logo_lock} />

          </TouchableOpacity>

        </View>
      </View>




      <View style={{ flexDirection: 'row', marginTop: 20, margin: 20, marginBottom: 40 }}>
        <View style={{ flexDirection: 'row' }}>
          <Image source={file_name == null ? Images.profile_pic : {uri : file_name}} style={styles.logo_icon_country} />
          <TouchableOpacity onPress={() => selectFile()} style={{
            width: 60,
            height: 60,
            position: 'absolute',
            bottom: 0,
            right: 0,
            marginBottom: -20
          }}>

            <Image source={Images.add_image} style={styles.add_image} />

          </TouchableOpacity>
        </View>

        <View style={{ width: 1, height: 100, marginLeft: 40, backgroundColor: colors.view_color }} />
        <Text style={styles.name_sub}>{languages.lang.edit_profile}</Text>

      </View>


      <View style={styles.view_flight_card}>

        <FloatingTitleTextInput
          placeholder={"Full name"}
          onChangeText={(value) => set_user({ ...user, ['username']: value })}
          onFocus={() => console.log("textInput in focus")}
          onBlur={(value) => console.log(value)}
          titleStyle={{ marginLeft: 3 }}
          placeholderTextColor="#000000"
          style={{ padding: 10, }}
          secureTextEntry={false}
        />

        <FloatingTitleTextInput
          placeholder={"Email address"}
          onChangeText={(value) => set_user({ ...user, ['email']: value })}
          onFocus={() => console.log("textInput in focus")}
          onBlur={(value) => console.log(value)}
          titleStyle={{ marginLeft: 3 }}
          placeholderTextColor="#000000"
          style={{ padding: 10, }}
          secureTextEntry={false}
        />

        <FloatingTitleTextInput
          placeholder={"Date of birth"}
          onChangeText={(value) => set_user({ ...user, ['dob']: value })}
          onFocus={() => console.log("textInput in focus")}
          onBlur={(value) => console.log(value)}
          titleStyle={{ marginLeft: 3 }}
          placeholderTextColor="#000000"
          style={{ padding: 10, }}
          secureTextEntry={false}
        />

        <FloatingTitleTextInput
          placeholder={"Home country"}
          onChangeText={(value) => set_user({ ...user, ['country']: value })}
          onFocus={() => console.log("textInput in focus")}
          onBlur={(value) => console.log(value)}
          titleStyle={{ marginLeft: 3 }}
          placeholderTextColor="#000000"
          style={{ padding: 10, }}
          secureTextEntry={false}
        />

        <FloatingTitleTextInput
          placeholder={"Address"}
          onChangeText={(value) => set_user({ ...user, ['address']: value })}
          onFocus={() => console.log("textInput in focus")}
          onBlur={(value) => console.log(value)}
          titleStyle={{ marginLeft: 3 }}
          placeholderTextColor="#000000"
          style={{ padding: 10, }}
          secureTextEntry={false}
        />

        <FloatingTitleTextInput
          placeholder={"Citizenship"}
          onChangeText={(value) => set_user({ ...user, ['citizenship']: value })}
          onFocus={() => console.log("textInput in focus")}
          onBlur={(value) => console.log(value)}
          titleStyle={{ marginLeft: 3 }}
          placeholderTextColor="#000000"
          style={{ padding: 10, }}
          secureTextEntry={false}
        />

        <FloatingTitleTextInput
          placeholder={"Contact No"}
          onChangeText={(value) => set_user({ ...user, ['contact_no']: value })}
          onFocus={() => console.log("textInput in focus")}
          onBlur={(value) => console.log(value)}
          titleStyle={{ marginLeft: 3 }}
          placeholderTextColor="#000000"
          style={{ padding: 10, }}
          secureTextEntry={false}
        />

        <FloatingTitleTextInput
          placeholder={"Job"}
          onChangeText={(value) => set_user({ ...user, ['job']: value })}
          onFocus={() => console.log("textInput in focus")}
          onBlur={(value) => console.log(value)}
          titleStyle={{ marginLeft: 3 }}
          placeholderTextColor="#000000"
          style={{ padding: 10, }}
          secureTextEntry={false}
        />


      </View>

      <View style={{ flexDirection: 'row', justifyContent: 'flex-start', flex: 1, marginTop: 20 }}>

        <TouchableOpacity style={styles.View_add}>
          <Text style={styles.logintext_button}>{Language.lang.update}</Text>
        </TouchableOpacity>

      </View>

    </ScrollView>

  );
});


export default EditProfileComponent;
