export const createTypes = (namespace, type) => [
  `${namespace}/${type}_REQUEST`,
  `${namespace}/${type}_SUCCESS`,
  `${namespace}/${type}_FAIL`,
];

const userSessionNameSpace = 'userSession';
const userSessionNameSpace1 = 'userSession1';
const userSessionNameSpace2 = 'userSession2';
const userSessionNameSpace3 = 'userSession3';
const userSessionNameSpace4 = 'userSession4';
const userSessionNameSpace5 = 'userSession5';
const userSessionNameSpace6 = 'userSession6';
const userSessionNameSpace7 = 'userSession7';
const userSessionNameSpace8 = 'userSession8';
const userSessionNameSpace9 = 'userSession9';
const userSessionNameSpace10 = 'userSession10';
const userSessionNameSpace11 = 'userSession11';
const userSessionNameSpace12 = 'userSession12';
const userSessionNameSpace13 = 'userSession13';
const userSessionNameSpace14 = 'userSession14';
const userSessionNameSpace15 = 'userSession15';
const userSessionNameSpace16 = 'userSession16';
const userSessionNameSpace17 = 'userSession17';
const userSessionNameSpace18 = 'userSession18';
const userSessionNameSpace19 = 'userSession19';
const userSessionNameSpace20 = 'userSession20';
const userSessionNameSpace21 = 'userSession21';

export const SET_USER_SESSION = 'SET_USER_SESSION';
export const CLEAR_USER_SESSION = 'CLEAR_USER_SESSION';
export const USER_LOGIN = createTypes(userSessionNameSpace, 'USER_LOGIN');
export const USER_SIGNUP = createTypes(userSessionNameSpace1, 'USER_SIGNUP');
export const USER_ROLE = createTypes(userSessionNameSpace2, 'USER_ROLE');
export const MY_TRIPS = createTypes(userSessionNameSpace13, 'MY_TRIPS');
export const GET_PROFILE = createTypes(userSessionNameSpace18, 'GET_PROFILE');
export const LATEST_TRIPS = createTypes(userSessionNameSpace14, 'LATEST_TRIPS');
export const ADD_FLIGHT = createTypes(userSessionNameSpace15, 'ADD_FLIGHT');
export const AIRLINES_GET = createTypes(userSessionNameSpace16, 'AIRLINES_GET');
export const SEARCH_FLIGHT = createTypes(userSessionNameSpace17, 'SEARCH_FLIGHT');
export const SEARCH_SCAN_FLIGHT = createTypes(userSessionNameSpace21, 'SEARCH_SCAN_FLIGHT');
export const SCANNER_DATA = createTypes(userSessionNameSpace20, 'SCANNER_DATA');
export const SCANNER_DATA_ERASE = createTypes(userSessionNameSpace19, 'SCANNER_DATA_ERASE');


export const ALERT = createTypes(userSessionNameSpace3, 'ALERT');
export const BOARDING_PASS = createTypes(userSessionNameSpace4, 'BOARDING_PASS');
export const TRAVELADVICE = createTypes(userSessionNameSpace5, 'TRAVELADVICE');
export const TRAVELDOCS = createTypes(userSessionNameSpace6, 'TRAVELDOCS');
export const AIRCRAFT = createTypes(userSessionNameSpace7, 'AIRCRAFT');
export const FROM_FLIGHT = createTypes(userSessionNameSpace8, 'FROM_FLIGHT');
export const TO_FLIGHT = createTypes(userSessionNameSpace9, 'TO_FLIGHT');
export const AIRLINES = createTypes(userSessionNameSpace10, 'AIRLINES');
export const BAGGEGES = createTypes(userSessionNameSpace11, 'BAGGEGES');
export const SHOPPING = createTypes(userSessionNameSpace12, 'SHOPPING');

export const SET_USER_OBJ = 'SET_USER_OBJ';
export const CLEAR_USER_OBJ = 'CLEAR_USER_OBJ';

