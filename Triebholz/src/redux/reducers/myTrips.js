import { MY_TRIPS } from '../types';

const initialData = {
  myTrips: null,
};

export default (state = initialData, action) => {
 switch (action.type) {
   case MY_TRIPS[0]:
     return {
       ...state,
     };
   case MY_TRIPS[1]:
     return {
       ...state,
       myTrips: action?.payload?.data,
     };
   case MY_TRIPS[2]:
     return {
       ...state,
       myTrips: {},
     };
   default:
     return state;
 }
};