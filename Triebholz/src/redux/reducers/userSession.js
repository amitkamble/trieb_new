
import { USER_LOGIN, USER_SIGNUP, SET_USER_SESSION, CLEAR_USER_SESSION, USER_ROLE,ALERT, BOARDING_PASS, TRAVELADVICE,
   TRAVELDOCS, AIRCRAFT, FROM_FLIGHT, AIRLINES, BAGGEGES, SHOPPING,LATEST_TRIPS, TO_FLIGHT,MY_TRIPS,
   ADD_FLIGHT,AIRLINES_GET,SEARCH_FLIGHT,GET_PROFILE , SCANNER_DATA, SCANNER_DATA_ERASE,SEARCH_SCAN_FLIGHT} from '../types';

const initialData = {
  isLoginSuccess: false,
  signupObj: {
    isProgress: false,
    status: null,
    response: null,
  },
  isUserLoggedin: false,
  alert: true,
  boarding_pass: true,
  travel_advice: true,
  travel_docs: true,
  aircraft: true,
  from_flight: true,
  to_flight: true,
  airlines: true,
  baggeges: true,
  shopping: true,
  selected_data: null,
};

export default (state = initialData, action) => {

  switch (action.type) {

    case ALERT:
      return {
        ...state,
        alert: !state.alert
      };

    case SCANNER_DATA:
      return {
        ...state,
        selected_data: action.payload
      };

    case SCANNER_DATA_ERASE:
      return {
        ...state,
        selected_data: null
      };

    case BOARDING_PASS:
      return {
        ...state,
        boarding_pass: !state.boarding_pass
      };

    case TRAVELADVICE:
      return {
        ...state,
        travel_advice: !state.travel_advice
      };

    case TRAVELDOCS:
      return {
        ...state,
        travel_docs: !state.travel_docs
      };

    case AIRCRAFT:
      return {
        ...state,
        aircraft: !state.aircraft
      };

    case FROM_FLIGHT:
      return {
        ...state,
        from_flight: !state.from_flight
      };

    case TO_FLIGHT:
      return {
        ...state,
        to_flight: !state.to_flight
      };

    case AIRLINES:
      return {
        ...state,
        airlines: !state.airlines
      };

    case BAGGEGES:
      return {
        ...state,
        baggeges: !state.baggeges
      };

    case SHOPPING:
      return {
        ...state,
        shopping: !state.shopping
      };

    case SET_USER_SESSION:
      return {
        ...state,
        isUserLoggedin: true,
      };
    case CLEAR_USER_SESSION:
      return {
        ...state,
        isUserLoggedin: false,
      };

    case USER_LOGIN[0]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: true,
          status: 'initiated',
        },
      };
    case USER_LOGIN[1]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'success',
          response: action?.payload?.data,
        },
        // token: action?.payload?.data?.data.token,

      };
    case USER_LOGIN[2]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'failure',
          response: null,
        },
      };

    case MY_TRIPS[0]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: true,
          status: 'initiated',
        },
      };
    case MY_TRIPS[1]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'success',
          response: action?.payload?.upcoming,
        },

      };
    case MY_TRIPS[2]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'failure',
          response: null,
        },
      };

    case ADD_FLIGHT[0]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: true,
          status: 'initiated',
        },
      };
    case ADD_FLIGHT[1]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'success',
          response: action?.payload?.upcoming,
        },

      };
    case ADD_FLIGHT[2]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'failure',
          response: null,
        },
      };

    case LATEST_TRIPS[0]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: true,
          status: 'initiated',
        },
      };
    case LATEST_TRIPS[1]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'success',
          response: action?.payload?.upcoming,
        },

      };
    case LATEST_TRIPS[2]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'failure',
          response: null,
        },
      };

    case USER_SIGNUP[0]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: true,
          status: 'initiated',
        },
      };
    case USER_SIGNUP[1]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'success',
          response: action?.payload?.data,
        },
      };
    case USER_SIGNUP[2]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'failure',
          response: null,
        },
      };

    case AIRLINES_GET[0]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: true,
          status: 'initiated',
        },
      };
    case AIRLINES_GET[1]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'success',
          response: action?.payload?.data,
        },
      };
    case AIRLINES_GET[2]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'failure',
          response: null,
        },
      };

    case GET_PROFILE[0]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: true,
          status: 'initiated',
        },
      };
    case GET_PROFILE[1]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'success',
          response: action?.payload?.data,
        },
      };
    case GET_PROFILE[2]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'failure',
          response: null,
        },
      };


    case USER_ROLE[0]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: true,
          status: 'initiated',
        },
      };
    case USER_ROLE[1]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'success',
          response: action?.payload?.data,
        },
      };
    case USER_ROLE[2]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'failure',
          response: null,
        },
      };

    case SEARCH_FLIGHT[0]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: true,
          status: 'initiated',
        },
      };
    case SEARCH_FLIGHT[1]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'success',
          response: action?.payload?.data,
        },
      };
      
    case SEARCH_FLIGHT[2]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'failure',
          response: null,
        },
      };

    case SEARCH_SCAN_FLIGHT[0]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: true,
          status: 'initiated',
        },
      };
    case SEARCH_SCAN_FLIGHT[1]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'success',
          response: action?.payload?.data,
        },
      };
      
    case SEARCH_SCAN_FLIGHT[2]:
      return {
        ...state,
        signupObj: {
          ...state.signupObj,
          isProgress: false,
          status: 'failure',
          response: null,
        },
      };

    default:
      return state;
  }
};
