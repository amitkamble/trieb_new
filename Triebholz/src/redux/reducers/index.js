import {combineReducers} from 'redux';
import userSession from './userSession';
import userObj from './userObj'
import myTrips from './myTrips'

export const persistWhitelist = ['userSession','userObj','myTrips'];

export default combineReducers({
  userSession,
  userObj,
  myTrips,
});
