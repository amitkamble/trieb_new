import { Value } from 'react-native-reanimated';
import { API } from '../../utils/theme';

import { USER_LOGIN, SET_USER_SESSION, CLEAR_USER_SESSION, SET_USER_OBJ, CLEAR_USER_OBJ, USER_SIGNUP,
  USER_ROLE,ALERT, BOARDING_PASS, TRAVELDOCS, TRAVELADVICE, AIRCRAFT, FROM_FLIGHT, TO_FLIGHT, AIRLINES
  , BAGGEGES, SHOPPING, MY_TRIPS,LATEST_TRIPS,ADD_FLIGHT,AIRLINES_GET,SEARCH_FLIGHT,GET_PROFILE,
  SCANNER_DATA_ERASE, SCANNER_DATA,SEARCH_SCAN_FLIGHT } from '../types';

// check login
export const userlogin = (params) => ({
  types: USER_LOGIN,
  payload: {
    client: 'default',
    request: {
      url: API.baseUrl + 'login',
      method: 'post',
      data: params
    },
  },
});

// social login
export const socialLogin = (params) => ({
  types: USER_LOGIN,
  payload: {
    client: 'default',
    request: {
      url: API.baseUrl + 'social-login',
      method: 'post',
      data: params
    },
  },
});

// social login
export const my_trips = (params) => ({
  types: MY_TRIPS,
  payload: {
    client: 'default',
    request: {
      url: API.baseUrl + 'trips',
      method: 'post',
      data: params
    },
  },
});

// social login
export const get_profile_data = (params) => ({
  types: GET_PROFILE,
  payload: {
    client: 'default',
    request: {
      url: API.baseUrl + 'my-profile',
      method: 'post',
      data: params
    },
  },
});

export const latest_trip = (params) => ({
  types: LATEST_TRIPS,
  payload: {
    client: 'default',
    request: {
      url: API.baseUrl + 'latest-trip',
      method: 'post',
      data: params
    },
  },
});

export const add_flight = (params) => ({
  types: ADD_FLIGHT,
  payload: {
    client: 'default',
    request: {
      url: API.baseUrl + 'add-flight',
      method: 'post',
      data: params
    },
  },
});

// check register
export const userSignup = (params) => ({
  types: USER_SIGNUP,
  payload: {
    client: 'default',
    request: {
      url: API.baseUrl + 'register',
      method: 'post',
      data: {
        params,
      }
    },
  },
});

// check register
export const search_flight = (params) => ({
  types: SEARCH_FLIGHT,
  payload: {
    client: 'default',
    request: {
      url: API.baseUrl + 'search-flight',
      method: 'post',
      data: params
    },
  },
});

// check register
export const search_flight_scan = (params) => ({
  types: SEARCH_SCAN_FLIGHT,
  payload: {
    client: 'default',
    request: {
      url: API.baseUrl + 'scan-boarding-pass',
      method: 'post',
      data: params
    },
  },
});

// check register
export const userrole = () => ({
  types: USER_ROLE,
  payload: {
    client: 'default',
    request: {
      url: API.baseUrl + 'roles',
      method: 'get',
    },
  },
});

export const getAirlines = () => ({
  types: AIRLINES_GET,
  payload: {
    client: 'default',
    request: {
      url: API.baseUrl + 'airlines',
      method: 'get',
    },
  },
});

export const alert_action = () => ({
  type: ALERT,
})

export const boarding_action = () => ({
  type: BOARDING_PASS,
})

export const travel_advice_action = () => ({
  type: TRAVELADVICE,
})

export const travel_docs_action = () => ({
  type: TRAVELDOCS,
})

export const aircraft_action = () => ({
  type: AIRCRAFT,
})

export const from_flight_action = () => ({
  type: FROM_FLIGHT,
})

export const to_flight_action = () => ({
  type: TO_FLIGHT,
})

export const airlines_flight_action = () => ({
  type: AIRLINES,
})

export const baggeges_fight_action = () => ({
  type: BAGGEGES,
})

export const shopping_fight_action = () => ({
  type: SHOPPING,
})

export const setUserObj = () => ({
  type: SET_USER_OBJ,
});

export const clearUserObj = () => ({
  type: CLEAR_USER_OBJ,
});

export const setUserSession = () => ({
  type: SET_USER_SESSION,
});

export const clearUserSession = () => ({
  type: CLEAR_USER_SESSION,
})

export const selected_data = (value) => ({
  type: SCANNER_DATA,
  payload : value
})

export const clear_data = () => ({
  type: SCANNER_DATA_ERASE,
})
