/**
 * @author Alexis Valenciano <gabr.933@gmail.com>
 */
import React from "react";
import { View, Text, TouchableOpacity, StyleSheet, Image } from "react-native";
import { Images ,colors,fonts,sizes} from '../../utils/theme'
const Chips = (props) => {
    const { value, onPress, chipStyle, type, selected, chipCloseStyle, valueStyleSelected, chipStyleSelected, valueStyle } = props;
    const returnStyles = () => {
        if (type == 'removable') {
            return removableStyles
        }
        return selectableStyles

    }
    const returnRemovable = () => {
        if (type == 'removable') {
            return (
                <TouchableOpacity onPress={onPress}>
                    <Image source={Images.Cancel} style={[returnStyles().chipCloseBtnTxt, chipCloseStyle]} />
                </TouchableOpacity>
            )
        }
    }
    return (
        <View>
            {
                type == 'removable' ?
                    // <TouchableOpacity onPress={onPress}>
                        <View style={selected ? [{ flexDirection: 'row' }, returnStyles().chipSelected, chipStyle, chipStyleSelected] : [{ flexDirection: 'row' }, returnStyles().chip, chipStyle]}>
                            <Text style={selected ? [{ paddingHorizontal: 5 }, returnStyles().valueStyleSelected, valueStyle, valueStyleSelected] : [{ paddingHorizontal: 5 }, returnStyles().valueStyle, valueStyle]}>{value}</Text>
                            {returnRemovable()}
                        </View>
                    // </TouchableOpacity> 
                    :
                    <TouchableOpacity onPress={onPress}>
                        <View style={selected ? [{ flexDirection: 'row' }, returnStyles().chipSelected, chipStyle, chipStyleSelected] : [{ flexDirection: 'row' }, returnStyles().chip, chipStyle]}>
                            <Text style={selected ? [{ paddingHorizontal: 5 }, returnStyles().valueStyleSelected, valueStyle, valueStyleSelected] : [{ paddingHorizontal: 5 }, returnStyles().valueStyle, valueStyle]}>{value}</Text>
                            {returnRemovable()}
                        </View>
                    </TouchableOpacity>

            }

        </View>

    )
}

const removableStyles = StyleSheet.create({
    chip: {
        backgroundColor: '#2196F3',
        borderColor: '#2196F3',
        borderWidth: 1,
        margin: 5,
        padding: 6,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    chipCloseBtnTxt: {
        fontSize: 20,
        color: '#FFF'
    },
    valueStyle: {
        color: '#FFF',
        fontSize: sizes.regularLarge,
        fontFamily:fonts.Regular
    }
})
const selectableStyles = StyleSheet.create({
    chip: {
        backgroundColor: '#FFF',
        borderColor: colors.sortButton,
        borderWidth: 1,
        margin: 5,
        padding: 6,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    valueStyle: {
        color: colors.sortButton,
        fontSize: sizes.regularLarge,
        fontFamily:fonts.Regular
    },
    chipSelected: {
        backgroundColor: colors.main,
        borderColor: colors.main,
        borderWidth: 1,
        margin: 5,
        padding: 6,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    valueStyleSelected: {
        color: '#FFF',
        fontSize: sizes.regularLarge,
        fontFamily:fonts.Regular
    }
})
export default Chips;

