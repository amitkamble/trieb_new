import { StyleSheet } from 'react-native';
import { colors, fonts, sizes, constants } from '../../utils/theme';
export default StyleSheet.create({

  
 
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  
  safeAreaContainer: {
    flex: 1,
    backgroundColor: colors.white
  },
 
  searchTextInput: {
    fontSize: sizes.regularLarge,
    color: colors.black,
    fontFamily: fonts.Regular,
    textAlign: 'left',
    flex:1,
    backgroundColor:colors.white,
    marginLeft:10,
    marginRight:10,
    marginTop:7,
    marginBottom:7,
    paddingTop:5,
    paddingBottom:5,
    paddingLeft:10,
    paddingRight:10,
    borderRadius:5,
    height:40
  },
  searchTextBackground: {
    flexDirection:'row',
    borderBottomColor:colors.colorF5F4F7,
    borderTopColor:colors.colorF5F4F7,
    backgroundColor: colors.colorF5F4F7,
    
  },

  searchTextCancelContainer: {
   justifyContent: 'center' 
  },
  searchTextCancel: {
    marginRight: 15,
    fontFamily: fonts.Medium,
    fontSize: sizes.medium,
    color: colors.black,
  },
  listText: {
    textAlign: 'left',
    fontFamily: fonts.Regular,
    fontSize: sizes.medium,
    color: colors.black,
    marginLeft:15,
    marginRight:15,
    marginBottom:12,
    paddingTop:5,
    paddingRight:5
  },
  listTextBack: {
    marginTop: 5,
    marginBottom: 5
  },
  seperator: {
    height: 1,
    backgroundColor:colors.bottomBorder,
   
  },


});
