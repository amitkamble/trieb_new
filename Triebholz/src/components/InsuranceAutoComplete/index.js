import React, { memo, useState } from 'react';
import { TextInput, FlatList, View, Text, SafeAreaView, TouchableOpacity } from 'react-native';
import styles from './styles';
import API from '../../utils/API';
import axios from 'axios';
import NetInfo from "@react-native-community/netinfo";
import { ValidationMessages } from '../../utils/validators';
const SearchAutoComplete = memo((props) => {


    const [seatch, setSearch] = useState("");
    const [state, setState] = useState([]);
    const searchText = async (search) => {
        if(search==""){
            setState([])
            return
        }
        console.log(`${API.search_insurance_auto}${search}`)
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                
                axios.get(`${API.search_insurance_auto}${search}`)
                    .then(response => {
                        console.log(response.data.Insurance);
                        setState(response.data.Insurance)
                        

                    })
                    .catch(error => {
                        
                        setTimeout(() => {
                            console.log(error)
                            ShowAlert(error.message)
                        }, 100);

                    });
            } else {
               
                setTimeout(() => {
                    ShowAlert(ValidationMessages.checkInterneMessage)
                }, 100);
            }
        });
        
    };

    const itemSelect = (item) => {
        props.searchResult(item)
    }
    const renderItemList = ({ item }) => (
        <TouchableOpacity onPress={() =>
            itemSelect(item)
        }>
            <View style={styles.listTextBack}>
                <Text style={styles.listText}>{item.name}</Text>
                <View style={styles.seperator}></View>
            </View>
        </TouchableOpacity>
    );

    return (
        <View style={styles.container}>
            <SafeAreaView style={[styles.safeAreaContainer]}>
                <View style={styles.searchTextBackground}>
                    <TextInput
                        multiline={false}
                        autoFocus={true}
                        autoCorrect={false}
                        returnKeyType='search'
                        placeholder="Search Insurance Providor"
                        autoCapitalize="none"
                        value={seatch}
                        onChangeText={(email) => {
                            searchText(email)
                            setSearch(email)
                        }}
                        keyboardType='default'
                        style={styles.searchTextInput}
                        onSubmitEditing={() => {  itemSelect({name:seatch}) }}
                    />
                    <View style={styles.searchTextCancelContainer}><Text style={styles.searchTextCancel} onPress={() => {
                        props.searchCancel()

                    }}>Cancel</Text></View>
                </View>
                <FlatList
                    data={state}
                    showsHorizontalScrollIndicator={false}
                    horizontal={false}
                    renderItem={renderItemList}
                    keyExtractor={item => item.id.toString()}
                />
            </SafeAreaView>
        </View>
    );
});

export default SearchAutoComplete;

