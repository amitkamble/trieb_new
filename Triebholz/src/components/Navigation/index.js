import React from 'react';
import { NavgationStyles, Images, colors, fonts } from '../../utils/theme'
import { Image, View, Text, TouchableOpacity, Platform } from 'react-native';

// import TextTicker from 'react-native-text-ticker'

function LogoTitle(navigation) {
    return (
        <View style={NavgationStyles.centerView}>
            <TouchableOpacity onPress={() => navigation.locationButtonPress()}>
                <View style={NavgationStyles.centerView}>
                    {/* <TextTicker
                        style={NavgationStyles.text}
                        duration={10000}
                        loop
                        bounce
                        repeatSpacer={10}
                        marqueeDelay={2000}
                    >
                        {navigation.titleString ? navigation.titleString : ""}
                    </TextTicker> */}
                    {/* <Text style={NavgationStyles.text}>{navigation.titleString ? navigation.titleString : ""}</Text> */}
                    {/* <Image
                        source={Images.Icon}
                        style={NavgationStyles.dropDown} /> */}
                </View>
            </TouchableOpacity>
        </View >

    );
}
function LogoTitle1(navigation) {
    return (
        <View style={NavgationStyles.centerView}>
            <Text style={NavgationStyles.text}>{navigation.titleString ? navigation.titleString : ""}</Text>
        </View>
    );
}

function LogoTitle2(navigation) {
    return (
        <View style={NavgationStyles.search_title_View}>
            <Text style={NavgationStyles.textWhite_Title}>Search Mutual Fund</Text>
        </View>
    );
}

function LogoTitle3(navigation) {
    return (
        <View style={NavgationStyles.search_title_View}>
            <Text style={NavgationStyles.textWhite_Title}>{navigation.titleString}</Text>
        </View>
    );
}


function LogoTitleGoalPlanner(navigation) {
    return (
        <View style={NavgationStyles.search_title_View_goal}>
            <Text style={NavgationStyles.textWhite_Title}>{navigation.titleString}</Text>
            <Text style={{ fontFamily: fonts.Regular, fontSize: 10, color: colors.white }}>{navigation.sub_titleString}</Text>

        </View>
    );
}


function LogoTitle_black(navigation) {
    return (
        <View style={NavgationStyles.search_title_View}>
            <Text style={NavgationStyles.textblack_Title}>{navigation.titleString}</Text>
        </View>
    );
}


function LogoTitleWhite(navigation) {
    return (
        <View style={NavgationStyles.centerView}>
            <Text style={NavgationStyles.textWhite}>{navigation.titleString ? navigation.titleString : ""}</Text>
        </View>
    );
}


function LogoTitleBlack(navigation) {
    return (
        <View style={NavgationStyles.filterViewTitle}>
            <Text style={NavgationStyles.textBlack}>{navigation.titleString ? navigation.titleString : ""}</Text>
        </View>
    );
}


function LogoTitle_Home() {
    return (
        <View style={NavgationStyles.centerView}>
            <Image
                source={Images.logo}
                style={NavgationStyles.logo_center} />
        </View>
    );
}

function Logo_notification() {
    return (
        <View style={NavgationStyles.centerView}>
            <Image
                source={Images.notification}
                style={NavgationStyles.notification_Button} />
        </View>
    );
}

///////////////////////////////////////////////////////////////////////////////
const setNavigationSearchTab = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitle2 {...navigation} />,
        headerRight: () => (
            <TouchableOpacity onPress={() => navigation.notificationButtonPress()} style={NavgationStyles.rightView}>
                <Image
                    source={Images.notification}
                    style={NavgationStyles.menuButton} />
            </TouchableOpacity>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back_icon}
                    style={NavgationStyles.menuButton} />
            </TouchableOpacity>
        ),
        headerTransparent: false,
        headerShown: true,

    })
}


const setNavigationSelectedGoal = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitleGoalPlanner {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>
                {/* <TouchableOpacity onPress={() => navigation.sortButtonPress()}>
                    <Text style={NavgationStyles.sortText}>{"Sort"}</Text>
                </TouchableOpacity>
                <Image
                    source={Images.drop_down_search}
                    style={NavgationStyles.dropdown_search} />

                <View style={NavgationStyles.separator}></View>
                <TouchableOpacity onPress={() => navigation.filterButtonPress()}>
                    <Image
                        source={Images.filter}
                        style={NavgationStyles.filter} />
                </TouchableOpacity> */}
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back_white}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerTransparent: true,
        headerShown: true,

    })
}


const setWatchListTab = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitle3 {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>

                <TouchableOpacity onPress={() => navigation.notification_pressed()}>
                    <Image
                        source={Images.notification}
                        style={NavgationStyles.filter} />
                </TouchableOpacity>
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.menu_pressed()}>
                <Image
                    source={Images.menu}
                    style={NavgationStyles.menuButton} />
            </TouchableOpacity>
        ),
        headerTransparent: true,
        headerShown: true,

    })
}

const setNavigationOrder = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitle3 {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>

                <TouchableOpacity onPress={() => navigation.NotificationButtonPress()}>
                    <Image
                        source={Images.notification}
                        style={NavgationStyles.filter} />
                </TouchableOpacity>
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back_white}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerTransparent: true,
        headerShown: true,

    })
}

const setNavigationPortfolio = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitle3 {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>

                <TouchableOpacity onPress={() => navigation.NotificationButtonPress()}>
                    <Image
                        source={Images.notification}
                        style={NavgationStyles.filter} />
                </TouchableOpacity>
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.menuButtonPress()}>
                <Image
                    source={Images.menu}
                    style={NavgationStyles.menuButton} />
            </TouchableOpacity>
        ),
        headerTransparent: true,
        headerShown: true,

    })
}


const setNavigationGoalPlanner = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitle3 {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>

                {/* <TouchableOpacity onPress={() => navigation.filterButtonPress()}>
                    <Image
                        source={Images.notification}
                        style={NavgationStyles.filter} />
                </TouchableOpacity> */}
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back_white}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerTransparent: true,
        headerShown: true,

    })
}



const setNavigationDetails = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitle3 />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>

                <TouchableOpacity onPress={() => navigation.filterButtonPress()}>
                    <Image
                        source={Images.addto_watchlist}
                        style={NavgationStyles.watchlist} />
                </TouchableOpacity>
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerTransparent: true,
        headerShown: true,

    })
}
///////////////////////////////////////////////////////////////////////////////////
const setNavigation = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitle2 />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>
                <TouchableOpacity onPress={() => navigation.filterButtonPress()}>
                    <Image
                        source={Images.notification}
                        style={NavgationStyles.notification_Button} />
                </TouchableOpacity>
            </View>),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerTransparent: true,
        headerShown: true,

    })
}



//////////////////////////////////////////////////////////////////////////////
const setNavigationColor = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitleBlack {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>
                <TouchableOpacity onPress={() => navigation.resetButtonPress()}>
                    <Text style={NavgationStyles.resetText}>{"RESET"}</Text>
                </TouchableOpacity>
            </View>
        ),
        headerLeft: () => (

            <View style={NavgationStyles.rightView}>
                <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                    <Image
                        source={Images.back}
                        style={NavgationStyles.backButton} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.filterButtonPress()}>
                    <Image
                        source={Images.logo_filter}
                        style={NavgationStyles.logo_filter} />
                </TouchableOpacity>

            </View>

        ),

        headerStyle: {
            backgroundColor: colors.white,
        },
        headerTintColor: colors.white,
        headerShown: true,

    })
}
///////////////////////////////////////////////////////////////////////////////////////
const setNavigationColorMenu = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitleWhite />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>
                <TouchableOpacity onPress={() => navigation.filterButtonPress()}>
                    <Image
                        source={Images.notification}
                        style={NavgationStyles.notification_Button} />
                </TouchableOpacity>
            </View>),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.menuWhite}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),

        headerStyle: {
            backgroundColor: colors.main,
        },
        headerTintColor: colors.white,
        headerShown: true,

    })
}
const setNavigationTitle = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitle1 {...navigation} />,
        headerRight: () => (
            <View></View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerTransparent: false,
        headerShown: true,
    })
}


const setNavigationTitleWhite = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitle1 {...navigation} />,
        headerRight: () => (
            <View></View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back_white}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerTransparent: true,
        headerShown: true,
    })
}


const setNavigationTitleAndBack = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitle_black {...navigation} />,
        headerRight: () => (
            <View></View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerStyle: {
            backgroundColor: colors.white,
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
        },
        headerTransparent: false,
        headerTintColor: colors.white,
        headerShown: true,
    })
}
///////////////////////////////////////////////////////////////////////////////////////////////
const setNavigationTitleMenu = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitleWhite {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>
                <TouchableOpacity onPress={() => navigation.onNotificationPress()}>
                    <Image
                        source={Images.notification}
                        style={NavgationStyles.notification_Button} />
                </TouchableOpacity>
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.nav_menu}
                    style={NavgationStyles.menuButton} />
            </TouchableOpacity>
        ),
        headerTransparent: true,
        headerShown: true,
    })
}
///////////////////////////////////////////////////////////////////////////////////////////////
const setNavigationTitleBack = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitleWhite {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>
                <TouchableOpacity style={{ marginTop: Platform.OS == 'ios' ? 10 : 15 }} onPress={() => navigation.onNotificationPress()}>
                    <Image
                        source={Images.notification}
                        style={NavgationStyles.notification_Button} />
                </TouchableOpacity>

                <TouchableOpacity style={{ marginTop: Platform.OS == 'ios' ? 10 : 15 }} onPress={() => navigation.onCalenderPress()}>
                    <Image
                        source={Images.calender}
                        style={NavgationStyles.calender_Button} />
                </TouchableOpacity>
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity style={{marginTop:5}} onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back_icon}
                    style={NavgationStyles.menuButton} />
            </TouchableOpacity>
        ),
        headerTransparent: true,
        headerShown: true,
    })
}

const setNavigationMenu = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <View />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>
                <TouchableOpacity style={{ marginTop: Platform.OS == 'ios' ? 10 : 15 }} onPress={() => navigation.onNotificationPress()}>
                    <Image
                        source={Images.notification}
                        style={NavgationStyles.notification_Button} />
                </TouchableOpacity>

                <TouchableOpacity style={{ marginTop: Platform.OS == 'ios' ? 10 : 15 }} onPress={() => navigation.onCalenderPress()}>
                    <Image
                        source={Images.calender}
                        style={NavgationStyles.calender_Button} />
                </TouchableOpacity>
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity style={{ marginTop: Platform.OS == 'ios' ? 15 : 20 }} onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.nav_menu}
                    style={NavgationStyles.menuButton} />
            </TouchableOpacity>
        ),
        headerTransparent: true,
        headerShown: true,
    })
}
////////////////////////////////////////////////////////////////////////////////////////////
const Icon = (image) => {
    return (
        <Image
            source={image}
            style={NavgationStyles.bottomTab} />
    );
}


const setNavigationAppointmentDetails = (navigation) => {


    navigation.setOptions({
        tabBarVisible: true,
        headerTitle: props => <LogoTitle1 {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>
                <TouchableOpacity onPress={() => navigation.calButtonPress()}>
                    <Image
                        source={Images.cal}
                        style={NavgationStyles.cal} />
                </TouchableOpacity>
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerTransparent: false,
        headerShown: true,


    })
}

const setNavigationReAppointment = (navigation) => {


    navigation.setOptions({
        tabBarVisible: true,
        headerTitle: props => <LogoTitle1 {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>

            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back_icon}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerTransparent: true,
        headerShown: true,


    })
}




const setNavigationMyProfile = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitleWhite {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>
                {/* <TouchableOpacity onPress={() => navigation.editButtonPress()}>
                    <Image
                        source={Images.editWhite}
                        style={NavgationStyles.rightButton} />
                </TouchableOpacity> */}
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.menuWhite}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerStyle: {
            backgroundColor: colors.main,
        },
        headerTintColor: colors.white,
        headerShown: true,

    })
}

const setNavigationMyProfileBack = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitleWhite {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>
                {/* <TouchableOpacity onPress={() => navigation.editButtonPress()}>
                    <Image
                        source={Images.editWhite}
                        style={NavgationStyles.rightButton} />
                </TouchableOpacity> */}
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.backWhite}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerStyle: {
            backgroundColor: colors.main,
        },
        headerTintColor: colors.white,
        headerShown: true,

    })
}

const setNavigationFilter = (navigation) => {
    navigation.setOptions({
        headerTitle: props => <LogoTitle1 {...navigation} />,
        headerRight: () => (
            <View style={NavgationStyles.rightView}>
                <TouchableOpacity onPress={() => navigation.resetButtonPress()}>
                    <Text style={NavgationStyles.restText}>{"Reset"}</Text>
                </TouchableOpacity>
            </View>
        ),
        headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.backButtonPress()}>
                <Image
                    source={Images.back}
                    style={NavgationStyles.backButton} />
            </TouchableOpacity>
        ),
        headerTransparent: false,
        headerShown: true,

    })
}

export default {
    setNavigationTitleBack,
    setNavigationOrder, setNavigationPortfolio, setNavigationMenu, setNavigationSelectedGoal, setNavigationTitleAndBack, setNavigationGoalPlanner, setWatchListTab, setNavigationDetails, setNavigationSearchTab, setNavigationTitleWhite, setNavigation, setNavigationTitle, Icon, setNavigationTitleMenu, setNavigationAppointmentDetails, setNavigationReAppointment, setNavigationColor, setNavigationMyProfile, setNavigationMyProfileBack, setNavigationColorMenu, setNavigationFilter
};